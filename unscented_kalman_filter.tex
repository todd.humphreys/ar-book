\chapter{Unscented Kalman Filter}
\label{chap:ukf}

The Unscented Kalman Filter (UKF) \cite{wan2001unscented} is one in a family of
estimators that apply \emph{deterministic sampling} to track the effect of a
nonlinear dynamics equation, a nonlinear measurement equation, or both, on the
probability distribution of a system's evolving state and its measurements.
The key insight, articulated by the UKF inventors Julier and Uhlmann, is this point
\cite{julier2004UKF}:
\begin{quotation}
  \noindent
  It is easier to approximate a probability distribution than it is to
  approximate an arbitrary non-linear function.
\end{quotation}

In other words, instead of approximating the nonlinear functions themselves,
as the EKF does through a Taylor series expansion, one can more easily
approximate the probability distribution resulting from passage through the
nonlinear functions.  This is done with the so-called Unscented Transformation
\cite{julier2002scaled}.  

The UKF is more accurate than the EKF and avoids the need to find the Jacobian
of nonlinear dynamics and measurement functions, so it also tends to be easier
to implement and less prone to bugs.  The downside is that the UKF is somewhat
more computationally demanding than the EKF.  For gentle nonlinearities with
easily-obtained Jacobians, the EKF's speed and adequate accuracy is hard to
beat.

The UKF's algorithms were originally derived by \emph{ad hoc} reasoning from the
intuitive principle quoted above.  The UKF was later shown to be closely related
to the so-called second-order EKF, which captures the second-order term in a
Taylor series expansions of dynamics and measurement nonlinearities
\cite{gustafsson2011some}.  Other, more mathematically-principled deterministic
sampling approaches were later proposed: the Gauss-Hermite Filter
\cite{ito2000gaussian} and the Cubature Kalman Filter
\cite{arasaratnam2009cubature}.  But in 2015, Menegaz et
al. \cite{menegaz2015systematization} gave a rigorous justification of the UKF's
choice of sigma points, and offered a systematization of UKF variants. The UKF
is by now widely accepted as a useful and trustworthy estimation tool.

\section{Prediction}
\subsection{Setup}
We assume a state dynamics model of the form
\begin{equation}
  \label{eq:dyn_model}
\vb{x}(k+1) = \vb{f}[\vb{x}(k), \vb{u}(k), \vb{v}(k) ]  
\end{equation}
where $\vb{f}$ is a (possibly nonlinear) dynamics function, $\vb{x}(k)$ is the
$n_x$-by-1 state vector that applies at time $t_k = k \Delta t$, $\vb{u}(k)$
is the $n_u$-by-1 vector of are known inputs (e.g., control inputs or IMU
data), and $\vb{v}(k)$ is the $n_v$-by-1 vector that models process noise
(e.g., wind disturbance, IMU noise processes).  We will assume that
$\vb{v}(k)$ is zero mean, independent in time, and has covariance $Q(k)$.  We
can write this formally as
\begin{align}
  E[\vb{v}(k)] & = \vb{0} \\
  E[\vb{v}(k)\vb{v}\T(j)] & = Q(k) \delta_{kj} 
\end{align}
The measurement update leaves us with an estimate $\vbh{x}(k)$, which we
assume is the mean of $\vb{x}(k)$, has covariance $P(k)$, and is uncorrelated
with $\vb{v}(j)$ for all $j$.  We can write this formally as
\begin{align}
  E[\vb{x}(k) - \vbh{x}(k)] & = \vb{0} \\
  E[(\vb{x}(k) - \vbh{x}(k))(\vb{x}(k) - \vbh{x}(k))\T] & = P(k) \\
  E[(\vb{x}(k) - \vbh{x}(k))\vb{v}\T(j)] & = \vb{0} ~\forall k,j 
\end{align}

Let $\vbb{x}(k+1)$ be the estimated (predicted) state at $k+1$.  We will
assume that $\vbb{x}(k+1)$ is the mean of $\vb{x}(k+1)$, and we will denote
its covariance as $\bar{P}(k+1)$:
\begin{align}
  E[\vb{x}(k+1)- \vbb{x}(k+1)] & = \vb{0} \\
  E[(\vb{x}(k+1)- \vbb{x}(k+1))(\vb{x}(k+1)- \vbb{x}(k+1))\T] & = \bar{P}(k+1)
\end{align}

\subsection{Transformation}
The UKF employs the Unscented Transform (UT) to find $\vbb{x}(k+1)$ and
$\bar{P}(k+1)$ given the setup above.  The UT is a way of inferring the
probability distribution of a set of random variables after the random
variables get transformed through a nonlinear function. The UT creates a set
of so-called sigma points that are representative of $\vb{x}(k)$ and
$\vb{v}(k)$.  It then pushes (transforms) these sigma points through $\vb{f}$
to generate new sigma points, and uses the new sigma points to calculate
$\vbb{x}(k+1)$ and $\bar{P}(k+1)$.  If $\vb{f}$ is nonlinear, then
$\vbb{x}(k+1)$ will not be exactly the conditional mean of $\vb{x}(k+1)$, nor
will $\bar{P}(k+1)$ be exactly the covariance, but they'll be close so long as
$\vb{f}$ isn't terribly nonlinear.

Follow these steps to apply the UT in the UKF's prediction step:
\begin{enumerate}
\item Stack the best estimates of $\vb{x}(k)$ and $\vb{v}(k)$ to get an
  augmented best-estimate vector and an augmented covariance matrix:
  \begin{align}
    \vbh{x}_a(k) & = \left[\ba{c}\vbh{x}(k) \\ \vb{0}\ea\right] \\
    P_a(k) & = \left[\ba{cc} P(k) & 0 \\ 0 & Q(k) \ea \right] 
  \end{align}
\item Cholesky factorize $P_a(k)$:
  \[ S_x = {\tt chol}(P_a(k))\T = [\vb{s}_1, \vb{s}_2, ..., \vb{s}_{n_x + n_v}] \]


\item Build up a set of sigma points for the prediction.  Because $P_a(k)$ is
  symmetrical, $\vb{s}_i$ is aligned along the $i$th eigenvector of $P_a(k)$
  and is weighted by the corresponding eigenvalue.  This means that the
  collection of columns in $S_x$ is representative of the spread of the
  distribution of $\vb{x}_a(k) = [\vb{x}\T(k), \vb{v}\T(k)]\T$ around its mean
  $\vbh{x}_a(k)$.  Thus, from the $\vb{s}_i$ we can create a set of
  representative sigma points $\vb{\chi}_i$, $i = 0, 1, ..., 2(n_x + n_v)$ as
  follows:
  \begin{align}
    \vb{\chi}_0 & = \vbh{x}_a(k) \\
    \vb{\chi}_i & = \left\{ \ba{ll} \vb{\chi}_0 + \sqrt{n_x + n_v +
                     \lambda} \cdot  \vb{s}_i &  i \in [1, n_x + n_v] \\
    \vb{\chi}_0 - \sqrt{n_x + n_v +
    \lambda} \cdot \vb{s}_{i-n_x-n_v} &  i \in [n_x + n_v + 1, 2(n_x + n_v)] \ea \right.
  \end{align}

  The parameter $\lambda$ is a tuning parameter that allows us to make the sigma
  point constellation either more spread out (larger $\lambda$) or more compact
  (smaller $\lambda$).  The UKF sets $\lambda$ in terms of two other tuning
  parameters, $\alpha$ and $\kappa$:
  \[ \lambda = \alpha^2 (n_x + n_v + \kappa) - (n_x + n_v) \] Following
  \cite{gustafsson2011some}, we'll set $\kappa = 0$ and $\alpha = 0.001$, which
  leads to satisfactory performance on a wide variety of nonlinear functions.

\item Push each sigma point $\vb{\chi}_i$ through the system dynamics function
  $\vb{f}$ to create a transformed sigma point $\vbb{\chi}_i$. The first $n_x$
  elements of $\vb{\chi}_i$ correspond to $\vb{x}(k)$, whereas the last $n_v$
  elements of $\vb{\chi}_i$ correspond to $\vb{v}(k)$.  Below, we use the
  notation $\vb{\chi}_i(j:p)$ to indicate selection of elements $j$ to $p$ of
  the vector $\vb{\chi}_i$.
  \[ \vbb{\chi}_i = \vb{f}[\vb{\chi}_i(1:n_x), \vb{u}(k), \vb{\chi}_i(n_x +
    1:n_x + n_v) ] \]
\item Combine the transformed sigma points $\vbb{\chi}_i$ to calculate the
  predicted mean and covariance:
  \begin{align}
    \vbb{x}(k+1) &  = \sum_{i = 0}^{2(n_x + n_v)} W_i^m \vbb{\chi}_i \\
    \bar{P}(k+1) &  = \sum_{i = 0}^{2(n_x + n_v)} W_i^c [\vbb{\chi}_i -
                   \vbb{x}(k+1)][\vbb{\chi}_i - \vbb{x}(k+1)]\T    
  \end{align}
  where
  \begin{align}
    W_0^m & = \frac{\lambda}{n_x + n_v + \lambda} \\
    W_i^m & = \frac{1}{2(n_x + n_v + \lambda)}, ~ i = 1, ... , 2(n_x + n_v) \\
    W_0^c & = \frac{\lambda}{n_x + n_v + \lambda} + 1 - \alpha^2 + \beta \\
    W_i^c & = W_i^m, ~ i = 1, ..., 2(n_x + n_v)
  \end{align}
  are the UT weights. Since we wish to approximate distributions as Gaussian, we
  will set $\beta = 2$ \cite{gustafsson2011some}.

\item Set $k \leftarrow (k+1)$ and start over again with the measurement update,
  continuing thus in the measurement-prediction cycle until all data are
  exhausted.
   
\end{enumerate}

\section{Measurement Update}
  \subsection{Setup}
We assume a measurement model of the form
\begin{equation}
  \label{eq:meas_model}
\vb{z}(k) = \vb{h}[\vb{x}(k),\vb{w}(k)]  
\end{equation}
where $\vb{z}(k)$ is the $n_z$-by-1 measurement vector that applies at time
$t_{k} = (k) \Delta t$, $\vb{h}$ is a (possibly nonlinear) measurement
function, $\vb{x}(k)$ is the $n_x$-by-1 state vector, and $\vb{w}(k)$ is
the $n_z$-by-1 vector that models measurement noise (e.g., GPS measurement
errors, camera measurement errors).  We will assume that $\vb{w}(k)$ is zero
mean, independent in time, and has covariance $R(k)$.  We can write this
formally as
\begin{align}
  E[\vb{w}(k)] & = \vb{0} \\
  E[\vb{w}(k)\vb{w}\T(j)] & = R(k) \delta_{kj} 
\end{align}
We begin the measurement update with the estimate $\vbb{x}(k)$ from the
prediction step, which we assume is unbiased, has covariance
$\bar{P}(k)$, and is uncorrelated with $\vb{w}(j)$ for all $j$.  We can
write this formally as
\begin{align}
  E[\vb{x}(k) - \vbb{x}(k)] & = \vb{0} \\
  E[(\vb{x}(k) - \vbb{x}(k))(\vb{x}(k) - \vbb{x}(k))\T] & = \bar{P}(k) \\
  E[(\vb{x}(k) - \vbb{x}(k))\vb{w}\T(j)] & = \vb{0} ~\forall k,j 
\end{align}

Let $\vbh{x}(k)$ be the estimated state at $k$ after having ingested the
measurements $\vb{z}(k)$.  We will assume that $\vbh{x}(k)$ is the mean of
$\vb{x}(k)$, and we will denote its covariance as ${P}(k)$:
\begin{align}
  E[\vb{x}(k)- \vbh{x}(k)] & = \vb{0} \\
  E[(\vb{x}(k)- \vbh{x}(k))(\vb{x}(k)- \vbh{x}(k))\T] & = {P}(k)
\end{align}

\subsection{Transformation}
We again apply the UT to find $\vbh{x}(k)$ and ${P}(k)$ given the setup
above. Recall that the UT is a way of inferring the probability distribution
of a set of random variables after the random variables get transformed
through a nonlinear function. In the present case, the function is the
measurement model and the random variables are the state $\vb{x}(k)$ and the
measurement noise $\vb{w}(k)$.

The UT creates a set of sigma points that are representative of $\vb{x}(k)$
and $\vb{w}(k)$.  The UT then pushes (transforms) these sigma points through
the measurement model to generate new sigma points, and uses the new sigma
points to calculate $\vbh{x}(k)$ and ${P}(k)$.  If $\vb{h}$ is nonlinear, then
$\vbh{x}(k)$ will not be exactly the conditional mean of $\vb{x}(k)$ given the
measurements, nor will ${P}(k)$ be exactly the covariance, but they'll
be close so long as $\vb{h}$ isn't terribly nonlinear.

Follow these steps to apply the UT in the UKF's measurement update:
\begin{enumerate}
\item Stack the best estimates of $\vb{x}(k)$ and $\vb{w}(k)$ to get an
  augmented best-estimate vector and an augmented covariance matrix:
  \begin{align}
    \vbb{x}_a(k) & = \left[\ba{c}\vbb{x}(k) \\ \vb{0}\ea\right] \\
    \bar{P}_a(k) & = \left[\ba{cc} \bar{P}(k) & 0 \\ 0 & R(k) \ea \right] 
  \end{align}
\item Cholesky factorize $\bar{P}_a(k)$:
  \[ \bar{S}_x = {\tt chol}(\bar{P}_a(k))\T = [\vbb{s}_1, \vbb{s}_2, ...,
    \vbb{s}_{n_x + n_z}] \]

\item Build up a set of sigma points for the measurement update.  Because
  $\bar{P}_a(k)$ is symmetrical, $\vbb{s}_i$ is aligned along the $i$th
  eigenvector of $\bar{P}_a(k)$ and is weighted by the corresponding
  eigenvalue.  This means that the collection of columns in $\bar{S}_x$ is
  representative of the spread of the distribution of
  $\vb{x}_a(k) = [\vb{x}\T(k), \vb{w}\T(k)]\T$ around its mean
  $\vbb{x}_a(k)$.  Thus, from the $\vbb{s}_i$ we can create a set of
  representative sigma points $\vbb{\gamma}_i$, $i = 0, 1, ..., 2(n_x + n_z)$
  as follows:
  \begin{align}
    \vbb{\gamma}_0 & = \vbb{x}_a(k) \\
    \vbb{\gamma}_i & = \left\{ \ba{ll} \vbb{\gamma}_0 + \sqrt{n_x + n_z +
                     \lambda_u} \cdot  \vbb{s}_i &  i \in [1, n_x + n_z] \\
    \vbb{\gamma}_0 - \sqrt{n_x + n_z +
    \lambda_u} \cdot \vbb{s}_{i-n_x - n_z} &  i \in [n_x + n_z + 1, 2(n_x + n_z)] \ea \right.
  \end{align}

  The quantity $\lambda_u$ is a tuning parameter that functions for the
  measurement update just as $\lambda$ did for the prediction step.  We set
  \[ \lambda_u = \alpha^2 (n_x + n_z + \kappa) - (n_x + n_z) \]
  with $\kappa = 0$ and $\alpha = 0.001$ as before.

\item Push each sigma point $\vbb{\gamma}_i$ through the measurement model to
  create a measurement sigma point $\vbb{z}_i$. The first $n_x$ elements
  of $\vbb{\gamma}_i$ correspond to $\vb{x}(k)$, whereas the last $n_z$
  elements of $\vbb{\gamma}_i$ correspond to $\vb{w}(k)$.  Below, we use the
  notation $\vbb{\gamma}_i(j:p)$ to indicate selection of elements $j$ to $p$
  of the vector $\vbb{\gamma}_i$.
  \[ \vbb{z}_i = \vb{h}[\vbb{\gamma}_i(1:n_x),  \vbb{\gamma}_i(n_x +
    1:n_x + n_z)] \]
\item Combine the measurement sigma points $\vbb{z}_i$ to calculate the
  predicted mean, its covariance, and its cross-covariance with the state:
  \begin{align}
    \vbb{z}(k) &  = \sum_{i = 0}^{2(n_x + n_z)} W_i^m \vbb{z}_i \\
    {P}_{zz}(k) &  = \sum_{i = 0}^{2(n_x + n_z)} W_i^c [\vbb{z}_i -
                    \vbb{z}(k)][\vbb{z}_i - \vbb{z}(k)]\T \\
    {P}_{xz}(k) &  = \sum_{i = 0}^{2(n_x + n_z)} W_i^c [\vbb{\gamma}_i(1:n_x) -
                   \vbb{x}(k)][\vbb{z}_i - \vbb{z}(k)]\T 
  \end{align}
  where
  \begin{align}
    W_0^m & = \frac{\lambda_u}{n_x + n_z + \lambda_u} \\
    W_i^m & = \frac{1}{2(n_x + n_z + \lambda_u)}, ~ i = 1, ..., 2(n_x + n_z) \\
    W_0^c & = \frac{\lambda_u}{n_x + n_z + \lambda_u} + 1 - \alpha^2 + \beta \\
    W_i^c & = W_i^m, ~ i = 1, ..., 2(n_x + n_z)
  \end{align}
  Again, set $\beta = 2$.

  \item Perform the linear minimum mean square error (LMMSE) update to obtain
    $\vbh{x}(k)$ and $P(k)$:
    \begin{align}
      \vbh{x}(k) & = \vbb{x}(k) +
                     P_{xz}(k)P_{zz}^{-1}(k)\left[\vb{z}(k) -
                     \vbb{z}(k) \right] \\
      P(k) & = \bar{P}(k) - P_{xz}(k) P_{zz}^{-1}(k) P_{xz}\T(k)
    \end{align}
\end{enumerate}
 

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:

% LocalWords:  Julier EKF's Julier's Cubature Menegaz chol LMMSE
