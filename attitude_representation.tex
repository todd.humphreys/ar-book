\chapter{Attitude Representation}
\label{chap:attRep}

% Until now, we have considered space vehicles (SVs) to be point masses, and we
% were unconcerned with the \textit{orientation} of these masses in space. We
% represented \textit{position} as a $3\times1$ vector expressed in some
% reference frame: e.g., $\vb{R} = \begin{bmatrix} x & y & z
% \end{bmatrix}\T$ or $\vb{L} = \begin{bmatrix} lat & lon & ht \end{bmatrix}\T$. 

For thousands of years, sailors were content with just one parameter to specify
a ship's orientation: bearing.
\begin{figure}[h]
  \centering
  \includegraphics{figs/ship_bearing_angle.png}
\end{figure}

The advent of aircraft added to the vernacular:
\begin{figure}[h]
  \centering
  \includegraphics{figs/airplane_roll_pitch_yaw.png}
\end{figure}

Artificial satellites starting with \textit{Sputnik} annihilated the concept of
a preferred ``up'' and ``down''; a more general framework was needed.

As regards aerial robotics, knowing (and controlling) how an aerial vehicle is
oriented in 3-space is critical. Think about the attitude control requirements
for an air taxi.

\textit{Attitude} expresses a relationship between two coordinate
systems. There are many options for expressing attitude, each with its special
niche.

\section{Right-handed Orthonormal Coordinate Systems}
An \textit{orthonormal basis} is a set of three mutually orthogonal unit
vectors:
\begin{figure}[h]
  \centering
  \includegraphics{figs/orth_basis.png}
\end{figure}
\begin{align*}
  \vb{i} \cdot \vb{j} = \vb{j} \cdot \vb{k} = \vb{i} \cdot \vb{k} &= 0 \\
  \vb{i} \cdot \vb{i} = \vb{j} \cdot \vb{j} = 
  \vb{k} \cdot \vb{k} &= 1	
\end{align*}
A \textit{right-handed} basis satisfies
\begin{align*}
  \vb{i} \times \vb{j} &= \vb{k} \\	
  \vb{j} \times \vb{k} &= \vb{i} \\	
  \vb{k} \times \vb{i} &= \vb{j} \\	
\end{align*}
We can represent any physical vector $\vb{r}$ in 3-space in a given orthonormal
basis $\{\vb{i}, \vb{j}, \vb{k}\}$ by finding the \textit{components} of
$\vb{r}$ along each basis vector:
\begin{figure}[h]
  \centering
  \includegraphics{figs/vector_components.png}
\end{figure}
\begin{align*}
  \vb{r} &= x\vb{i} + y\vb{j} + z\vb{k} \\
  x &= \vb{i} \cdot \vb{r} \\
  y &= \vb{j} \cdot \vb{r} \\
  z &= \vb{k} \cdot \vb{r}
\end{align*}
We write $\vb{r}$ as a column vector:
\begin{align*}
  \vb{r} = \begin{bmatrix} x \\ y \\ z \end{bmatrix}
\end{align*}
Consider two vectors, $\vb{u}$ and $\vb{v}$. These may be represented as
\begin{align*}
  \vb{u} &= u_1\vb{i} + u_2\vb{j} + u_3\vb{k} \\
  \vb{v} &= v_1\vb{i} + v_2\vb{j} + v_3\vb{k}
\end{align*}
\begin{align*}
  \vb{u} =  \begin{bmatrix} u_1 \\ u_2 \\ u_3 \end{bmatrix}, \quad
  \vb{v} =  \begin{bmatrix} v_1 \\ v_2 \\ v_3 \end{bmatrix}
\end{align*}
The \textit{dot product}, or \textit{scalar product}, is
\begin{equation}
  \label{eq:scalar_prod}
  \vb{u} \cdot \vb{v} = \sum_{i=1}^3 u_i v_i = \vb{u}\T\vb{v}
\end{equation}
where $\vb{u}\T = \begin{bmatrix} u_1 & u_2 & u_3 \end{bmatrix}$.
The \textit{cross
  product} or \textit{vector product} is
\begin{align}
  \vb{u} \times \vb{v} &= (u_2 v_3 - u_3 v_2)\vb{i} + (u_3 v_1 - u_1 v_3)\vb{j} + (u_1 v_2 - u_2 v_1)\vb{k} \\
                       &= \begin{bmatrix} u_2 v_3 - u_3 v_2 \\ u_3 v_1 - u_1 v_3 \\ u_1 v_2 - u_2 v_1 \end{bmatrix} \\
                       &= \begin{bmatrix} 0 & -u_3 & u_2 \\ u_3 & 0 & -u_1 \\ -u_2 & u_1 & 0	\end{bmatrix} \begin{bmatrix} v_1 \\ v_2 \\ v_3 \end{bmatrix}
\end{align}
The skew-symmetric matrix
\[[\vb{u}\times] \define \begin{bmatrix} 0 & -u_3 & u_2 \\ u_3 & 0 & -u_1 \\
    -u_2 & u_1 & 0 \end{bmatrix} \] is called the \textit{cross-product
  equivalent matrix}. With it we can compute the cross product as
\begin{equation}
  \vb{u} \times \vb{v} = [\vb{u}\times] \vb{v}
\end{equation}

\begin{rem}~
  \begin{enumerate}
  \item There is only one \textit{physical} vector $\vb{r}$, but there are
    infinitely many representations of it.
  \item The \textit{origin} of reference frames is irrelevant for attitude
    determination, so one might as well assume coincident origins for all bases.
  \end{enumerate}
\end{rem}

\section{Orthonormal Transformations}
\label{sec:orth-transf}
Suppose we are given two orthonormal bases: $\{\vb{I}, \vb{J}, \vb{K}\}$ (e.g.,
inertial), and $\{\vb{i}, \vb{j}, \vb{k}\}$ (e.g., body).  These can be
represented in terms of each other:
\begin{align*}
  \vb{i} &= c_{11}\vb{I} + c_{12}\vb{J} + c_{13}\vb{K} \\
  \vb{j} &= c_{21}\vb{I} + c_{22}\vb{J} + c_{23}\vb{K} \\
  \vb{k} &= c_{31}\vb{I} + c_{32}\vb{J} + c_{33}\vb{K}
\end{align*}
\begin{align*}
  \vb{I} &= c_{11}'\vb{i} + c_{12}'\vb{j} + c_{13}'\vb{k} \\
  \vb{J} &= c_{21}'\vb{i} + c_{22}'\vb{j} + c_{23}'\vb{k} \\
  \vb{K} &= c_{31}'\vb{i} + c_{32}'\vb{j} + c_{33}'\vb{k}
\end{align*}
Since the bases $\{\vb{I}, \vb{J}, \vb{K}\}$ and $\{\vb{i}, \vb{j}, \vb{k}\}$
are orthonormal,
\begin{align*}
  c_{11} &= \vb{I} \cdot \vb{i} = c_{11}' \\
  c_{12} &= \vb{J} \cdot \vb{i} = c_{21}' \\
  c_{13} &= \vb{K} \cdot \vb{i} = c_{31}' \\
    &~~~~~~~\vdots\\
  c_{33} &= \vb{K} \cdot \vb{k} = c_{33}'
\end{align*}

Thus, $c_{ij} = c_{ji}'$. Think of the $c_{ij}$ and $c_{ij}'$ as elements of the
$3 \times 3$ matrices $C$ and $C'$, respectively. Then $C\T = C'$.

The coefficients $c_{ij}$ and $c_{ij}'$ are called \textit{direction cosines}
because, e.g., $\vb{I} \cdot \vb{i}$ is the cosine of the angle between the
directions $\vb{I}$ and $\vb{i}$.  $C$ and $C'$ are \textit{direction cosine
  matrices}. We can show that $CC\T = C\T C = I$, the identity matrix.

\begin{rem}~
  \begin{enumerate}
\item $C\T$ is the matrix inverse of $C$. 
\item A square matrix for which $C\T = C^{-1}$ is called \textit{orthonormal}.
\item Orthonormal bases are related by orthonormal matrices. 
\item The direction-cosine matrix $C$ is one example of an attitude representation. 
\end{enumerate}
\end{rem}

\qa{Let
  $\vb{r}_{[\vb{I}, \vb{J}, \vb{K}]} = \begin{bmatrix} X & Y &
    Z \end{bmatrix}\T$ and
  $\vb{r}_{[\vb{i}, \vb{j}, \vb{k}]} = \begin{bmatrix} x & y &
    z \end{bmatrix}\T$. How can we relate the two vectors?}  {Let
  $\vb{r}$ be a physical vector (no basis specified).  Then
  \begin{align*} 
    x = \vb{i} \cdot \vb{r} &= (c_{11}\vb{I} + c_{12}\vb{J} + c_{13}\vb{k}) \cdot \vb{r} \\
                            &= c_{11}X + c_{12}Y + c_{13}Z \\
                            &~~ \vdots \\
                            &
                              \textrm{etc. for
                              all
                              other
                              components}
  \end{align*}
  Hence,
  \begin{align*}
    \begin{bmatrix} x \\ y \\ z \end{bmatrix} &= \begin{bmatrix} c_{11} & c_{12}
      & c_{13} \\ c_{21} & c_{22} & c_{23} \\ c_{31} & c_{32} &
      c_{33} \end{bmatrix} \begin{bmatrix} X \\ Y \\ Z \end{bmatrix}
  \end{align*} which can be written compactly as
  \begin{align*}  
    \vb{r}_{[\vb{i}, \vb{j}, \vb{k}]} &= C \vb{r}_{[\vb{I}, \vb{J}, \vb{K}]}
  \end{align*}}
The determinant of an orthonormal matrix $C$, denoted $\det{C}$, can be found as follows:
\begin{align*}
  \det(I) &= \det(CC\T) = \det(C)\det(C\T) = (\det(C))^2 = 1 \\
         &\Rightarrow \det(C) = \pm 1
\end{align*}
where
\begin{align*}
  \det(C) = \begin{cases} ~~1 & \text{if C is \textit{proper}} \\
    -1 & \text{if C is \textit{improper}} \end{cases}
\end{align*}
Properties of orthonormal transformations:
\begin{itemize}
\item $C\vb{x} \cdot C\vb{y} = \vb{x} \cdot \vb{y}$ (scalar product preserved)
\item $(C\vb{x}) \times (C\vb{y}) = (\det(C))C(\vb{x} \times \vb{y})$ (vector
  product preserved if $C$ is proper)
\end{itemize}

\section{The Rotation Matrix}
Consider a rotation about a single axis, say, the $\vb{K}$ axis:
\begin{figure}[h]
  \centering
  \includegraphics{figs/single_axis_rot.png}
\end{figure}
\begin{align*}
  \vb{i} &= ~~\cos(\phi)\vb{I} + \sin(\phi)\vb{J} \\
  \vb{j} &= -\sin(\phi)\vb{I} + \cos(\phi)\vb{J} \\
  \vb{k} &= \vb{K}
\end{align*}
We can pack these coefficients into matrix notation:
\begin{equation}
  R(\vb{K}, \phi) = \begin{bmatrix} ~~\cos(\phi) & \sin(\phi) & 0 \\
    -\sin(\phi) & \cos(\phi) & 0 \\
    0 & 0 & 1 \end{bmatrix}
\end{equation}
$R(\vb{K}, \phi)$ is called a \emph{rotation matrix}. We can think
of it from two different perspectives:
\begin{enumerate}
\item \textbf{Frame Rotation:} Just as with the $C$ matrix, the
  $R(\vb{K}, \phi)$ matrix can transform a representation
  $\vb{r}_{[\vb{I}, \vb{J}, \vb{K}]}$ of a physical vector $\vb{r}$ in the
  $\{\vb{I}, \vb{J}, \vb{K}\}$ basis to a representation
  $\vb{r}_{[\vb{i}, \vb{j}, \vb{k}]}$ of the same physical vector in a basis
  $\{\vb{i}, \vb{j}, \vb{k}\}$ that happens to be rotated $\phi$ radians about
  the $\vb{K}$ axis:
  \begin{figure}[h]
    \centering
    \includegraphics{figs/two_diff_bases.png}
  \end{figure}
  \begin{align*}
    \vb{r}_{[\vb{i}, \vb{j}, \vb{k}]} &= R(\vb{K}, \phi) \vb{r}_{[\vb{I}, \vb{J}, \vb{K}]} \\
    \begin{bmatrix} x \\ y \\ z \end{bmatrix} &= R(\vb{K}, \phi) \begin{bmatrix} X \\ Y \\ Z \end{bmatrix} 
  \end{align*}
\item \textbf{Point Rotation:} The $R\T(\vb{K}, \phi)$ matrix (note the
  transpose!) can be thought of as rotating a vector about $\vb{K}$ in the
  $\{\vb{I}, \vb{J}, \vb{K}\}$ basis. An initial vector
  $\vb{r}_{1[\vb{I}, \vb{J}, \vb{K}]}$ can be rotation by $\phi$ about $\vb{K}$
  to a final vector $\vb{r}_{2[\vb{I}, \vb{J}, \vb{K}]}$. These are now
  different physical vectors expressed in the same basis:
  \begin{figure}[h]
    \centering
    \includegraphics{figs/rot_within_basis.png}
  \end{figure}
  \begin{align*}
    \vb{r}_{2, [\vb{I}, \vb{J}, \vb{K}]} &= R\T(\vb{K}, \phi) \vb{r}_{1, [\vb{I}, \vb{J}, \vb{K}]} \\
    \begin{bmatrix} X_2 \\ Y_2 \\ Z_2 \end{bmatrix} &= R\T(\vb{K}, \phi) \begin{bmatrix} X_1 \\ Y_1 \\ Z_1 \end{bmatrix} 
  \end{align*}
  For example, one may wish to rotate the Hubble space telescope's pointing
  vector in inertial space. The matrix $R\T(\vb{K}, \phi)$ would describe this
  operation.
\end{enumerate}

Rotations about the $\vb{I}$ and $\vb{J}$ axes may be formed in a similar
manner:
\begin{align*}
  R(\vb{I}, \phi) = \begin{bmatrix} 1 & 0 & 0 \\
    0 & ~~\cos(\phi) & \sin(\phi) \\
    0 & -\sin(\phi) & \cos(\phi) \end{bmatrix} \\
  R(\vb{J}, \phi) = \begin{bmatrix} \cos(\phi) & 0 & -\sin(\phi) \\
    0 & 1 & 0 \\
    \sin(\phi) & 0 & ~~\cos(\phi) \end{bmatrix} \\
\end{align*}	                          
\begin{align*}
  \textrm{Frame rotation: } R(\vb{K}, \phi) \\
  \textrm{Point rotation: } R\T(\vb{K}, \phi)
\end{align*}
For a rotation through an angle $\phi$ about an arbitrary axis $\hat{\vb{a}}$,
the general formula is
\begin{equation}
  R(\hat{\vb{a}}, \phi) = \cos(\phi) I_{3\times3} + (1 - \cos(\phi))\hat{\vb{a}}\hat{\vb{a}}\T - \sin(\phi)[\hat{\vb{a}}\times]
\end{equation}

This is \textit{Euler's formula}. The vector $\hat{\vb{a}}$ is the
\textit{eigenvector of rotation}. One can verify that
$\det(R(\hat{\vb{a}}, \phi)) = 1$. Thus, all rotation matrices are
\textit{proper orthogonal}.

\qa{(T/F) Every rotation can be expressed as a rotation about a single axis.}
{True---this is Euler's rotation theorem.}

\qa{How many parameters are required to specify a rotation?}  {3. The axis of
  rotation has two free parameters, and the angle of rotation is a third
  parameter. } The above implies that the 9 elements of $C$ are not
independent. Indeed, they are subject to six constraints:
\begin{align*}
  C\T C = CC\T = I
\end{align*}
Orthonormality satisfies the six constraints.

\qa{How many vectors $\vb{v}$ exist such that $\vb{v} = R\vb{v}$?}  {One, 
  $\vb{v}$ is the eigenvector of $R$ with eigenvalue $\lambda = 1$, unless
  $\phi = 0$ so that $R = I$, in which case there are infinitely many.}

\begin{rem}~
  \begin{enumerate}
  \item Frame rotation is standard for expressing attitude.
  \item \textsc{Matlab}'s \texttt{rotx} function returns a matrix for point
    rotation.
  \item $R(\vbh{a},\phi) = R\T(-\vbh{a},-\phi)$
  \end{enumerate}
\end{rem}

\section{Euler Angles}
Consider a sequence of three rotations about body axes:
\begin{align*}
  C = R(\hat{\vb{a}}_3, \phi_3)R(\hat{\vb{a}}_2, \phi_2)R(\hat{\vb{a}}_1, \phi_1)
\end{align*}
Here, the $\hat{\vb{a}}_i$ are chosen from the set of three unit column vectors: 
\begin{align*}
  \vb{e}_1 = \begin{bmatrix} 1 \\ 0 \\ 0 \end{bmatrix}, \quad
  \vb{e}_2 = \begin{bmatrix} 0 \\ 1 \\ 0 \end{bmatrix}, \quad
  \vb{e}_3 = \begin{bmatrix} 0 \\ 0 \\ 1 \end{bmatrix}
\end{align*}
One possible ordering is
\begin{align*}
  \hat{\vb{a}}_1 = \vb{e}_3, \quad  \hat{\vb{a}}_2 = \vb{e}_1, \quad \hat{\vb{a}}_3 = \vb{e}_3
\end{align*}
This is a 3-1-3 sequence. We interpret this sequence of rotations just as we do
for rotations relating the inertial and perifocal frames in spacecraft dynamics:
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{figs/seq_of_rotations.png}
\end{figure}
\begin{align*}
  \{\vb{I}, &\vb{J}, \vb{K}\} \\
            &\downarrow \textrm{ rotation about } \vb{K} \\
  \{\vb{I}', &\vb{J}', \vb{K}'\} \\
            &\downarrow \textrm{ rotation about } \vb{I}' \\
  \{\vb{I}'', &\vb{J}'', \vb{K}''\} \\	
            &\downarrow \textrm{ rotation about } \vb{K}'' \\
  \{\vb{i}, &\vb{j}, \vb{k}\}
\end{align*}

\qa{Why not simply perform a single rotation about the single rotation axis
  guaranteed by Euler's theorem?}  {
  Think about how a physical rotation would be \textit{mechanized}. We likely
    have actuators (e.g., motors, reaction wheels) mounted such that they
    provide a torque about body axes.}
\qa{Which rotation sequences make sense?}
{\begin{itemize}
  \item 3-1-3: Yes.
  \item 3-1-2: Yes. 
  \item 3-3-1: No. 
  \item 1-2-3: Yes. 
  \item 2-3-3: No. 
  \end{itemize}}
\qa{How many unique rotation sequences are there?}
{$3 \times 2 \times 2 = 12$.}

The 3-1-3 sequence is commonly used in quantum theory and
for spacecraft attitude representation.  The 3-1-2 sequence is popular among
roboticists and for aircraft attitude representation.

The angles $\phi_1$, $\phi_2$, and $\phi_3$ are called \textit{Euler angles},
and can be associated in various ways with the symbols $\phi$, $\theta$, and
$\psi$. The axes $\hat{\vb{a}}_1$, $\hat{\vb{a}}_2$, and $\hat{\vb{a}}_3$ are
\textit{Euler axes}. An attitude matrix $C$ can be related to Euler angles and
Euler axes as follows:
\begin{equation}
  C(\phi_1, \phi_2, \phi_3) = R(\hat{\vb{a}}_3, \phi_3)R(\hat{\vb{a}}_2, \phi_2)R(\hat{\vb{a}}_1, \phi_1)
\end{equation}
These rotations are read right to left. 

Every rotation can be written in terms of 3-1-3 Euler angles, or in terms of any
sequence in the set of 12 valid sequences. For asymmetric sequences (e.g.,
3-1-2), we can call $\phi$ roll, $\theta$ pitch, and $\psi$ yaw.

Our convention:
\begin{itemize}
\item $\psi$ represents a rotation about the body $\vb{z}$ axis
\item $\theta$ represents a rotation about the body $\vb{y}$ axis
\item $\phi$ represents a rotation about the body $\vb{x}$ axis
\end{itemize}

\begin{figure}[h]
  \centering
  \includegraphics{figs/phi_th_psi_airplane.png}
\end{figure}
Thus, our 3-1-2 rotation matrix is
\begin{equation}
  C(\psi, \phi, \theta) = R(\vb{e}_2, \theta) R(\vb{e}_1, \phi) R(\vb{e}_3, \psi)
\end{equation}
\begin{rem}~
  \begin{enumerate}
  \item Different conventions apply to the use of $\phi$, $\theta$, and
    $\psi$. For 3-1-3, the typical convention is
\begin{align*}
  C(\phi, \theta, \psi) = R(\vb{e}_3, \psi) R(\vb{e}_1, \theta) R(\vb{e}_3, \phi)
\end{align*}
For 3-2-1, it is
\begin{align*}
  C( \psi, \theta, \phi) = R(\vb{e}_1, \phi) R(\vb{e}_2, \theta) R(\vb{e}_3, \psi)
\end{align*}
  \item For small Euler angles, all 6 asymmetric sequences yield the same
rotation matrix:
\begin{equation}
  \label{eq:smallEulerAngleDCM}
  C \cong \begin{bmatrix} 1 & \psi & -\theta \\ 
    -\psi & 1 & \phi \\
    \theta & -\phi & 1 \end{bmatrix}  
\end{equation}
Thus, for example,
$\underbrace{C(\psi, \phi, \theta)}_{3-1-2} \cong \underbrace{C(\psi, \theta,
  \phi)}_{3-2-1}$ for small Euler angles.
\end{enumerate}
\end{rem}
By convention we limit Euler angles to a range to preserve uniqueness. For
3-1-3, the convention is
\begin{align*}
  0 \leq \phi < 2\pi, \quad 0 \leq \theta \leq \pi, \quad 0 \leq \psi < 2\pi 
\end{align*}

We've seen that
$C(\phi, \theta, \psi) = R(\hat{\vb{a}}_3, \psi) R(\hat{\vb{a}}_2, \theta)
R(\hat{\vb{a}}_1, \phi)$. To go the other direction---from $C$ to Euler
angles---we must examine C for a particular sequence of rotations.  For the
3-1-3 sequence:
\begin{align}
  \label{eq:euler-angles313}
  C(\phi, \theta, \psi) = \begin{bmatrix} 
    c\psi c\phi - s\psi c\theta s\phi & c\psi s\phi + s\psi c\theta c\phi & s\psi s\theta \\
    -s\psi c\phi - c\psi c\theta s\phi & -s\psi s\phi + c\psi c\theta c\phi & c\psi s\theta \\
    s\theta s\phi & -s\theta c\phi & c\theta \end{bmatrix}
\end{align}
\begin{itemize}
\item[]$C_{33} = \cos(\theta$).  Thus $\theta = \arccos(C_{33})$. 
\item[]$C_{31} = \sin(\theta)\sin(\phi)$ and $C_{32} =
  -\sin(\theta)\cos{\phi}$. Thus $\phi = \textrm{atan2}(C_{31}, -C_{32})$.
\item[]$C_{13} = \sin(\psi)\sin(\theta)$ and $C_{23} =
  \cos(\psi)\sin(\theta)$. Thus $\psi = \textrm{atan2}(C_{13}, C_{23})$.
\end{itemize}
Note that if $\sin(\theta) = 0$, the second argument in the $\text{atan2}$
function will be zero, causing the function to return $\pi/2$ irrespective of
the actual values of $\phi$ and $\psi$.  The problem is that we've effectively
cut out the second rotation of the 3-1-3 sequence, which means that the
contributions from the first and third angles, $\phi$ and $\psi$, cannot be
distinguished.  One can write the resulting direction cosine matrix as
\begin{align*}
  C = \begin{bmatrix}
    ~~\cos(\phi \pm \psi) & ~~\sin(\phi \pm \psi) & 0 \\
    -\sin(\phi \pm \psi) & \pm \cos(\phi \pm \psi) & 0 \\
    0 & 0 & \pm 1
  \end{bmatrix}
\end{align*}
where we choose $\begin{cases} + & \text{if} \cos(\theta) > 0 \\
  - & \text{if} \cos(\theta) < 0 \end{cases}$

Note from the structure of this matrix that $\phi$ cannot be distinguished from
$\psi$, so neither can be determined uniquely.  Hence, $\theta = 0$ or
$\theta = \pi$ causes a \textit{singularity} in the 3-1-3
representation. Similarly, $\phi = \pm \pi/2$ causes a singularity in the 3-1-2
representation.  In every case of singularity, the second rotation is such that
the first and third rotations have equivalent effects, and so cannot be
distinguished from one another.  In other words, it is impossible to discern how
much of the combined rotation was due to the first Euler angle and how much was
due to the third.

\qa{What should our \texttt{dcm2euler} function do if the input attitude matrix
  is singular for the chosen Euler sequence?}{Return an error message.}
\begin{rem}~
  \begin{enumerate}
  \item It can be proven (not easily!) that \textit{any} 3-parameter
    representation of a rotation matrix is singular for certain attitudes
    \cite{stuelpnagel1964parametrization}. 3-parameter attitude representations
    are not unique!
  \item To avoid the singularity that arises with Euler angles, either
    \begin{enumerate}
    \item Work with quaternions, or
    \item Work with the full attitude matrix $C$ and use Euler angles only to
      express adjustments to $C$ or errors in $C$.
    \end{enumerate}
  \end{enumerate}
\end{rem}

\section{The Quaternion}
The quaternion is not a minimal attitude representation, but it avoids
singularities inherent in a minimal set. The addition of one component makes the
attitude representation unique to within a sign.  Let
\begin{align*}
  \vbb{q} \define \begin{bmatrix} q_1 \\ q_2 \\ q_3 \\ q_4 \end{bmatrix} = 
  \begin{bmatrix} \vb{q} \\ q_4 \end{bmatrix}
\end{align*}
where
\begin{align*}
  \vb{q} &\define \begin{bmatrix} q_1 \\ q_2 \\ q_3 \end{bmatrix} = \sin(\phi/2)\hat{\vb{a}} \\
  q_4 &\define \cos(\phi/2)
\end{align*}
In these expressions, $\vb{q}$ is the \textit{vector component} and $q_4$ is the
\textit{scalar component} of the full quaternion $\vbb{q}$.  Having four
components, the quaternion must satisfy a constraint:
\begin{align*}
  \bar{\vb{q}}\T\bar{\vb{q}} = q_1^2 + q_2^2 + q_3^2 + q_4^2 = 1
\end{align*}
A rotation matrix can be expressed in terms of $\bar{\vb{q}}$:
\begin{equation}
  R = (q_4^2 - |\vb{q}|^2)\vb{I}_{3\times 3} + 2\vb{q}\vb{q}\T - 2q_4[\vb{q}\times]
\end{equation}
Quaternions were once looked on with great suspicion by NASA spaceflight project
managers (read Malcolm Shuster's history of attitude estimation within NASA
\cite{shuster2006quest}). But they have prevailed owing to their many
advantages:
\begin{itemize}
\item No singularity in representation.
\item Conversion between a rotation matrix and a quaternion involves only
  multiplication and addition---no expensive trigonometric function.
\item It's easy to enforce the normalization constraint:
  $\bar{\vb{q}}_{\mathrm{new}} = \frac{1}{\sqrt{\bar{\vb{q}}\T\bar{\vb{q}}}}
  \bar{\vb{q}}$
\item Composition is easier than for matrices:
  \begin{align*} 
    R(\bar{\vb{q}}'') &= R(\bar{\vb{q}}')R(\bar{\vb{q}}) \\
    \bar{\vb{q}}'' &= \bar{\vb{q}}'\bar{\vb{q}} = 
                     \begin{bmatrix} q_4'\vb{q} + q_4 \vb{q}' - \vb{q}' \times \vb{q} \\
                       q_4'q_4 - \vb{q}' \cdot \vb{q} \end{bmatrix}
  \end{align*}
\end{itemize}
Naturally\footnote{In keeping with the well-known Law of Conservation of Pain.}, quaternions
have a few drawbacks:
\begin{itemize}
\item Non-minimal representation (4 parameters vs. 3 for Euler angles)
\item Less intuitive than Euler angles 
\item The error covariance matrix associated with the four quaternion parameters
  is singular owing to the normalization constraint \cite{leffens1982kalman}.
\end{itemize}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:

% LocalWords:  ij ji rotx atan dcm euler Shuster's
