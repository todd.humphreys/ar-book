\chapter{Attitude Kinematics and Dynamics}
\label{chap:attKinAndDyn}

\begin{description}
\item[Attitude kinematics] describes how attitude changes as a function of
  angular velocity, analagous to translational kinematics, which are described
  by the equation $\vbd{r} = \vb{v}$.
\item[Attitude dynamics] describes how angular velocity changes as a function
  of applied torques, analagous to translational dynamics, which are described
  by the equation $\vbdd{r} = \vbd{v} = \sfrac{1}{m} \vb{f}$.
\end{description}

\section{Rates of Change in Moving Frames}
Consider a rigid body rotating about an axis $\vbh{n}$ (a unit vector).  Let
$\vb{b}(t)$ be a physical vector fixed in the body frame, and let $\Delta \phi$
be the angle through which the body rotates about $\vbh{n}$ in time $\Delta t$.
\begin{figure}[h]
  \centering
  \includegraphics[width=0.3\textwidth]{figs/b_rotation}
  \label{fig:b_rotation}
\end{figure}

For small $\Delta \phi$,
\[ \Delta \vb{b} \define \vb{b}(t + \Delta t) - \vb{b}(t) = \Delta \phi (\vbh{n}
  \times \vb{b}) \]  We can use this expression to derive the time derivative
of $\vb{b}$:
\[ \vbd{b} \define \lim_{\Delta t \rightarrow 0} \frac{\Delta \vb{b}}{\Delta t}
  = \lim_{\Delta t \rightarrow 0} \frac{\Delta \phi}{\Delta t}(\vbh{n} \times
  \vb{b}) = \dot{\phi}(\vbh{n} \times \vb{b}) \]

We define the physical vector $\vb{\omega} = \dot{\phi} \vbh{n}$ as the
\emph{angular velocity vector}.  Then
\begin{equation}
  \label{eq:bdot_physical}
\vbd{b} = \vb{\omega} \times \vb{b}  
\end{equation}

Let B denote the body frame, which is attached to the rigid body.  Let I denote
an inertial frame, or a frame that is not experiencing acceleration or
rotation.\footnote{A more precise definition of an inertial reference frame, due
  to Ludwig Lange (1885), is as follows: An inertial frame is a reference frame
  in which a point mass thrown from the same point in three different (non
  co-planar) directions follows rectilinear paths each time it is thrown.}  The
angular velocity $\vb{\omega}$ is a relative quantity: it can be thought of as
describing the instantaneous rotation of any rigid body relative to any
reference frame.  \textbf{But for our purposes, $\vb{\omega}$ will always
  represent angular velocity of the body frame B relative to the inertial frame
  I.}  This definition of $\vb{\omega}$ implies that the time derivative
(velocity) of $\vb{b}$ in (\ref{eq:bdot_physical}) is with respect to the I
frame.  Let $\vb{b}_{\rm I}$ and $\vb{\omega}_{\rm I}$ represent $\vb{b}$ and
$\vb{\omega}$ expressed in the I frame.  Then we can write
\[ \vbd{b}_{\rm I} = \vb{\omega}_{\rm I} \times \vb{b}_{\rm I} \]

In general, our convention for derivatives will be as follows.  Consider an
arbitrary physical vector $\vb{r}$.  We may express this vector in an arbitrary
frame Q as $\vb{r}_\text{Q}$.  The notation $\vbd{r}_\text{Q}$ will always
signify the time derivative of $\vb{r}_\text{Q}$.  In other words, each element
of $\vbd{r}_\text{Q}$ is simply the time rate of change of the corresponding
element of $\vb{r}_\text{Q}$.

To check your understanding of this, let $\vb{b}_{\rm B}$ and
$\vb{\omega}_{\rm B}$ represent $\vb{b}$ and $\vb{\omega}$ expressed in the B
frame. \qa{What is $\vbd{b}_{\rm B}$, the time rate of change of
  $\vb{b}_{\rm B}$?}{$\vbd{b}_{\rm B} = \vb{0}$ because $\vb{b}$ is fixed in the
  B frame.  Thus,
  $\vbd{b}_{\rm B} \neq \vb{\omega}_{\rm B} \times \vb{b}_{\rm B}$. }

\section{Kinematic Equation for a Direction Cosine Matrix}
How does $C$ change with time when the body frame B is rotating with respect to
the inertial frame I?

Differentiate both sides of 
\[ \vb{b}_{\rm B} = C \vb{b}_{\rm I} \]
to get
\begin{align*}
  \vb{0} = \vbd{b}_{\rm B} =& \dot{C} \vb{b}_{\rm I} + {C} \vbd{b}_{\rm I} \\
=&  \dot{C} \vb{b}_{\rm I} + {C} (\vb{\omega}_{\rm I} \times \vb{b}_{\rm I})
\end{align*}
Since $C$ is proper orthogonal, we can rewrite this as
\[\dot{C} \vb{b}_{\rm I} + ({C}\vb{\omega}_{\rm I}) \times (C\vb{b}_{\rm I}) = 0\]
But $\vb{\omega}_{\rm B} = C \vb{\omega}_{\rm I}$, so
\[\dot{C} \vb{b}_{\rm I} + ([\vb{\omega}_{\rm B} \times]C)\vb{b}_{\rm I} = 0\]
Since this must be true for \emph{any} $\vb{b}_{\rm I}$, it follows that
\begin{align}
 \dot{C} = -[\vb{\omega}_{\rm B} \times ] C
  \label{eq:dc-kinematics}
\end{align}
This is the kinematic equation for $C$.  Delightfully simple!

\section{Equations for a vector moving in B}
\label{sec:equat-vect-moving}
In preparation for dynamics equations that will be introduced later on,
now consider a vector $\vb{l}$ that may be moving in the B frame:
\begin{align*}
  \vb{l}_{\rm B}(t) &= C(t) \vb{l}_{\rm I}(t) \\
  \vbd{l}_{\rm B} &= \dot{C} \vb{l}_{\rm I} + {C} \vbd{l}_{\rm I} \\
  &= -[\vb{\omega}_{\rm B} \times ] C \vb{l}_{\rm I} + {C} \vbd{l}_{\rm I} \\ 
\end{align*}
From this we get an important equation:
\begin{align}
  \vbd{l}_{\rm B} = -\vb{\omega}_{\rm B} \times  \vb{l}_{\rm B} + {C} \vbd{l}_{\rm I}
   \label{eq:lBdot}
\end{align}
\qa{Is it correct to write $\vbd{l}_{\rm B} = C \vbd{l}_{\rm I}$?}{No! Beware of
  this trap!  It's clear from (\ref{eq:lBdot}) that
  $\vbd{l}_{\rm B} = C \vbd{l}_{\rm I}$ only if
  $\vb{\omega}_{\rm B} \times \vb{l}_{\rm B} = \vb{0}$.}

Differentiating (\ref{eq:lBdot}) once more yields another equation that
will be important later on when we wish to interpret inertial acceleration as
sensed in the B frame:
\begin{align*}
  \vbdd{l}_{\rm B} = -\vbd{\omega}_{\rm B} \times  \vb{l}_{\rm B}
  -\vb{\omega}_{\rm B} \times  \vbd{l}_{\rm B} + \dot{C}
  \vbd{l}_{\rm I} + {C}\vbdd{l}_{\rm I}
\end{align*}
Substituting $\dot{C} = -[\vb{\omega}_{\rm B} \times] C$ from
(\ref{eq:dc-kinematics}) and
$C \vbd{l}_{\rm I} = \vbd{l}_{\rm B} + \vb{\omega}_{\rm B} \times \vb{l}_{\rm
  B}$ from (\ref{eq:lBdot}), then combining terms and rearranging, we have
\begin{align}
  {C}\vbdd{l}_{\rm I}  = \vbdd{l}_{\rm B} + (\vbd{\omega}_{\rm B}
  \times  \vb{l}_{\rm B})
  + 2(\vb{\omega}_{\rm B} \times  \vbd{l}_{\rm B}) + \vb{\omega}_{\rm
  B}\times(\vb{\omega}_{\rm B} \times \vb{l}_{\rm B})
  \label{eq:rotating_only}
\end{align}
Equation (\ref{eq:rotating_only}) applies when the B frame is rotating, but not
translating, with respect to the I frame. In this case, one can think of the
origins of the B and I frames as coincident.

Now suppose B is also translating with respect to I.  Let $\vb{R}$ be the
vector pointing from the I frame origin to the B frame origin, and let $P$
denote the point to which $\vb{l}$ refers, as shown below.
\begin{figure}[h]
  \centering
  \includegraphics[width=0.6\textwidth]{figs/axes_B_I_translated}
\end{figure}

When expressed in the I frame, $\vb{R}$ is written $\vb{R}_{\rm I}$, and the
vector to $P$ is written $\vb{r}_{\rm I} = \vb{R}_{\rm I} + \vb{l}_{\rm
  I}$. From this it follows that
$C\vbdd{l}_\text{I} = C \vbdd{r}_\text{I} - C \vbdd{R}_\text{I}$.
Substitututing into (\ref{eq:rotating_only}), we obtain the full acceleration
equation for point $P$:
\begin{align}
  C\vbdd{r}_{\rm I}  = \underbrace{{C}\vbdd{R}_{\rm I}}_{\rm
  accel.~of~B~origin} + \underbrace{\vbdd{l}_{\rm B}}_{\rm rel.~accel.} + \underbrace{(\vbd{\omega}_{\rm B}
  \times  \vb{l}_{\rm B})}_{\rm Euler}
  + \underbrace{2(\vb{\omega}_{\rm B} \times  \vbd{l}_{\rm B})}_{\rm Coriolis} + \underbrace{\vb{\omega}_{\rm
  B}\times(\vb{\omega}_{\rm B} \times \vb{l}_{\rm B})}_{\rm centripetal}
  \label{eq:rotating_and_translating}
\end{align}
Let's take a moment to unpack this equation.  The term on the left-hand side,
$C\vbdd{r}_{\rm I}$, is the acceleration of $P$ with respect to I but expressed
in B. In the absence of gravity, this is the acceleration that an ideal 3-axis
B-frame-aligned accelerometer would sense if located at $P$. The terms on the
right-hand side of (\ref{eq:rotating_and_translating}) show all the effects that
contribute to this acceleration:
\[\ba{ll}
C\vbdd{R}_{\rm I} & \mbox{Inertial acceleration of the origin of B,
  expressed in B} \\
\vbdd{l}_{\rm B} & \mbox{Relative acceleration of $\vb{l}$ with respect to B,
  expressed in B} \\
\vbd{\omega}_{\rm B} \times \vb{l}_{\rm B} & \mbox{Euler acceleration,
  also known as transverse acceleration} \\
2(\vb{\omega}_{\rm B} \times  \vbd{l}_{\rm B}) & \mbox{Coriolis acceleration} \\
\vb{\omega}_{\rm B}\times(\vb{\omega}_{\rm B} \times \vb{l}_{\rm B}) &
\mbox{Centripetal acceleration} \ea \]

\section{Kinematic Equation for Euler Angles}
For a 3-1-2 Euler angle sequence, the angular rate expressed in B can be viewed
as the sum of the angular rates contributed by each of the Euler angles, with
suitable rotations to put the rates in the B frame:
\begin{align*}
  \vb{\omega}_{\rm B} =& \dot{\theta}\vb{e}_2 + \dot{\phi} R(\vb{e}_2, \theta)\vb{e}_1 +
                         \dot{\psi} R(\vb{e}_2, \theta) R(\vb{e}_1, \phi)\vb{e}_3 \\
  =& \left[\ba{ccc} c\theta & 0 & -s\theta c\phi \\
  0 & 1 & s\phi \\
  s\theta & 0 & c\theta c\phi \ea \right] \left[ \ba{c} \dot{\phi} \\
  \dot{\theta} \\ \dot{\psi} \ea \right]
\end{align*}
The matrix can be inverted to obtain the Euler rates in terms of
$\vb{\omega}_{\rm B}$:
\begin{align}
   \label{eq:eulerRates_312}
   \left[ \ba{c} \dot{\phi} \\
  \dot{\theta} \\ \dot{\psi} \ea \right]  = & \frac{1}{c\phi} \left[\ba{ccc}
  c\phi c\theta & 0 & c\phi s\theta \\
  s\phi s\theta & c\phi & -c\theta s\phi \\
  -s\theta & 0 & c \theta \ea \right] \vb{\omega}_{\rm B}  \\
  =& S(\phi,\theta,\psi)  \vb{\omega}_{\rm B} \nonumber
\end{align}
\begin{rem}
  The 3-1-2 Euler rates become \emph{infinite} when $\cos \phi = 0$ (which
  occurs when $\phi = \pm \pi/2$), even if $\vb{\omega}_{\rm B}$ is finite.
  This is another manifestation of the singularity of Euler angles.
\end{rem}

\subsection{Kinematic Equation for Error Euler Angles}
\label{sec:kinem-equat-error}
Let $\vb{e} = \left[ \delta \phi, \delta \theta, \delta \psi\right]\T$ be of
3-1-2 error Euler angles defined such that if $C$ is the true attitude matrix
and $\bar{C}$ is a constant modeled attitude close to $C$, then
\begin{align}
  \label{eq:delEulerAngles}
  C = D(\vb{e}) \bar{C}
\end{align}
where $D(\vb{e})$ is the attitude matrix associated with $\vb{e}$. The vector
$\vb{e}$ provides a minimal 3-element repesentation of attitude yet avoids any
singularity: since the error Euler angles are always small, it follows that
$|\delta \phi| \ll \pi/2$ and there is no danger of singularity in the 3-1-2
conversion from $D$ to $\vb{e}$.

As was done for the full Euler angles in (\ref{eq:eulerRates_312}), we will
want a way to relate $\vbd{e}$ to $\vb{\omega}_{\rm B}$. First apply
(\ref{eq:dc-kinematics}) to (\ref{eq:delEulerAngles}) to obtain
\begin{align}
  \dot{D}(\vb{e})\bar{C} & = -\left[\wB \times \right] D(\vb{e}) \bar{C}
\end{align}
Then right multiply by $\bar{C}\T$ to get
\begin{align}
  \dot{D}(\vb{e}) &= -\left[\wB \times \right] D(\vb{e}) 
\end{align}
Note that this equation and (\ref{eq:dc-kinematics}) are identical in form,
which implies that the error Euler angle rates implicit in $\dot{D}(\vb{e})$
have the same relationship with the error Euler angles implicit in $D(\vb{e})$
as the full Euler angle rates have with the full Euler angles.  It follows that
\begin{align}
  \label{eq:errorEulerKinematics}
  \vbd{e} = S(\delta \phi, \delta \theta, \delta \psi) \vb{\omega}_{\rm B}
\end{align}
where $S$ is defined implicitly in (\ref{eq:eulerRates_312}).  Note that
$S(\delta \phi, \delta \theta, \delta \psi) \approx I_{3 \times 3}$ because the
error Euler angles are small.  However, to accurately propagate the error Euler
angles based on $\vb{\omega}_{\rm B}$, one should not invoke this approximation.

\section{Kinematic Equation for the Quaternion}
The kinematic equation for the quaternion is a simple, non-singular expression:
\begin{align}
  \dot{\vbb{q}} = \sfrac{1}{2} \Omega(\vb{\omega}_{\rm B}) \vbb{q}
  \label{eq:qdot}
\end{align}
where $\Omega(\vb{\omega}_{\rm B})$ is the $4 \times 4$ skew-symmetric matrix
\[\Omega(\vb{\omega}_{\rm B}) = \left[\ba{cccc}
    0 & \omega_3 & -\omega_2 & \omega_1 \\
    -\omega_3 & 0 & \omega_1 & \omega_2 \\
    \omega_2 & -\omega_1 & 0 & \omega_3 \\
    -\omega_1 & -\omega_2 & -\omega_3 & 0 \ea \right] \]

\section{Attitude Dynamics}
For translational motion, the momentum $\vb{p}$ and the velocity $\vb{v}$ are
related by a scalar mass $m$: \[\vb{p} = m\vb{v} \] Hence, $\vb{p}$ and
$\vb{v}$ are always parallel vectors.  By contrast, the angular momentum vector
$\vb{L}$ is related to the angular rate $\vb{\omega}$ by
\begin{equation}
  \label{eq:ang_momentum}
  \vb{L} = J \vb{\omega}
\end{equation}
where $J \in \mathbb{R}^{3\times3}$ is called the inertia matrix (or inertia
tensor).  Only under the special circumstances are $\vb{\omega}$ and $\vb{L}$
parallel (when $\vb{\omega}$ is an
eigenvector of $J$).

\begin{framed}
  \textbf{A note on notation:} Recall that we defined $\vb{\omega}$ as the
  angular velocity of a body frame B relative to an inertial frame
  I. Similarly, $\vb{L}$ is the angular momentum of a rotating body relative
  to I.  As is the case for $\vb{\omega}$, we denote $\vb{L}$ when expressed in
  B as $\vb{L}_{\rm B}$ or in I as $\vb{L}_{\rm I}$.  When we write
  $\dot{\vb{L}}_{\rm B}$ we refer to the time derivative of the B-frame
  expression of $\vb{L}$: the time rate of change of ${\vb{L}}_{\rm B}$.
  Likewise, $\dot{\vb{\omega}}_{\rm B}$ is the time derivative of
  $\vb{\omega}_{\rm B}$.
\end{framed}

A fundamental law from classical mechanics relates the rate of change of the
angular momentum vector $\vb{L}_{\rm I}$ to the applied torque $\vb{N}_{\rm I}$:
\begin{equation}
  \label{eq:ang_momentum_torque}
  \vbd{L}_{\rm I} = \vb{N}_{\rm I}  
\end{equation}
For a closed system (no exogenous torques), this leads to the law of
conservation of angular momentum:
\begin{equation}
  \label{eq:cons_ang_mom}
\vb{L}_{\rm I} = \sum_i \vb{L}_{{\rm I}i} = \mbox{const} 
\end{equation}
Here, $\vb{L}_{{\rm I}i}$ is the $i$th component of a system's total angular
momentum.

The angular momentum of single particle with mass $m$ at location $\vb{r}$ having
velocity $\vb{v}$ is
\begin{equation}
  \label{eq:single_part}
  \vb{L} = \vb{r} \times m \vb{v} 
\end{equation}
From this we can infer that the units of $\vb{L}$ are kg m$^2$ s$^{-1}$, and
the units of the inertia matrix $J$ are kg m$^2$.

Suppose we wish to render (\ref{eq:ang_momentum_torque}) in body coordinates.
Recall from Section \ref{sec:equat-vect-moving} that for any arbitrary vector
$\vb{l}$ we can write
\[\vbd{l}_{\rm B} = -\vb{\omega}_{\rm B} \times \vb{l}_{\rm B} + {C}
  \vbd{l}_{\rm I}\] This relationship must also apply for the vector $\vb{L}$.
Thus,
\begin{equation}
  \label{eq:euler_proto}
  \vbd{L}_{\rm B} = -\vb{\omega}_{\rm B} \times  \vb{L}_{\rm B} + {C}
  \vbd{L}_{\rm I}
\end{equation}
Recognizing that ${C}\vbd{L}_{\rm I} = C \vb{N}_{\rm I} = \vb{N}_{\rm B}$, we
obtain Euler's famous equation governing the evolution of angular momentum as
expressed in the body frame:
\begin{equation}
  \label{eq:euler_final}
  \vbd{L}_{\rm B} = -\vb{\omega}_{\rm B} \times  \vb{L}_{\rm B} + \vb{N}_{\rm B}
\end{equation}
Recognizing that $\vb{L}_{\rm B} = J \vb{\omega}_{\rm B}$, we can write this as
\begin{equation}
  \label{eq:euler_final_reorg1}
  \dot{J} \vb{\omega}_{\rm B} + J \vbd{\omega}_{\rm B}  = \vb{N}_{\rm B}  -
  [\vb{\omega}_{\rm B} \times]  J \vb{\omega}_{\rm B}
\end{equation}
If the inertial properties of the body are constant, then $\dot{J} = 0$ and we
have
\begin{equation}
  \label{eq:euler_final_reorg2}
  \vbd{\omega}_{\rm B}  = J^{-1} \left( \vb{N}_{\rm B}  - [\vb{\omega}_{\rm B}
    \times]  J \vb{\omega}_{\rm B}\right)
\end{equation}
Hence, Euler's equation describes the evolution of $\vb{\omega}_{\rm B}$ in
time.  It is the foundation of attitude estimation and control

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:
