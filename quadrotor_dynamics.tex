\chapter{Quadrotor Dynamics}
\label{cha:quadrotor-dynamics}

\section{Review of Newtonian Mechanics}
\paragraph{Center of Mass Theorem} The center of mass (CM) of a system of
particles---whether rigid or not---moves as if the entire mass were
concentrated at the CM and all external forces acted there.  In mathematical
terms,
\begin{equation}
  \label{eq:translational_motion}
  {m} \vbdd{r}_{\rm I} = \vb{F}_{\rm I} = \sum_i \vb{F}_{i\text{I}}
\end{equation}
\[\ba{ll}
m & \mbox{total mass} \\
\rI & \mbox{position of the center of mass (CM), expressed in I} \\
\vb{F}_{\rm I} = \sum_i \vb{F}_{i\text{I}} & \mbox{sum of all external forces
  acting on the system, expressed in I}
\ea \]

\paragraph{Angular Momentum and Torque} Consider a system composed of many
particles.  The $i$th particle has position $\vb{r}_{i\text{I}}$ and velocity
$\vb{v}_{i\text{I}} = \vbd{r}_{i\text{I}}$, both relative to an inertial frame
I, and mass $m_i$.  Its angular momentum relative to the origin of I is
\begin{equation}
  \label{eq:ang_momentum_def}
  \vb{L}_{i\text{I}} \triangleq  \vb{r}_{i\text{I}} \times m_i \vb{v}_{i\text{I}}
\end{equation}
Let $\vb{F}_{i\text{I}}$ be the sum of all forces acting on the $i$th
particle. Then the torque on the particle about the origin of I is
\begin{equation}
  \label{eq:torque_def}
  \vb{N}_{i\text{I}} \triangleq  \vb{r}_{i\text{I}} \times \vb{F}_{i\text{I}}
\end{equation}

\paragraph{Angular Momentum Theorem}
The rate of change of the angular momentum of a system equals the sum of
external torques (moments) applied:
\begin{equation}
  \label{eq:ang_mom_thm}
  \vbd{L}_{\rm I} = \vb{N}_{\rm I} = \sum_i \vb{N}_{i\text{I}} 
\end{equation}
\[\ba{ll}
\vb{L}_{\rm I} & \mbox{angular momentum, expressed in I} \\
 \vb{N}_{\rm I} = \sum_i \vb{N}_{i\text{I}} & \mbox{sum of all external torques,
   expressed in I}
\ea \]

\section{Evolution of Quadrotor State}
\subsection{State Definition}
Let us define the state of our quadrotor as 
\begin{align*}
  \vb{X} = \begin{bmatrix} \rI \\ \vb{e} \\ \vb{v}_{\rm I} \\ \vb{\omega}_{\rm B} \end{bmatrix}
\end{align*}
where $\rI$ and $\vb{v}_{\rm I}$ were defined above,
$\vb{\omega}_{\rm B}$ is the angular rate expressed in B, and $\vb{e}$ is a
vector of 3-1-2 Euler angles that describe the attitude of B with respect to I:
\begin{align*}
  \vb{e} = \begin{bmatrix} \phi \\ \theta \\ \psi \end{bmatrix}
\end{align*}
\paragraph{Goal} Starting with an initial state $\vb{X}(0)$, we wish to know
how $\vb{X}$ evolves in time.  In other words, we wish to find $\vb{X}(t)$ for
$0<t\leq t_\text{f}$, where $t_\text{f}$ is some final time.

\subsection{Reference Frames}
\begin{figure}[h!]
  \centering
  \includegraphics[width=0.4\textwidth,trim={0 0 -80 0},clip]{figs/inertial_frame.pdf}
  \includegraphics[width=0.45\textwidth]{figs/body_frame.pdf}
  \label{fig:inertial_frame}
  \caption{Inertial (left) and body (right) reference frames.}
\end{figure}
\paragraph{Inertial Reference Frame} 
We take a local East-North-Up (ENU) reference frame as our inertial frame I.
Although this frame is not actually inertial (it's fixed on a rotating Earth,
for one thing), it's close enough to inertial to fool low-to-moderate-quality
inertial sensors.  The axes of I, denoted $\vb{X}, \vb{Y}$, and $\vb{Z}$, are
aligned respectively with the East, North, and Up directions.

\paragraph{Body Reference Frame}
The body reference frame B has its origin at the quadrotor CM.  Its axes are
denoted $\vb{x}, \vb{y}$, and $\vb{z}$.  The plane spanned by
$\{\vb{x},\vb{y}\}$ passes through the CM and is parallel to the plane passing
through the tip of each rotor shaft.  The $\vb{x}$ axis identifies the nominal
forward direction of the quad.  The $\vb{z}$ axis completes the right-handed
triad.

Note that \[\vb{X}_{\rm I} = \vb x_{\rm B} = \vb{e}_1 = \begin{bmatrix} 1 \\ 0
    \\ 0 \end{bmatrix}, \quad
 \vb Y_{\rm I} = \vb y_{\rm B} = \vb{e}_2 = \begin{bmatrix} 0 \\ 1 \\
   0 \end{bmatrix}, \quad 
 \vb Z_{\rm I} = \vb z_{\rm B} = \vb{e}_3=  \begin{bmatrix} 0 \\ 0 \\ 1 \end{bmatrix}\]

\subsection{Definitions}
To account for the different spin directions of the quad's four propellers, let
$s_1 = s_3 = -1$ and $s_2 = s_4 = 1$.  Then, for $i=1,2,3,4$, define
\[\ba{ll}
\vb{r}_{i\text{B}} & \mbox{location of center of $i$th propeller in B} \\
\vb{F}_{i\text{B}} & \mbox{external force due to $i$th propeller, expressed B:
  $\vb{F}_{i\text{B}}=F_i \vb{z}_{\rm B}$, $F_i \geq 0$} \\
\vb{\omega}_{i\text{B}} & \mbox{angular velocity of $i$th rotor with respect to I,
  expressed in B: $\vb{\omega}_{i\text{B}}= s_i \omega_i \vb{z}_{\rm B}$,} \\ & \mbox{where
$\omega_i = |\dot{\theta}_i| \geq 0$ is the speed of the $i$th rotor.} \\
{J} & \mbox{inertia matrix for quad, expressed in B} \\
{R}_{\rm BI} & \mbox{attitude matrix for quad: ~$\forall \vb{w} \in \mathbb{R}^{3}$,
  ~$\vb{w}_{\rm B} = \RBI \vb{w}_{\rm I}$} \\
\vb{N}_{i\text{B}} & \mbox{torque caused by $i$th propeller spinning against air,
  expressed in B:} ~\vb{N}_{i\text{B}} = s_i N_i \vb{z}_{\rm B}, ~N_i \geq 0  \\
\vb{\omega}_{\rm B} & \mbox{angular rate vector of B with respect to  I, expressed in B} \\
\vb{d}_{\rm I} & \mbox{disturbance force acting on CM, expressed in I}
\ea \]
\subsection{Governing Equations}
\paragraph{Goal} We wish to find expressions for $\vbd{r}_{\rm I}$,
$\vbd{v}_{\rm I}$, $\vbd{\omega}_{\rm B}$, and $\vbd{e}$ (or
$\dot{R}_{\rm BI}$).  From these we can obtain $\vbd{X}(t)$.  Then, starting with
an initial state $\vb{X}(0)$, we can numerically integrate $\vbd{X}(t)$ to find
$\vb{X}(t)$ for $0<t\leq t_\text{f}$.
\begin{equation}
   \label{eq:ge_r}
  m\vbd{r}_{\rm I} = m \vb{v}_{\rm I}
\end{equation}

\begin{equation}
     \label{eq:ge_v}
     m\vbd{v}_{\rm I} = m\vbdd{r}_{\rm I} = -mg \vb{z}_{\rm I} + \sum_{i=1}^4
     \vb{F}_{i\text{I}} + \vb{d}_{\rm I} =  \begin{bmatrix} 0 \\ 0 \\ -mg \end{bmatrix} + R\T_{\rm BI} \sum^4_{i=1} \begin{bmatrix} 0 \\ 0 \\ F_i \end{bmatrix} + \vb{d}_{\rm I}
\end{equation}

\begin{equation}
     \label{eq:ge_omega}
  \vbd{\omega}_{\rm B} = J^{-1} \left(\vb{N}_{\rm B} -[\vb{\omega}_{\rm B} \times] J
  \vb{\omega}_{\rm B}\right) \quad \mbox{where} \quad \vb{N}_{\rm B}= \sum^4_{i=1} \left(\vb{N}_{i\text{B}} + \vb{r}_{i\text{B}} \times \vb{F}_{i\text{B}} \right)
\end{equation}

\begin{equation}
     \label{eq:ge_RBI}
  \dot{R}_{\rm BI}=-[\vb{\omega}_{\rm B} \times]\RBI
\end{equation}

\paragraph{Relating Forces and Torques to Rotor Rates}
The scalar forces $F_i$ and torques $N_i$ can be related to the scalar rotor
rates $\omega_i$ by the following approximations:
\[\ba{ccl}
F_i = k_F \omega_i^2 & \mbox{where  $k_F$ has units of} & \mbox{N/(rad/s)$^2$} \\
N_i = k_N \omega_i^2 & \mbox{where $k_N$ has units of} & \mbox{Nm/(rad/s)$^2$}
\ea \]

\subsection{Numerical Integration}
\paragraph{Goal} Numerically integrate $\vbd{X}(t)$ to find $\vb{X}(t)$ for
$0<t\leq t_\text{f}$ for some final time $t_\text{f}$.  

To avoid the singularity associated with our Euler angle attitude
representation $\vb{e}$, we define a new state that replaces $\vb{e}$ with the
whole $\RBI$ matrix unrolled column by column:
\[\vb{X}_{\rm BIG} = 
\begin{bmatrix} 
\rI \\ 
\vb{v}_{\rm I} \\ 
\RBI(1,1) \\
\RBI(2,1) \\ 
\vdots \\
\RBI(3,3) \\ 
\vb{\omega}_{\rm B}
\end{bmatrix}\]
Here, $\RBI(i,k)$ denotes the $(i,j)$th element of the $\RBI$
matrix (the element in the $i$th row and the $j$th column).

We can package (\ref{eq:ge_r})--(\ref{eq:ge_RBI}) into a single
first-order vector-valued ordinary differential equation (ODE):
\begin{equation}
  \label{eq:ode}
  \vbd{X}_{\rm BIG}(t) = \vb{f}_{\rm QUAD}\left(t,\vb{X}_{\rm BIG}, \vb{\omega}, \vb{d}_{\rm I}, P\right)
\end{equation}

where \[\vb{\omega} = 
\begin{bmatrix} 
\omega_{1} \\ 
\omega_{2} \\
\omega_{3} \\
\omega_{4} \\
\end{bmatrix} \] holds the scalar rotor rates, and $P$ is a structure holding
necessary parameters.

\begin{framed}
  An \emph{ordinary differential equation} involves a derivative with respect
  to one independent variable (e.g., $t$), whereas a \emph{partial
    differential equation} involves derivatives with respect to multiple
  independent variables.
\end{framed}

The simplest form of numerical integration, Euler integration, makes the
following approximation:
\begin{equation}
  \label{eq:euler_int}
  \vbd{X}_{\rm BIG}(t_k) \approx \frac{\vb{X}_{\rm BIG} (t_{k+1}) - \vb{X}_{\rm BIG} (t_k)}{\Delta t} , \quad \Delta t = t_{k+1} - t_k
\end{equation}
Thus, starting with $\vb{X}_{\rm BIG} (t_k)$, it obtains
$\vb{X}_{\rm BIG} (t_{k+1})$ by
\begin{equation}
  \label{eq:euler_sol}
  \vb{X}_{\rm BIG} (t_{k+1}) = \Delta t \cdot \vb{f}_{\rm QUAD} 
  \left[t_k, \vb{X}_{\rm BIG} (t_{k}), \vb{\omega} (t_k), \vb{d}_{\rm I}(t_k), P\right]
  +  \vb{X}_{\rm BIG} (t_{k})
\end{equation}
Euler integration becomes exact as in the limit as $\Delta t \rightarrow
0$. \textsc{Matlab}'s {\tt ode45} is a fancier algorithm that is more accurate
than Euler integration for the same $\Delta t$.

\section{Neglected Effects}

\newpage

\section{Model of Direct-Current Motor}
High-quality multi-rotor micro aerial vehicles (MAVs) employ brushless DC
motors.  Brushless motors are more expensive than brushed motors but are more
efficient: 85-90\% efficient vs. 75-80\%.  They also last longer, as there are
no brushes to wear out.  Switching between coils as the rotor angle advances is
accomplished by an electronic commutator rather than a mechanical one (brushes).

The \emph{stator} is the stationary part of a motor; the \emph{rotor} is the
rotary part.  

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.45\textwidth]{figs/dc_motor_model.pdf}
  \label{fig:dc_motor_model}
  \caption{Model of a DC motor: an electro-mechanical system.}
\end{figure}

\paragraph{Goal} We want to develop a system model of how input voltage
$e_a(t)$ affects rotor angle $\theta (t)$.  We need to model
\begin{itemize}
\item Mechanical part
\item Electrical part
\item Electro-mechanical coupling
\end{itemize}

\subsection{Mechanical Part}
A rotor with inertia is subject to torques from the motor and viscous drag:

\begin{minipage}{0.4\linewidth}
\begin{tikzpicture}
  \node[cylinder, black, shape border rotate=180, draw, minimum height=0.5cm,
  minimum width=1.5cm, aspect=0.5, cylinder uses custom fill,
  cylinder body fill=gray!30, cylinder end fill=gray!60, anchor=west] (cyl)
  at (2.5,0) {$J_m$};
  \draw[-Stealth,thick] ($(cyl.west)+(-0.3,0)$) arc
  [
  start angle=30,
  end angle=180,
  x radius=0.3cm,
  y radius =0.8cm
  ];
  \draw[-Stealth,thick] ($(cyl.west)+(-1,0)$) arc
  [
  start angle=30,
  end angle=180,
  x radius=0.3cm,
  y radius =0.8cm
  ];
  \draw[-Stealth,thick] ($(cyl.east)+(0.8,0)$) arc
  [
  start angle=30,
  end angle=180,
  x radius=0.3cm,
  y radius =0.8cm
  ];
  \node at ($(cyl.west)+(-0.45,0.65)$){$N_m$};
  \node at ($(cyl.west)+(-1.25,0.65)$){$\theta$};
  \node at ($(cyl.east)+(0.6,0.65)$){$N_d$};
\end{tikzpicture}
\end{minipage}
\begin{minipage}{0.3\linewidth}
  \begin{tabular}{ll}
    $\theta$ & rotor angle \\
    $N_{m}$ & motor torque \\
    $J_m$ & rotor moment of inertia (including propeller) \\
    $N_{d}$ & drag torque (due to internal drag)
  \end{tabular}
\end{minipage}

Rate of change of angular momentum:
\[{N}_{m} + {N}_{d} = {J}_{m}\ddot{\theta}\]
Governing law for viscous drag torque:
\[ N_{d} = -b \dot{\theta} \]
Combined dynamics equation:
\begin{equation}
  \label{eq:motor_mechanical_dynamic}
  J_{m} \ddot{\theta} + b \dot{\theta} = N_{m}
\end{equation}
  
\subsection{Electrical Part}
A lumped-element electrical model of a DC motor:

\begin{minipage}{0.55\textwidth}
  \begin{circuitikz}
  \draw (0,0)
  to[V,l=$e_a$, invert] (0,3)
  to[R,l=$R_a$,v=$e_R$] (3,3)
  to[short, i=$i_a$] (3.3,3)
  to[L,l=$L_a$,v=$e_L$] (6,3)
  to[V,l=$e_b$] (6,0)
  to[short] (0,0);
\end{circuitikz}
\end{minipage}
\begin{minipage}{0.4\textwidth}
  \begin{tabular}{ll}
    $e_a$ & ideal source voltage \\
    $R_a$ & resistance of armature coils \\
    $e_R$ & voltage across resistor \\
    $i_a$ & current through armature coils \\
    $L_a$ & inductance of armature coils \\
    $e_L$ & voltage across inductor \\
    $e_b$ & motor back EMF (electro-motive force) 
  \end{tabular}
\end{minipage}

In reality, these lumped elements are one physical device including rotating
motor coils, a switching mechanism (commutator), and static permanent magnets.

The voltages across the resistor $R_a$ and the inductor $L_a$ are subject to the
following governing laws:
\begin{align}
\label{eq:electrical_governing_laws}
{e}_{R}& = {R}_{a}{i}_{a} \\
{e}_{L} &= {L}_{a}\dfrac{{d}{i_{a}}}{dt}
\end{align}
From Kirchoff's law, the loop voltage must be zero:
\begin{equation}
  \label{eq:loop_voltage}
  \begin{split}
    0 &= {e}_{a} - {e}_{R} - {e}_{L} - {e}_{b} \\
    &= {e}_{a} - {R}_{a}{i}_{a} - {L}_{a}\dfrac{{d}{i_{a}}}{dt} - {e}_{b}
\end{split}
\end{equation}

\subsection{Electro-Mechanical Coupling}
Now we can examine the interplay between the electrical and the mechanical
parts.  The effect of the electrical part on the mechanical part is that motor
torque is proportional to current:
\begin{equation}
  \label{eq:e_on_m}
{N}_{m}=K{i}_{a}  
\end{equation}
The effect of the mechanical part on the electrical part is that back EMF is
proportional to rotor speed (this is known as the generator effect):
\begin{equation}
  \label{eq:m_on_e}
{e}_{b}={K}_{b}\dot{\theta}
\end{equation}
\[\ba{rl}
K & \mbox{has units of} \quad \dfrac{\rm N~m}{\mbox{amp}} \\
{K}_{b} & \mbox{has units of} \quad
\dfrac{\rm V}{\rm rad/s}=\dfrac{\dfrac{{\Delta}{\rm W}}{1 \mbox{~coulomb}}}{\rm
  rad/s}=
\dfrac{\rm N~m}{\mbox{amp}} 
\ea \]

Power considerations allow us to relate $K$ to $K_b$.
Electrical power dissipated through an element equals the current that flows
through the element multiplied by the voltage drop across the element.  Thus
the electrical power delivered to the motor is
\[{P}_{m} = {i}_{a}{e}_{b} =
  \dfrac{N_m}{K}{e}_{b}=\dfrac{N_m}{K}{K}_{b}\dot{\theta}\] Mechanical work for
a rotary system with displacement $\theta$ and constant torque $N_m$ is
$N_m\theta$.  Power is the time derivative of work.  Thus the power of the
(constant) torque acting on the motor is
\[{P}_{m} = {N}_{m}\dot{\theta} \]
Equating these expressions for $P_m$ reveals that
\[ K={K}_{b}\]
Putting the mechanical and electrical dynamics equations together, we now have
\begin{equation}
  \label{eq:mechanical_coupled}
  {J}_{m}\ddot{\theta}+b\dot{\theta}={K}{i}_{a}
\end{equation}
\begin{equation}
  \label{eq:electrical_coupled}
{L}_{a} \left(\frac{di_{a}}{dt} \right) + {R}_{a}i_{a} = {e}_{a} - K\dot{\theta} 
\end{equation}

\subsection{Voltage to angular rate transfer function}
Eqs. (\ref{eq:mechanical_coupled}) and (\ref{eq:electrical_coupled}) are
coupled ODEs. We could use an ODE solver to determine $\theta(t)$ for a given
${e}_{a}(t)$. Alternatively, we can assume zero initial conditions and 
apply the Laplace transform:
\begin{equation}
  \label{eq:laplace_tranform_power_1}
  [{J}_{m}s^{2}+bs]\Theta(s)=K{I}_{a}(s) \\
\end{equation}
\begin{equation}
  \label{eq:laplace_tranform_power_2}
[{L}_{a}s+{R}_{a}]{I}_{a}(s)={E}_{a}(s) - Ks\Theta(s)\\
\end{equation}
Substituting \ref{eq:laplace_tranform_power_2} into
\ref{eq:laplace_tranform_power_1}, we have
\[[L_{a}s+R_{a}][J_{m}s^2+bs]\Theta(s)=K[E_{a}(s)-Ks\Theta(s)] \]
Multiplying and rearranging yields
\[
\left[{J}_{m}{L}_{a}{s}^{3} + ({J}_{m}{R}_{a}+{L}_{a}{b}){s}^{2} + ({R}_{a}{b}+{K}^{2})s\right] \Theta(s)= K{E}_{a}(s) 
\]
which can be written as a transfer function:
\begin{equation}
  \label{eq:dc_motor_trans_fcn}
G_\theta(s) \triangleq \dfrac{\Theta(s)}{E_{a}(s)}=\dfrac{K}{s[J_{m}L_{a}s^2+(J_{m}R_{a}+L_{a}b)s+R_{a}b+K^2]} 
\end{equation}
We recognize this as a 3rd-order system.

Typically, $L_{a}$ is small. Assume $L_{a} = 0$, then
\begin{equation}
  \label{eq:transf_fcn_Lazero}
G_\theta(s) = \dfrac{\dfrac{K}{R_{a}b+K^2}}{\left(\dfrac{J_{m}R_{a}}{R_{a}b+K^2}\right)s^2+s}  
\end{equation}
Our interest is not with rotor angular displacement $\theta$ but rotor
angular rate $\omega = \dot\theta$, whose Laplace transform $\Omega(s)$ is
related to $G_\theta(s)$ and $E_a(s)$ by \[\Omega(s) = sG_\theta(s)E_a(s) \]
Thus, the transfer function from $E_a(s)$ to $\Omega(s)$ is 
\begin{align}
  \label{eq:to_omega_transfer_fcn}
G_\omega(s) \define \frac{\Omega(s)}{E_a(s)} & =
  \dfrac{\dfrac{K}{R_{a}b+K^2}}{\left(\dfrac{J_{m}R_{a}}{R_{a}b+K^2}\right)s+1}
  \\
  & = \frac{c_m}{\tau_m s + 1}
\end{align}
We recognize this as a first-order transfer function with time constant
$\tau_m$.  The response of $G_\omega(s)$ to a unit step input (a signal whose
Laplace transform is $1/s$) looks like this when plotted in the time domain:
\begin{center}
\includegraphics[width=0.55\textwidth]{figs/unit_step_response}
\end{center}
We can apply the final value theorem to find the asymptotic value of
$\omega(t)$:
\[ \lim_{s \rightarrow 0} G_\omega(s) \frac{1}{s} = c_m \] Within $\tau_m$
seconds, the rotor rate reaches $c_m(1 - e^{-1})$, or about 63\% of its final
value.

\newpage

\section{Relating Thrust to Rotor Rate}
We previously defined the following relationship between the $i$th rotor's
angular rate $\omega_i$ and the thrust and torque it produces:
\[\ba{ccl}
  F_i = k_F \omega_i^2 & \mbox{where  $k_F$ has units of} & \mbox{N/(rad/s)$^2$} \\
  N_i = k_N \omega_i^2 & \mbox{where $k_N$ has units of} & \mbox{Nm/(rad/s)$^2$}
  \ea \] In this section, we will take a closer look at $k_F$ and $k_N$.  Our
analysis is based on a balance of two powers:
\[\ba{ll}
  P_m = N_m \omega & \mbox{power delivered to the motor} \\
  P_a = F \Delta v_h & \mbox{power delivered to the air} \ea \] where
\(\Delta v_h\) represents the change in airspeed induced by the propellers.
(For notational brevity we have temporarily dropped the $i$
subscripts.)  Assuming an initial air speed equal to zero, we can apply momentum
theory to express \(\Delta v_h\) as \cite{leishman2000principles}
\[
  \Delta v_h = \sqrt{\frac{F}{2\rho A}} = \sqrt{\frac{F}{2\rho \pi R^2}}
\]
where \(R\) is the prop radius and \(\rho\) is the mass density of the air.

One can show that in steady state, torque is proportional to thrust; i.e.,
\(N_m = k_T F\).  Then, assuming all the power delivered to the motor eventually goes
into the air, we equate \(P_m = P_a\):
\begin{align*}
  P_m &= P_a \\
  N_m \omega &= F
  \Delta v_h \\
   k_T F \omega & = \frac{F^{3/2}}{\sqrt{2\rho A}}  
\end{align*}
Replacing the subscript $i$ for the $i$th rotor, it follows that
\[
  F_i = \underbrace{2\rho A_i k_T^2}_{k_F} \omega_i^2 = k_F \omega_i^2
\]
  and
\[
  N_i = k_T  F_i = \underbrace{k_Tk_F}_{k_N} \omega_i^2 = k_N \omega_i^2
\]
\begin{rem}~
\begin{enumerate}
\item Steady-state $\omega_i$ is proportional to $e_{ai}$, the voltage
  applied to the $i$th motor.
\item Steady-state $\omega_i$ is achieved after about $4\tau_m$.
\item $N_i$ and $F_i$ are both proportional to $A_i$, and $\omega_i^2$.  
\end{enumerate}
\end{rem}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:

% LocalWords:  ccl Nm rl Kirchoff's di Eqs ODEs bs sG
