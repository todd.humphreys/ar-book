\chapter{Nonlinear Estimation: EKF vs. UKF}
\label{cha:nonl-state-estim}
The measurement model (\ref{eq:measModelNL}) and the dynamics model
(\ref{eq:dynamModelNL}) are both nonlinear in the state $\vb{x}(k)$, so a
standard linear sequential state estimator such as the Kalman filter cannot be
applied to estimate the state from noisy measurements $\vb{z}(k)$,
$k = 0, 1, ...$.  Instead, some nonlinear estimator must be used.  The extended
Kalman filter (EKF) and the unscented Kalman filter (UKF) are two commonly-used
nonlinear state estimators.  This chapter gives a brief overview of the EKF and
UKF.  For further details, see the comparison in \cite{gustafsson2011some}.

\section{Prediction Step}
Suppose we start with an estimate of $\vb{x}(k)$.  Our estimate, denoted
$\vbh{x}(k)$, isn't perfect, but it's correct on average.  In other words, the
average of the estimation error $\vbt{x}(k) = \vbh{x}(k) - \vb{x}(k)$ is zero,
written $\E{\vbt{x}(k)} = \vb{0}$.  The spread of our estimated state about the
truth is quantified by the so-called the error covariance matrix
\[P(k) = \E{\vbt{x}(k) \vbt{x}\T(k)}\] We assume the estimation error
$\vbt{x}(k)$ is a Gaussian random variable.  As such it can be completely
characterized by its mean $\E{\vbt{x}(k)} = \vb{0}$ and its covariance $P(k)$.

The philosophy of both the EKF and the UKF is that the best estimate of
$\vb{x}(k)$ at any time $k$ is the so-called conditional mean: the expected
value of $\vb{x}(k)$ conditioned either on all the measurements up to time
$k-1$, yielding the \emph{prior} estimate $\vbb{x}(k)$, or on all the
measurements up to time $k$, yielding the \emph{posterior} estimate
$\vbh{x}(k)$.  The prior estimate's error covariance matrix is denoted
$\bar{P}(k)$, whereas the posterior estimate's is $P(k)$.

The \emph{prediction step} of a state estimator takes $\vbh{x}(k)$ and $P(k)$
and propagates these forward based on the dynamics model (\ref{eq:dynamModelNL})
to get the best state estimate at time $k+1$ based on all the measurements up
through time $k$, denoted $\vbb{x}(k+1)$, and its associated error covariance,
denoted $\bar{P}(k+1)$.

\subsection{EKF Approach}
The EKF propagates the state estimate $\vbh{x}(k)$ directly through $\vb{f}$,
but propagates the state error covariance $P(k)$ using a linearized
approximation of $\vb{f}$.  Specifically, it expands $\vb{f}$ in a Taylor series
and retains only up to the linear term.  Its propagation looks like this:
\begin{align}
  \vbb{x}(k+1) &= \vb{f}\left[\vbh{x}(k), \vb{u}(k), \vbh{v}(k) \right] \\
  \bar{P}(k+1) &= F(k) P(k) F\T(k) + \Gamma(k) Q(k) \Gamma\T(k)
\end{align}
where $\vbh{v}(k) = 0$ is our best guess of $\vb{v}(k)$ at time $k$, and where
$F(k)$ and $\Gamma(k)$ are Jacobians of $\vb{f}$ with respect to $\vb{x}$ and
$\vb{v}$:
\begin{align}
  F(k) &= \left[\left.\frac{\partial \vb{f}}{\partial
  \vb{x}}\right|_{\vbh{x}(k),\vb{u}(k), \vbh{v}(k)} \right] \\
  \Gamma(k) &= \left[\left.\frac{\partial \vb{f}}{\partial
  \vb{v}}\right|_{\vbh{x}(k),\vb{u}(k), \vbh{v}(k)} \right] 
\end{align}

\subsection{UKF Approach}
The UKF's approach to the prediction step is not based on explicit
linearization.  Instead, the UKF invokes the so-called unscented transform (UT),
which creates a set of specially-chosen sample points called sigma points that
are meant to represent possible values of the true state $\vb{x}(k)$ and of the
true process noise $\vb{v}(k)$.  It sends these points, together with
$\vb{u}(k)$, through the dynamics function $\vb{f}$ to yield a set of projected
points:
\begin{center}
\includegraphics[width=\textwidth]{figs/ukfProp}
\end{center}
Roughly speaking, the updated state estimate $\vbb{x}(k+1)$ is formed from the
sample mean of the projected points, and its error covariance $\bar{P}(k+1)$ is
formed from the points' sample covariance.

The beauty of the UKF is that no Jacobians are required to calculate
$\vbb{x}(k+1)$ and $\bar{P}(k+1)$, so we avoid laborious and error-prone
derivative calculations.  What is more, the UKF is able to capture second-order
effects of $\vb{f}$ in $\vbb{x}(k+1)$ and $\bar{P}(k+1)$, which makes these
quantities more accurate than the ones calculated by the EKF.

\section{Measurement Update}
At time $k+1$, suppose we receive a measurement modeled as
\begin{align}
  \label{eq:linInMeasNoise}
\vb{z}(k+1) = \vb{h}\left[\vb{x}(k+1) \right] + \vb{w}(k+1)  
\end{align}
Because $\vb{z}(k+1)$ depends on $\vb{x}(k+1)$, it follows that they are
correlated.  Dropping the $(k+1)$ for brevity, let
$\vbb{z} = \vb{h}\left[\vbb{x}\right]$ and define a new vector $\vb{y}$ that is
the vertical concatenation of $\vb{x}$ and $\vb{z}$:
\[\vb{y} \define \left[\ba{c} \vb{x} \\ \vb{z} \ea \right], \quad \vbb{y}
  \define \E{\vb{y}} = \left[\ba{c} \vbb{x} \\ \vbb{z} \ea \right] \] Suppose we
represent the covariance of $\vb{y}$ as
\[P_{yy} \define \E{(\vb{y} - \vbb{y})(\vb{y} - \vbb{y})\T} = \left[\ba{cc}
    P_{xx} & P_{xz} \\ P\T_{xz} & P_{zz} \ea \right] \] It turns out that one
way to get the conditional mean of $\vb{x}(k+1)$---an estimate that is informed
not only by the prediction $\vbb{x}(k+1)$ but also by the measurement
$\vb{z}(k+1)$---is to blend the prediction and measurement together in the
so-called linear minimum-mean-square-error (LMMSE) measurement update formulas:
\begin{align}
  \label{eq:LMMSEformula1}
  \vbh{x}(k+1) &= \vbb{x}(k+1) + P_{xz}P^{-1}_{zz}\left[\vb{z}(k+1) -
                 \vbb{z}(k+1) \right] \\
  \label{eq:LMMSEformula2}
  P(k+1) &= \bar{P}(k+1) - P_{xz} P^{-1}_{zz} P\T_{xz}
\end{align}
This formula takes advantage of the correlation between $\vb{x}(k+1)$ and
$\vb{z}(k+1)$ to update the predicted state $\vbb{x}(k+1)$ with new information
provided by the measurements $\vb{z}(k+1)$.  Both the EKF and the UKF invoke
this formula, but they do so in different ways.  

\subsection{EKF Approach}
The EKF takes the Jacobian of $\vb{h}$ with respect to the state $\vb{x}$ and
uses this to calculate the LMMSE update.  Let the Jacobian be defined as
\[H(k+1) \define \left[\left.\frac{\partial \vb{h}}{\partial
        \vb{x}}\right|_{\vbb{x}(k+1)}\right] \] Recall that
$R(k+1) = \E{\vb{w}(k+1)\vb{w}\T(k+1)}$ is the covariance of the measurement
noise.  With a little work, one can show that $P_{zz}$ and $P_{xz}$ in the LMMSE
formulas above can be approximated in terms of $H(k+1)$ and $R(k+1)$ as follows:
\begin{align}
  \label{eq:EKF_Pxz_Pzz}
  P_{zz} &= H(k+1)\bar{P}(k+1)H\T(k+1) + R(k+1) \\
  P_{xz} &= \bar{P}(k+1)H\T(k+1)
\end{align}
The EKF plugs these expressions into the LMMSE formulas to obtain the updated
state estimate $\vbh{x}(k+1)$ and its covariance $P(k+1)$.

\subsection{UKF Approach}
Instead of calculating the Jacobian $H(k+1)$, the UKF again invokes the
unscented transform.  It creates a set of sigma points that are meant to
represent possible values of the true state $\vb{x}(k+1)$ and of the true
measurement noise $\vb{w}(k+1)$.  It sends these points through the measurement
function $\vb{h}$ to yield a set of transformed points:
\begin{center}
\includegraphics[width=0.9\textwidth]{figs/ukfMeas}
\end{center}
Roughly speaking, the measurement covariance matrix $P_{zz}$ is formed from the
sample covariance of the transformed points, and the covariance matrix $P_{xz}$
is formed from the cross-sample covariance between the sigma points for
$\vbb{x}(k+1)$ and the transformed points.  Once these are calculated, the UKF
plugs them expressions into the LMMSE formulas in (\ref{eq:LMMSEformula1}) and
(\ref{eq:LMMSEformula2}) to obtain the updated state estimate $\vbh{x}(k+1)$ and
its covariance $P(k+1)$.

Note that the UKF approach works whether the measurements are linear in the
measurement noise, as in (\ref{eq:linInMeasNoise}), or nonlinear, as in
\begin{align}
  \label{eq:nlinInMeasNoise}
\vb{z}(k+1) = \vb{h}\left[\vb{x}(k+1), \vb{w}(k+1)\right]
\end{align}

\section{Iteration}
For both the EKF and UKF, once $\vbh{x}(k+1)$ and $P(k+1)$ have been found, we
set $k \leftarrow k+1$ and start again with the prediction step.  We repeat this
prediction-step-then-measurement-update pattern until all data $\vb{z}(k)$ are
exhausted.

\section{Solving Wahba's Problem to Obtain an Initial Attitude Estimate}
Both the EKF and UKF require an initial state estimate at time $k = 0$, written
$\vbh{x}(0)$.  If this initial estimate is too far from the truth, in other
words, if the initial state error $\vbt{x}(0) = \vbh{x}(0) - \vb{x}(0)$ is too
large, then the EKF's Taylor series expansions will be a poor approximation of
the nonlinear dynamics function $\vb{f}$ and measurement function $\vb{h}$,
causing the EKF to diverge.

The UKF can handle a large $\vbt{x}(0)$ better than the EKF, but even it may
diverge for a sufficiently large $\vbt{x}(0)$ because its sigma points won't be
located near the true state.

Among the state elements shown in (\ref{eq:state15}), errors in the attitude,
represented by $\vb{e}$, are most likely to cause divergence because $\vb{f}$
and $\vb{h}$ are highly nonlinear functions of $\vb{e}$.  Thus, to prevent
estimator divergence, we need a way to get a fairly accurate initial estimate of
our quadrotor's attitude $R_\text{BI}$.  We can do this by measuring at least
two vectors in the B frame whose directions we know in the I frame.

The general problem for estimating the attitude matrix $R_\text{BI}$ from a set
of vectors measured in both B and I (or any pair of frames) was posed by Grace
Wahba in 1965.  Grace was at the time a graduate student at Stanford and posed
her famous problem while at an internship with IBM studying spacecraft attitude
problems.

Wahba's problem can be stated as follows: Suppose we have $N$ physical vectors
$\left\{ \vb{v}_1, \vb{v}_2, \dots, \vb{v}_N \right\}$ which we know perfectly
in I but can only measure imperfectly in B.  Let $\vbt{v}^u_{i\text{B}}$ be the
imperfect measurement of $\vb{v}_{i\text{B}}$, normalized to have unit length,
and $\vb{v}^u_{i\text{I}}$ be the corresponding known unit vector in I, for
$i = 1, \dots, N$.  Find the orthonormal matrix $R_\text{BI}$ that minimizes the
cost function
\begin{equation}
  \label{eq:wahbaCost}
  J_w(R_\text{BI}) = \frac{1}{2} \sum_{i = 1}^N a_i || \vbt{v}^u_{i\text{B}} -
  R_\text{BI} \vb{v}^u_{i\text{I}} ||^2
\end{equation}
where the weights $\left\{a_1, a_2, \dots, a_N\right\}$ are chosen to emphasize
the cost contribution more heavily for vectors measured more accurately.

Many researchers, including Wahba herself, have offered solutions to this
problem \cite{markley2000quaternion}.  The gold standard solution in terms of
accuracy and analytic insight is based on the singular value decomposition
(SVD), a particularly stable and versatile matrix factorization technique.

Form the matrix $B$ by
\[ B = \sum_{i=1}^N a_i \vbt{v}^u_{i\text{B}} \left(\vb{v}^u_{i\text{I}}\right)\T \]  Now
decompose B using the SVD:
\[ U S V\T = B \]  In \textsc{Matlab}, the decomposition is performed as
\begin{verbatim}
[U,S,V] = svd(B)
\end{verbatim}
The matrices $U$ and $V$ are orthonormal, while $S$, a diagonal matrix, holds
the \emph{singular values} associated with $B$.  The best estimate of
$R_\text{BI}$ is simply the product of $U$ and $V\T$, with a matrix
$M = \text{diag}(1, 1, \det{(U)}\det{(V)})$ interposed to ensure that the result
is a proper orthogonal matrix:
\[R_\text{BI} = U M V\T\]


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:

% LocalWords:  yy xz zz LMMSE Wahba's EKF's Wahba SVD diag
