all:
	latexmk -pdf main

clean:
	rm -f *.aux *.bbl *.log *.blg *.dvi *.ps *.out *.fls *.fdb_latexmk *.toc
