\chapter{Sensor Models}
\label{chap:sensor_models}

Our quad platform has three primary sensor systems: GNSS, a camera, and an
inertial sensor.  We will develop measurement models for each.  As preparation,
we will briefly review some fundamentals of probability theory.

\section{Probability Theory Basics}
\subsection{Random Variables}
Probability theory begins with the notion of a random variable, which is a
mapping (function) that yields a numerical value for each possible outcome of
some random experiment.  For example, the random variable $X$ could take on the
value 1 when a tossed coin turns up heads, and 0 if tails.  Or $X$ could
represent heads by 1 and tails by -1.  By assigning numerical values to
outcomes, we make transform observations of random phenomena into statistics,
thus facilitating probabilistic calculations.
\begin{center}
\includegraphics[width=0.6\textwidth]{figs/randomVariable}
\end{center}

\subsection{Probability Distribution}
Suppose $D_1$ and $D_2$ are random variables that represent the value shown on
each of two dice when they are tossed.  Consider the sum of these, 
\[ Z = D_1 + D_2 \] Suppose the dice are fair and that we toss them a very large
number of times.  A histogram of the \emph{realized} values of $Z$ would look
like this:
\begin{center}
\includegraphics[width=0.65\textwidth,trim={0 160 0 0},clip]{figs/pmfOfSumOfDiceP}
\end{center}
We have normalized the histogram by the total number of tosses so that the
numbers on the vertical axis represent the probability that $Z$ equals $k$,
written $P(Z = k)$.  As one can see, the most likely outcome, called the
\emph{mode}, is $Z = 7$, which avid players of Settlers of Catan already know.

As shorthand, we can let $\mu_Z(k) \define P(Z = k)$.  Generalizing, we write
$\mu_Z(z_k)$, where $z_k$ for $k \in \mathbb{Z}$ are the possible values that
$Z$ can take on.  The function $\mu_Z(z_k)$ is called a \emph{probability mass
  function} or pmf of $Z$.  Due to our normalization, the pmf sums to unity:
\[ \sum_k \mu_Z(z_k) = 1\]

A pmf is one example of a \emph{probability distribution}.  If we allow the
random variable $Z$ to assume a continuum of values instead of the discrete ones
we have supposed so far, then we describe $Z$'s distribution by a
\emph{probability density function} or pdf, written $p_Z(z)$ with
$z \in \mathbb{R}$.  Like the pmf, the pdf integrates to unity:
\[ \int_{-\infty}^{\infty} p_Z(z) dz = 1\]

\subsection{Central Limit Theorem}
Suppose that, instead of two dice, we throw ten, so that
$Z = D_1 + D_2 + \dots + D_{10}$.  For a large number of realizations, the
normalized histogram for $Z$ in this case begins to look very much like the
famous Gaussian or bell curve:
\begin{center}
\includegraphics[width=0.7\textwidth,trim={0 160 0 0},clip]{figs/pmfOfSumOfDice10P}
\end{center}
In fact, as the number of dice thrown increases, the resulting pmf $\mu_Z(z_k)$
becomes indistinguishable from a Gaussian distribution.  A central pillar of
probability theory, the \emph{central limit theorem}, states that the sum of
independent random variables---even ones with different distributions---is
Gaussian-distributed in the limit of a large number of summands.  As we can see
from the figure above, $P(Z = k)$ looks awfully close to Gaussian even with only
10 summands.

This explains why Gaussian distributions arise so often in practice: Many
variables of interest---the weight of 5-year-old Chihuahuas, the temperature in
Austin on February 20---are the cumulative result of a large number of factors
that are significantly independent.

\subsection{Expected Value}
The \emph{expected value} of $Z$, written $\E{Z}$ or $\bar{Z}$, is also known as
the \emph{average} or \emph{mean} of $Z$.  It's the value we get by summing up
all the many realizations of $Z$ and dividing by the number of experiments.  It
can be calculated from the pmf as follows:
\[ \E{Z} = \sum_k z_k \mu_Z(z_k) \] For our example where $Z$ is the sum of the
values shown on two dice, $\E{Z} = 7$, which is obvious from the symmetry of the
pmf shown above.  Beware that the mean $\E{Z}$ is not always equal to the mode
(the most likely value) as in this case.

For a pdf, the expected value is
\[ \E{Z} = \int_{-\infty}^{\infty} z p_Z(z) dz\] The expected value operator
$\E{\cdot}$ is linear, which makes it easy to calculate the expected value of
sums of random variables.  Suppose $X$ and $Y$ are random variables, and
that $\alpha$ is a known (deterministic) scalar.  Then
\[\E{X + \alpha Y} = \E{X} + \alpha \E{Y} = \bar{X} + \alpha \bar{Y}\]
Calculating the expected value of a vector-valued random variable is also easy.
For $\vb{Z} = \left[Z_1, Z_2, \dots, Z_n\right]\T$, we have
\[ \E{\vb{Z}} = \left[\ba{c} \E{Z_1} \\ \E{Z_2} \\ \vdots \\ \E{Z_n} \ea
  \right] =  \left[\ba{c} \bar{Z}_1 \\ \bar{Z}_2 \\ \vdots \\ \bar{Z}_n \ea
  \right] \]

\subsection{Variance and Covariance}
The two most basic characteristics of a probability distribution are its mean,
which we have already discussed, and its \emph{variance}, which is a measure of
the spread about the mean.  The variance can be written in terms of the expected
value.  Taking $Z$ again as our random variable, the variance is
\[\text{Var}(Z) = \E{(Z - \bar{Z})^2} \]
As you can see, $\text{Var}(Z)$, also written $\sigma_Z^2$, is the average
squared deviation of $Z$ from its mean value $\bar{Z}$. This gets larger the
more widely spread the distribution.  The \emph{standard deviation} $\sigma_Z$
is simply the square root of the variance.

For two different random variables $Z$ and $X$, the \emph{covariance} is the
average product of the deviations from their respective means:
\[ \text{Cov}(Z,X) = \sigma_{ZX}^2 = \E{(Z - \bar{Z})(X - \bar{X})} \] If $Z$
and $X$ are \emph{independent} random variables, meaning that knowing the value
of one doesn't tell you anything about the value of the other, then
$\text{Cov}(Z,X) = 0$.  If $\text{Cov}(Z,X) \neq 0$, then we say that $Z$ and
$X$ are \emph{correlated}, whether negatively ($\text{Cov}(Z,X) < 0$) or
positively ($\text{Cov}(Z,X) > 0$).  A positive correlation might arise, for
example, if $X$ is the average daily temperature in Austin and $Z$ is the number
of ice cream cones sold there.

For discrete-valued random variables, the variance and covariance can be
calculated as
\begin{align*}
  \text{Var}(Z) &= \sum_k (z_k - \bar{Z})^2 \mu_Z(z_k)    \\
  \text{Cov}(Z,X) &= \sum_{k} \sum_{j} (z_k - \bar{Z})(x_j - \bar{X})  \mu_{ZX}(z_k, x_j)   
\end{align*}
where $\mu_{ZX}(z_k, x_j) \define P(Z = z_k, X = x_j)$ is the joint pmf of $Z$
and $X$: it's the probability that $Z = z_k$ \emph{and} that $X = x_j$.  Note
that $\text{Cov}(Z,Z) = \text{Var}(Z)$.

For continuous-valued random variables, we have
\begin{align*}
  \text{Var}(Z) &= \int_{-\infty}^{\infty} (z - \bar{Z})^2 p_Z(z) dz    \\
  \text{Cov}(Z,X) &= \int_{-\infty}^{\infty} \int_{-\infty}^{\infty} (z -
                    \bar{Z})(x - \bar{X})  p_{ZX}(z, x) dz dx   
\end{align*}
For a vector-valued random variable
$\vb{Z} = \left[Z_1, Z_2, \dots, Z_n\right]\T$, the \emph{covariance matrix} is
given by
\begin{align*}
  R_Z &= \E{(\vb{Z} - \vbb{Z})(\vb{Z} - \vbb{Z})\T}  \\
      &= \left[ \ba{cccc}
        \text{Var}(Z_1) & \text{Cov}(Z_1,Z_2)  &\cdots &\text{Cov}(Z_1,Z_n) \\
  \text{Cov}(Z_2,Z_1) & \text{Var}(Z_2)  & \cdots
                                              &\text{Cov}(Z_2,Z_n) \\
  \vdots & \vdots & \ddots & \vdots \\
  \text{Cov}(Z_n,Z_1) & \text{Cov}(Z_n,Z_2)  & \cdots
                        &\text{Var}(Z_n) \ea \right]
                          = \left[ \ba{cccc}
        \sigma^2_1 & \sigma^2_{12}  &\cdots &\sigma^2_{1n} \\
  \sigma^2_{21} & \sigma^2_2  & \cdots
                                              &\sigma^2_{2n} \\
  \vdots & \vdots & \ddots & \vdots \\
  \sigma^2_{n1} & \sigma^2_{n2}  & \cdots
                        &\sigma^2_n \ea \right]
\end{align*}
In the special case that the elements of $\vb{Z}$ are mutually independent, all
the off-diagonal elements of $R_Z$ will be zero:
$R_Z = \text{diag}(\sigma^2_1, \sigma^2_2, \dots, \sigma^2_n)$.

\subsection{Simulating Random Variables}
Suppose you wish to simulate a single realization of a scalar Gaussian random
variable $Z$ whose mean is $\bar{Z}$ and whose variance is $\sigma_Z^2$.  You
can do this in \textsc{Matlab} using the {\tt randn} function as follows:
\begin{verbatim}
z = sigmaZ*randn + Zbar;
\end{verbatim}
A second invocation of this command will yield another value {\tt z} that is
independent from the first.

Now suppose you wish to simulate a realization of a vector-valued Gaussian random
variable $\vb{Z}$ with mean $\vbb{Z}$ and covariance matrix $R_Z$.  This can be
done by
\begin{verbatim}
z = mvnrnd(Zbar, RZ);
\end{verbatim}
Once again, a second invocation of this command will yield another vector {\tt
  z} that is independent from the first.

\section{GNSS Measurements}
\subsection{The Primary Antenna Position Measurement Model}
Let G denote the Earth-centered, Earth-fixed (ECEF) reference frame.  The axes
$\vb{x}_\text{G}$ and $\vb{y}_\text{G}$ lie in the equatorial plane, with
$\vb{x}_\text{G}$ passing through the prime meridian, and $\vb{z}_\text{G}$ is
aligned with Earth's axis of rotation:
\begin{center}
\includegraphics[width=0.4\textwidth]{figs/ecef}
\end{center}

Denote by $\vb{r}_{0 \rm G}$ the fixed absolute location of a reference antenna
in G, and by $\vb{r}_{0p \rm G}(t_k)$ the absolute location of the quad's
primary antenna in G at time $t_k$.  Then the primary antenna location relative
to the reference antenna location at time $t_k$, expressed as a relative vector
in G, is
\[\vb{r}_{p \rm G}(t_k) = \vb{r}_{0p \rm G}(t_k) - \vb{r}_{0 \rm G}\]
The physical vector $\vb{r}_p$ is shown in red below.
\begin{center}
\includegraphics[width=0.65\textwidth]{figs/gnssVectors}
\end{center}
Likewise, let $\vb{r}_{s \rm G}$ denote the position of the quad's secondary
antenna relative to the reference antenna, expressed in G.  The ``baseline
vector'' pointing from the primary antenna to the secondary antenna,
expressed in G, is
\[ \vb{r}_{b \rm G} = \vb{r}_{s \rm G} - \vb{r}_{p \rm G} \]
The physical vector $\vb{r}_b$ is shown in blue in the figure above.

Of course, we can't obtain the true vector $\vb{r}_{p \rm G}(t_k)$ directly, we
can only \emph{measure} it by a process---in our case, carrier phase
differential global navigation satellite system (CDGNSS) positioning---that
introduces noise.  We model our measurement of $\vb{r}_{p \rm G}(t_k)$ as
\begin{equation}
  \label{eq:rpGmodel}
  \vbt{r}_{p\rm G}(t_k) = \vb{r}_{p\rm G}(t_k) + \vb{w}_{p\rm G}(t_k) 
\end{equation}
Here, $\vb{w}_{p \rm G}(t_k)$ is a time-independent discrete-time Gaussian noise
process that we model statistically as follows:
\[\E{\vb{w}_{p\rm G}(t_k)} = 0, \quad \E{\vb{w}_{p\rm G}(t_k)\vb{w}\T_{
      p\rm G}(t_j)} = R_{p \rm G} \delta_{kj} \] where $\delta_{kj}$ is the
Kronecker delta, which equals 1 when $k = j$ and 0 otherwise.  This noise model
implies that the noise on the 3-D CDGNSS measurement $\vbt{r}_{p \rm G}(t_k)$ is
zero mean (if you average over a bunch of noise samples $\vb{w}_{p \rm G}(t_k)$
you'll get something close to a zero vector), has a covariance matrix of
$R_{p \rm G}$, and is uncorrelated in time, which means that if you observe
$\vb{w}_{p \rm G}(t_k)$ you won't obtain any information about the value of
$\vb{w}_{p \rm G}(t_j)$ for $j \neq k$.

GNSS measurements are rendered natively in the G frame, but they are easier to
visualize and their covariance matrix has a simpler structure if expressed in a
local East-North-Up (ENU) frame L:
\begin{center}
\includegraphics[width=0.4\textwidth]{figs/enu}
\end{center}
The L frame isn't inertial, since it's fixed to the rotating and orbiting Earth,
but it's close enough that the inertial sensors we use in this course can't tell
the difference.  Therefore, in this course we will treat the L frame as inertial
and call it I.  The vector $\vb{r}_{0 \rm G}$ will be taken as the origin of the
L frame.

Let $R_\text{LG}$ be the attitude matrix that transforms coordinates from G to L.
With $R_\text{LG}$, the measurement model in (\ref{eq:rpGmodel}) can be rendered
in the L frame:
\begin{align*}
  \label{eq:rpLmodel}
  \vbt{r}_{p\rm L}(t_k) &= R_\text{LG} \vb{r}_{p\rm G}(t_k) + R_\text{LG}
                          \vb{w}_{p\rm G}(t_k) \\
&=  \vb{r}_{p\rm L}(t_k) +  \vb{w}_{p\rm L}(t_k) 
\end{align*}
The transformed noise has covariance matrix
\begin{align*}
  R_{p\rm L} &= \E{\vb{w}_{p\rm L}(t_k) \vb{w}_{p\rm L}\T(t_k)} \\
             &=  R_\text{LG} \E{\vb{w}_{p\rm G}(t_k) \vb{w}_{p\rm G}\T(t_k)}
               R_\text{LG}\T \\
    &=  R_\text{LG}  R_{p\rm G} R_\text{LG}\T 
\end{align*}
Due to average symmetry in the constellation of overhead GNSS satellites,
$R_{p\rm L}$ can be modeled as having a simple diagonal structure:
\[  R_{p\rm L} = \left[\ba{ccc} \sigma_\text{E}^2 & 0 & 0 \\
    0 & \sigma_\text{N}^2 & 0 \\
    0 & 0 & \sigma_\text{U}^2 \ea \right] \] CDGNSS positioning is remarkably
precise, with errors measured in millimeters!  We will assume the following
values for the standard deviations:
\begin{align*}
  \sigma_\text{E} = \sigma_\text{N} &= 6~ \text{mm} \\
  \sigma_\text{U} &= 12~ \text{mm}
\end{align*}
Errors in the Up direction are roughly double the size of those in the East and
North directions because satellites are visible only above the horizon, so the
Up direction is less constrained.

\subsection{The Baseline Vector Measurement Model}
Similar to the one for $\vb{r}_{p \rm G}(t_k)$, we can offer a measurement model
for the baseline vector $\vb{r}_{b \rm G}(t_k)$:
\begin{equation}
  \label{eq:rbGmodel}
  \vbt{r}_{b \rm G}(t_k) = \vb{r}_{b \rm G}(t_k) + \vb{w}_{b \rm G}(t_k) 
\end{equation}
The additive noise $\vb{w}_{b \rm G}(t_k)$ is modeled statistically as
follows:
\[\E{\vb{w}_{b \rm G}(t_k)} = 0, \quad \E{\vb{w}_{b \rm G}(t_k)\vb{w}\T_{b \rm
      G}(t_j)} = R_{b \rm G} \delta_{kj} \] The measurement model for
$\vbt{r}_{b \rm G}$ in (\ref{eq:rbGmodel}) looks similar to that of
$\vbt{r}_{p \rm G}$ in (\ref{eq:rpGmodel}), but there is an important
difference: $\vbt{r}_{b \rm G}$ has a known length, so it must satisfy the
constraint
\[ \| \vbt{r}_{b\rm G}\| = \| \vb{r}_{b\rm G}\| \] In other words, there is no
measurement error in the \emph{length} of $\vbt{r}_{b\rm G}$.  This is because
the mechanism by which $\vbt{r}_{b\rm G}$ is generated takes advantage of the
known distance between the two antennas.  To account for this constraint,
$R_{b\rm G}$ is formed in a special way:
\[ R_{b\rm G} = \| \vb{r}_{b\rm G}\|^2 \sigma_{b}^2[I_{3\times 3} - \vb{r}^u_{b
    \rm G} (\vb{r}^u_{b\rm G})\T] \] where
$\vb{r}^u_{b\rm G} = \vb{r}_{b\rm G}/\|\vb{r}_{b\rm G} \|$ is the unit norm
vector in the direction of $\vb{r}_{b \rm G}$, and $\sigma_{b}$, is the standard
deviation of the angular error (in radians) of the baseline vector measurement
$\vbt{r}_{b \rm B}$. Instead of having rank 3 like $R_{p\rm G}$ and
$R_{p\rm L}$, $R_{b\rm G}$ only has rank 2.  This is because the length
constraint eliminates one of its 3 degrees of freedom.

\section{Camera Measurements}
\label{sec:camera-measurements}
\subsection{Pinhole Camera Model}
We will adopt the \emph{pinhole} camera model in which reflected light from a
point $\vb{v}\in \mathbb{R}^3$ travels a perfectly rectilinear path to the
camera center $O_c$, which is also the origin of the camera (C) frame.
In an actual camera, the image sensor sits behind $O_c$ so that collected
light rays pass through $O_c$ before arriving at the sensor, but it is
conceptually simpler and functionally equivalent to place the image plane in
front of $O_c$ along the camera's principal axis.  The C frame's axes
$\left\{\vb{X}_c, \vb{Y}_c, \vb{Z}_c \right\}$ are such that the
$\vb{X}_c, \vb{Y}_c$ plane is parallel to the image plane, and the $\vb{Z}_c$
axis is the principal axis, aligned along the camera boresight:
\begin{center}
  \centering
\includegraphics[width=0.7\textwidth]{figs/cameraModel}
\end{center}
The image plane has its own two-dimensional coordinate system whose axes
$\vb{x}_m$ and $\vb{y}_m$ are aligned respectively with $\vb{X}$ and $\vb{Y}$.
Point $\vb{p} \in \mathbb{R}^2$, called the \emph{principal point} and taken to
be the origin of the image plane coordinate system, is the point at which the
principal axis pierces the image plane.  For simplicity, we will assume
$\vb{p} = 0$.  Point $\vb{x} \in \mathbb{R}^2$ is the pierce point of
$\vb{v}$.


The beauty of the pinhole camera model is that mapping 3D coordinates in C to 2D
coordinates on the image plane is a straightforward application of the laws of
similar triangles.  Consider a 3D point lying in the $\vb{Y,Z}$ plane, with
coordinates $Y$ and $Z$.  Suppose the image plane is located a distance $f$
along the principal axis away from $O_c$.  By similar triangles, the
pierce point's vertical coordinate in the image plane is $fY/Z$:
\begin{center}
\includegraphics[width=0.7\textwidth]{figs/similarTriangles}
\end{center}
Generalizing, we see that the point
$\vb{v}_\text{C} = \left[X_\text{C},Y_\text{C},Z_\text{C}\right]\T$ gets projected to
$\vb{x} = \left[x,y\right]\T$ by the mapping
\begin{align}
  \label{eq:nonlinearPinholeMapping}
x = \frac{fX_\text{C}}{Z_\text{C}}, \quad y = \frac{fY_\text{C}}{Z_\text{C}}   
\end{align}

\begin{rem}
  Note that when viewed from $O_c$, the image cast on the image plane by
  light sources in the environment is what would be seen naturally by an
  observer at $O_c$ with a limited field of view.  But the coordinate
  system $\left\{\vb{x}_m, \vb{y}_m\right\}$, when viewed from $O_c$, is
  unusual in that $\vb{x}_m$ points \emph{left} rather than right.  To remedy
  this, some computer vision textbooks and codes flip $\vb{x}_m$ and $\vb{X}$ so
  they point in the direction opposite the one shown in the figure.  But this
  results in a camera frame with a \emph{left-handed} basis, which leads to
  \emph{improper} attitude matrices, as discussed in Section
  \ref{sec:orth-transf}.  Recognizing a left-handed basis as the greater evil,
  we adopt the convention shown in the figure above.
\end{rem}

\subsection{Homogeneous Coordinates}
The mapping in (\ref{eq:nonlinearPinholeMapping}) is nonlinear since it involves
a quotient of input variables.  But we can make it appear linear by adopting
\emph{homogeneous coordinates}.  In homogeneous coordinates, a line in
$\mathbb{R}^2$ is represented by a vector $\vb{l} \in \mathbb{R}^3$ composed of
the line's three parameters:
\[ax + by + c = 0, \quad \vb{l} = \left[\ba{c} a\\b\\c \ea \right] \] Similarly,
a point in $\mathbb{R}^2$ with coordinates $x,y$ is represented by a vector
$\vb{x} \in \mathbb{R}^3$ by tacking on an extra coordinate equal to unity:
$\vb{x} = [x,y,1]\T$.  With this notation, it is easy to determine whether the
point represented by $\vb{x}$ lies on the line represented by $\vb{l}$: we have
only to calculate whether $\vb{x}\T \vb{l} = 0$, since
\[\vb{x}\T \vb{l} = [x,y,1]\left[\ba{c} a\\b\\c \ea \right] =
  ax + by+c = 0 \] In fact, this calculation would remain an effective test even
if $\vb{x}$ were defined as
\[\vb{x} = \left[\ba{c} \gamma x \\ \gamma y \\ \gamma \ea \right] \] for any
$\gamma \neq 0$.  We call $\vb{x}$ a homogeneous representation of the 2D-point
$[x,y]\T$.  For some arbitrary homogeneous representation
$\vb{x} = [x_1,x_2,x_3]\T$, the mapping from 3D homogeneous coordinates to 2D
Cartesian coordinates is
\begin{align}
  \label{eq:homogeneousMappingTo2D}
\vb{x} = \left[\ba{c} x_1\\x_2\\x_3 \ea \right] \mapsto \left[\ba{c} x_1/x_3
    \\ x_2/x_3 \ea \right]
\end{align}
A comparison of this mapping with the one in (\ref{eq:nonlinearPinholeMapping})
shows why homogeneous notation is so convenient for the pinhole camera model:
division by the third coordinate happens by definition when converting from
homogeneous coordinates to Cartesian coordinates.

With homogeneous coordinates, it is also easy to determine the point at the
intersection of two lines.  Suppose $\vb{l}_1, \vb{l}_2 \in \mathbb{R}^3$ are
the homogeneous representations of two lines in a plane.  Then the vector
$\vb{x} \in \mathbb{R}^3$, which represents in homogeneous coordinates the point at
which the lines intersect (assuming they do), is given by the cross product of
$\vb{l}_1$ and $\vb{l}_2$:
\[\vb{x} = \vb{l}_1 \times \vb{l}_2 \]

\begin{framed}
  {\bf What does \emph{homogeneous} mean in the context of homogeneous
    coordinates?}

  A homogeneous function $\vb{f}:\mathcal{V} \mapsto \mathcal{W}$ between vector
  spaces $\mathcal{V}$ and $\mathcal{W}$ is such that for a nonzero scalar
  $\alpha$, $\vb{f}(\alpha \vb{v}) = \alpha^k \vb{f}(\vb{v})$ for
  $\vb{v} \in \mathcal{V}$, where $k \in \mathbb{Z}$ is called the \emph{degree
    of homogeneity}.

  Thus, for example, $f(x,y) = 2x + 3y$ is homogeneous of degree $k=1$, and
  $f(x,y) = x^2 + y^2$ is homogeneous of degree $k=2$, but $f(x,y) = ax + by +
  c$ is not homogeneous due to the non-homogeneous term $c$.

  But if we write $ax + by + c$ in homogeneous coordinates, with $\vb{x}
  = \left[\gamma x, \gamma y, \gamma\right]$, so that $f(\vb{x}) = \gamma a x +
  \gamma b y + \gamma c$, then $f$ becomes a homogeneous function (of degree $k =
  1$).

  Thus, transforming into homogeneous coordinates is a way to homogenize a
  non-homogeneous function.
\end{framed}
\subsection{Pinhole Camera Model in Homogeneous Coordinates}
When $\vb{v}_\text{C}$ and $\vb{x}$ are represented in homogeneous coordinates,
the mapping from 3D camera-frame coordinates to 2D image-plane coordinates
becomes beautifully simple and linear.  Let $\vb{v}^h_\text{C}$ and $\vb{x}^h$
be $\vb{v}_\text{C}$ and $\vb{x}$ when rendered in homogeneous coordinates.
Then
\begin{align}
  \label{eq:homoMappingCtoImage}
  \vb{x}^h = \left[\ba{c} f X_\text{C} \\ f Y_\text{C} \\ Z_\text{C} \ea \right] &= \left[\ba{cccc} f & 0 & 0 & 0
    \\ 0 & f & 0 & 0 \\ 0 & 0& 1 &0 \ea \right] \left[\ba{c} X_\text{C} \\ Y_\text{C} \\ Z_\text{C}
  \\ 1 \ea \right] \\
  &= P_c \vb{v}_\text{C}^h  \nonumber 
\end{align}
where $P_c \in R^{3\times 4}$ is the camera \emph{projection matrix}.

\subsection{From the I Frame to the Image Plane}
\label{sec:from-i-frame}
Recall that $\vb{v}$ is the vector pointing from $O_c$ to a 3D feature
point. Let $\vb{X}$ be the vector pointing from $O_i$, the origin
of the I frame, to the same point:
\begin{center}
\includegraphics[width=0.5\textwidth]{figs/C_and_I_frames}
\end{center}
In practice, the coordinates of $\vb{X}$ may be known (e.g., from a surveying
campaign).  The mapping $\vb{x}^h = P_c\vb{v}_\text{C}^h$ maps
$\vb{v}_\text{C}^h$ to $\vb{x}^h$, the homogeneous coordinates of the pierce
point on the image plane.  The full camera measurement model maps
$\vb{X}_\text{I}^h$ to $\vb{v}_\text{C}^h$ and thence to the image plane.
Because $O_c$ and $O_i$ are not generally coincident, the mapping of a feature
point from I to C involves both a rotation and a translation.  Let $\vb{t}$ be
the vector pointing from $O_i$ to $O_c$.  Then $\vb{X}_\text{I}^h$ is mapped to
$\vb{v}_\text{C}^h$ by
\begin{align}
  \label{eq:homoMappingItoC}
  \vb{v}_\text{C}^h = \left[\ba{c} X_\text{C} \\ Y_\text{C} \\ Z_\text{C} \\ 1 \ea
    \right] &=
  \left[\ba{cc} R_\text{CI} & -\vb{t}_\text{C} \\
      0_{1\times 3} & 1 \ea \right] \left[\ba{c} X \\ Y \\ Z
  \\ 1 \ea \right] \\
  &=
  \left[\ba{cc} R_\text{CI} & -\vb{t}_\text{C} \\
      0_{1\times 3} & 1 \ea \right] \vb{X}_\text{I}^h \nonumber
\end{align}
Here, $0_{1\times 3}$ refers to a 1-by-3 matrix of zeros, and
$\vb{t}_\text{C} = R_\text{CI} \vb{t}_\text{I}$ is the translation vector
$\vb{t}$ expressed in C.  Homogeneous notation again proves convenient: the
joint translation and rotation operations are performed by a single matrix
multiplication.

Concatenating the mappings in (\ref{eq:homoMappingItoC}) and
(\ref{eq:homoMappingCtoImage}), we obtain the full homogeneous-coordinates
mapping from I to the image plane:
\begin{equation}
  \label{eq:homoMappingItoImage}
  \underbrace{\left[\ba{c} f X_\text{C} \\ f Y_\text{C} \\ Z_\text{C} \ea \right]}_{\vb{x}^h} =
  {\left[\left.\underbrace{\ba{ccc} f & 0 & 0
      \\ 0 & f & 0 \\ 0 & 0& 1 \ea}_{K} \right| \ba{c} 0 \\ 0\\ 0\ea \right]}
  {\left[\ba{cc} R_\text{CI} & -\vb{t}_\text{C} \\
      0_{1\times 3} & 1 \ea \right]} \underbrace{\left[\ba{c} X \\ Y \\ Z
      \\ 1 \ea \right]}_{\vb{X}^h_{\text{I}}} 
\end{equation}
Taking $K\in \mathbb{R}^{3\times 3}$ to represent the sub-matrix indicated above,
we can write this mapping even more compactly as
\begin{equation}
  \label{eq:homoMappingItoImageCompact}
  \vb{x}^h = K\left[R_\text{CI}, -\vb{t}_\text{C}\right] \vb{X}^h_{\text{I}} = P\vb{X}^h_{\text{I}}
\end{equation}
where $P = K\left[R_\text{CI}, -\vb{t}_\text{C}\right]$ is the
I-frame-to-image-plane projection matrix.  We say that $K$ contains the camera's
\emph{intrinsic} parameters (the distance $f$ and the principal point $\vb{p}$,
which is zero in our case), and that $\left[R_\text{CI}, -\vb{t}_\text{C}\right]$
contains the camera's \emph{extrinsic} parameters (its attitude and translation
with respect to I).

\subsection{Camera Measurement Model}
The final step in development of the camera measurement model is to convert from
homogeneous to standard Cartesian coordinates, scale the result, and add a vector
that models noise.  Starting with $\vb{x}^h = P \vb{X}^h_{\text{I}}$,
the Cartesian 2D image plane coordinates are obtained by
\begin{align}
     \label{eq:camera_meters1}
  \vb{x} = \left[\ba{c} x \\ y \ea \right] = \left[\ba{c} f X_\text{C}/Z_\text{C} \\ f Y_\text{C}/Z_\text{C}
  \ea \right] \quad \mathrm{(meters)}  
\end{align}
Assuming $x$ and $y$ have units of meters, we scale them by $1/p_s$ to convert
to units of pixels, where $p_s$ is the pixel size in meters.  We then add a
noise vector $\vb{w}_{c}$ to account for the fact that a feature's projection
may be blurred across several pixels and so cannot be measured exactly.  The
final camera measurement model is
\begin{align}
  \label{eq:cameraFinal}
  \vbt{x}_{c} = \left(\sfrac{1}{p_s}\right) \vb{x} + \vb{w}_{c} \quad
  \mathrm{(pixels)}
\end{align}
where $\vb{w}_{c}$ is modeled as zero-mean with covariance matrix $R_{c} \in
\mathbb{R}^{2 \times 2}$:
\[ \E{\vb{w}_{c}} = 0, \quad \E{\vb{w}_{c} \vb{w}\T_{c}} = R_{c} \]

\section{Inertial Measurements}
An inertial measurement unit (IMU) is a combination of accelerometers and
angular rate sensors (also known as rate gyros).  We will consider IMUs with a
3-axis accelerometer and a 3-axis angular rate sensor.

\subsection{Accelerometer Measurement Model}
\label{sec:accel-meas-model}
\qa{What do accelerometers measure?}{Not only acceleration!  They measure
  \emph{specific force}, which is a combination of acceleration and the
  mass-normalized force of gravity.}

Recall the equation for the acceleration of a point $P$ located at coordinates
$\vb{l}_{\rm B}$ as derived in Section \ref{sec:equat-vect-moving}:
\begin{align}
  R_{\rm BI}\vb{a}_{\rm I}  = \underbrace{R_{\rm BI}\vbdd{R}_{\rm I}}_{\rm
  accel.~of~B~origin} + \underbrace{\vbdd{l}_{\rm B}}_{\rm rel.~accel.} + \underbrace{(\vbd{\omega}_{\rm B}
  \times  \vb{l}_{\rm B})}_{\rm Euler}
  + \underbrace{2(\vb{\omega}_{\rm B} \times  \vbd{l}_{\rm B})}_{\rm Coriolis} + \underbrace{\vb{\omega}_{\rm
  B}\times(\vb{\omega}_{\rm B} \times \vb{l}_{\rm B})}_{\rm centripetal}
  \label{eq:rotating_frame_repeat}
\end{align}
where $\vb{a}_{\rm I} = \vbdd{r}_{\rm I}$ is the acceleration of $P$ with
respect to I.  If an IMU's accelerometer triad is located at $P$, and if the
accelerometer's axes are aligned with the B frame, and if the local ENU frame,
designated I, is approximated as an inertial frame, then the specific force
the accelerometer senses can be modeled as
\begin{align}
  \vbt{f}_{\rm B} = R_{\rm BI}(\vb{a}_{\rm I}  + g \vb{e}_3)
  + \vb{b}_a + \vb{v}_a
  \label{eq:accel_model_full}
\end{align}
The units of $\vbt{f}_{\rm B}$ are $m/s^2$. The random processes $\vb{b}_a$ and
$\vb{v}_a$ model accelerometer bias and noise.  One can see from
(\ref{eq:accel_model_full}) that the accelerometer senses both true inertial
acceleration $\vb{a}_{\rm I}$ and the mass-normalized force of gravity as if it
were an acceleration at a rate $g$ in the vertical direction.  This is a
consequence of Einstein's equivalence principle, which states that the effects
of acceleration and gravity on a particle are fundamentally indistinguishable.

One can substitute (\ref{eq:rotating_frame_repeat}) into
(\ref{eq:accel_model_full}) to obtain a detailed model of specific force for
the general case in which $\vb{l}$ is moving with respect to B.  But in
practice, two simplifications often arise.  First, the IMU is typically fixed in
the body frame, in which case $\vbd{l}_{\rm B} = \vbdd{l}_{\rm B} = \vb{0}$.
Second, the origin of B can be defined to coincide with the IMU's accelerometer
triad, in which case $\vb{l}_{\rm B} = \vb{0}$.  Taken together, these
simplifications cause the relative, Euler, Coriolis, and centripetal
acceleration terms in (\ref{eq:rotating_frame_repeat}) to vanish.

For convenience, we will invoke both of the foregoing simplifications by
assuming that the origin of B, the quadrotor center of mass CM, and the IMU
accelerometer triad are all co-located, in which case
$\vb{l}_{\rm B} = \vbd{l}_{\rm B} = \vbdd{l}_{\rm B} = \vb{0}$, and
(\ref{eq:rotating_frame_repeat}) reduces to
$R_{\rm BI} \vb{a}_{\rm I} = R_{\rm BI} \vbdd{R}_{\rm I}$.

The bias $\vb{b}_a$ is modeled as a first-order discrete-time Gauss-Markov
process
\begin{equation}
  \label{eq:gm_accel}
   \vb{b}_a(t_{k+1}) = \alpha_a \vb{b}_a(t_{k}) + \vb{v}_{a2}(t_k) 
\end{equation}
where $\alpha_a = e^{-\Delta t/\tau_a} \leq 1$ with $\Delta t = t_{k+1} - t_k$.
The parameter $\tau_a$ is called the correlation time of the Gauss-Markov
process. For finite $\tau_a$, $\alpha_a < 1$, which makes the process
mean-reverting; for infinite $\tau_a$, $\alpha_a = 1$, which makes the process
a so-called random-walk process.

The noise terms $\vb{v}_a$ and $\vb{v}_{a2}$ are modeled as white, Gaussian,
discrete-time, mutually-uncorrelated, random sequences.  As such, 
\begin{align*}
  \E{\vb{v}_a} = \E{\vb{v}_{a2}} &= \vb{0} \\
  \E{\vb{v}_a(t_k) \vb{v}_a\T(t_j)} &= Q_a \delta_{kj} \\
  \E{\vb{v}_{a2}(t_k) \vb{v}_{a2}\T(t_j)} &= Q_{a2} \delta_{kj} \\
  \E{\vb{v}_{a}(t_k) \vb{v}_{a2}\T(t_j)} &= \vb{0}~~ \forall~ k,j
\end{align*}
Here, $\delta_{kj}$ is the Kronecker delta, which equals 1 when $k = j$ and 0
otherwise.

\subsection{Rate Gyro Measurement Model}
\label{sec:rate-gyro-meas}
The rate gyro measurement model is 
\begin{equation}
  \label{eq:gyroModel}
  \vbt{\omega}_{\rm B} = \vb{\omega}_{\rm B} + \vb{b}_g + \vb{v}_g 
\end{equation}
The quantity $\vb{b}_g$ is the gyro bias.  Like $\vb{b}_a$, its value changes
slowly with time. We model it as a first-order Gauss-Markov process
\begin{equation}
  \label{eq:gmpg}
  \vb{b}_g(t_{k+1}) = \alpha_g \vb{b}_g(t_k) + \vb{v}_{g2}(t_k) 
\end{equation}
where $\alpha_g = e^{-\Delta t/\tau_g} \leq 1$.

The noise processes $\vb{v}_g(t_k)$ and $\vb{v}_{g2}(t_k)$ are modeled as
white, Gaussian, discrete-time, mutually-uncorrelated, random sequences. As
such,
\begin{align*}
   \E{\vb{v}_g} = \E{\vb{v}_{g2}} &= \vb{0} \\
  \E{\vb{v}_g(t_k) \vb{v}_g\T(t_j)} &= Q_g \delta_{kj} \\
  \E{\vb{v}_{g2}(t_k) \vb{v}_{g2}\T(t_j)} &= Q_{g2} \delta_{kj} \\
  \E{\vb{v}_{a}(t_k) \vb{v}_{g2}\T(t_j)} &= \vb{0}~~ \forall~ k,j
\end{align*}

We'll further assume that the rate gyro and accelerometer noise processes are
independent of each other, which implies that
\[E[\vb{v}_g(t_k)\vb{v}\T_{a}(t_j)] = E[\vb{v}_g(t_k)\vb{v}\T_{a2}(t_j)] =
  E[\vb{v}_{g2}(t_k)\vb{v}\T_{a}(t_j)] = E[\vb{v}_{g2}(t_k)\vb{v}\T_{a2}(t_j)]
  = \vb{0} \quad \forall~k,j \]

\subsection{IMU Noise and Bias Parameters}
\label{sec:imu-noise-bias}
The process below describes how one can obtain values for the parameters in the
foregoing IMU measurement models from the types of parameters typically offered
in IMU datasheets or academic studies.  

\subsubsection{Accelerometer}
$S_a$: Accelerometer white noise $(\mathrm{milli-g})^2$/Hz \\
$\sigma_a$: Accelerometer bias steady state standard deviation
$(\mathrm{milli-g})$ \\

Convert these to the quantities $Q_a$ and $Q_{a2}$ from Section
\ref{sec:accel-meas-model} as follows.  Let $g \approx 9.8$ be the magnitude of
local gravity in m/s$^2$, $\beta_a = g/1000$, and $\delta t_a$ be the averaging
interval for accelerometer measurements.  Then
\begin{align} \boxed{Q_a = \frac{\beta_a^2 S_a}{\delta t_a}} \quad
  (\mathrm{m/s^2})^2
\end{align}

If the accelerometer is performing proper internal anti-alias filtering, then
$\delta t_a$ should never be less than the  measurement interval
$\Delta t$.  It is often assumed that $\delta t_a = \Delta t$.

To obtain $Q_{a2}$, consider a scalar version of (\ref{eq:gm_accel}):
\[ b_a(k+1) = \alpha_a b_a(k) + v_{a2}(k) \] Then
\[ \sigma_a^2(k+1) \define \E{b_a^2(k+1)} = \alpha_a^2 \sigma_a^2(k) +
  \sigma_{a2}^2 \] In steady state for $\alpha_a < 1$ (the usual case),
$\sigma_a^2(k)$ will approach a constant:
\begin{align}
  \sigma_a^2 = \alpha_a^2 \sigma_a^2 + \sigma_{a2}^2
  \label{eq:sigma_a_scalar}
\end{align}
Solving for $\sigma_{a2}^2$, we have
\begin{align}
\sigma_{a2}^2 = \sigma_a^2 ( 1 - \alpha_a^2) 
  \label{eq:sigma_a2_scalar}
\end{align}
Converting to the units of $Q_{a2}$ and assuming that (\ref{eq:sigma_a2_scalar})
applies for each of the three independent elements of the vector $\vb{v}_{a2}$,
we have
\begin{align}
  \boxed{Q_{a2} = \beta_a^2 \sigma_{a2}^2 I_{3 \times 3}  = \beta_a^2
  \sigma_a^2 ( 1 - \alpha_a^2) I_{3 \times 3} } \quad (\mathrm{m/s^2})^2
\end{align}
The parameter $\alpha_a$ is related to the so-called time constant $\tau_a$ of
the accelerometer Gauss-Markov process: \[ \alpha_a = e^{-\Delta t/\tau_a} \]  IMU data
sheets rarely specify $\tau_a$, but it can be determined experimentally.  One
typically chooses it to be quite long (e.g., 100 seconds).

\subsubsection{Rate Gyro}
$S_g$: Gyro white noise $(\mathrm{deg/s})^2$/Hz \\
$\sigma_g$: Gyro bias steady state standard deviation
$(\mathrm{deg/h})$ \\

Convert these to the quantities $Q_g$ and $Q_{g2}$ from Section
\ref{sec:rate-gyro-meas} as follows.  Let $\beta_g = \pi/180$, and let
$\delta t_g$ be the averaging interval for gyro measurements.  Then
\begin{align} \boxed{Q_g = \frac{\beta_g^2 S_g}{\delta t_g}} \quad
  (\mathrm{rad/s})^2
\end{align}

If the gyro is performing proper internal anti-alias filtering, then
$\delta t_g$ should never be less than the  measurement interval
$\Delta t$.  It is often assumed that $\delta t_g = \Delta t$.

To obtain $Q_{g2}$, consider a scalar version of (\ref{eq:gmpg}):
\[ b_g(k+1) = \alpha_g b_g(k) + v_{g2}(k) \] Then
\[ \sigma_g^2(k+1) \define \E{b_g^2(k+1)} = \alpha_g^2 \sigma_g^2(k) +
  \sigma_{g2}^2 \] In steady state for $\alpha_g < 1$ (the usual case),
$\sigma_g^2(k)$ will approach a constant:
\begin{align}
  \sigma_g^2 = \alpha_g^2 \sigma_g^2 + \sigma_{g2}^2
  \label{eq:sigma_g_scalar}
\end{align}
Solving for $\sigma_{g2}^2$, we have
\begin{align}
\sigma_{g2}^2 = \sigma_g^2 ( 1 - \alpha_g^2) 
  \label{eq:sigma_g2_scalar}
\end{align}
Converting to the units of $Q_{g2}$ and assuming that (\ref{eq:sigma_g2_scalar})
applies for each of the three independent elements of the vector $\vb{v}_{g2}$,
we have
\begin{align}
  \boxed{Q_{g2} = (\beta_g/3600)^2 \sigma_{g2}^2 I_{3 \times 3}  = (\beta_g/3600)^2
  \sigma_g^2 ( 1 - \alpha_g^2) I_{3 \times 3} } \quad (\mathrm{rad/s})^2
\end{align}
The parameter $\alpha_g$ is related to the time constant $\tau_g$ of
the gyro bias Gauss-Markov process: \[ \alpha_g = e^{-\Delta t/\tau_g} \]  IMU data
sheets rarely specify $\tau_g$, but it can be determined experimentally.  One
typically chooses it to be quite long (e.g., 100 seconds).  


\subsubsection{Typical Parameter Values}
Table \ref{tab:typicalImuVals} lists typical noise and bias parameter values
for different categories of IMU, from lower to higher quality.  Also see Table
9.3 in \cite{brown2012introKf}.

\begin{table}[htbp]
  \centering
  \caption{Typical Noise and Bias Parameter Values for Various IMU Classes}
  \begin{tabular}[c]{l|cccccc}
    \toprule
    Class &  $S_a$ (milli-g)$^2$/Hz & $\sigma_a$ (milli-g) & $\tau_a$ (s)
    & $S_g$ (deg/s)$^2$/Hz & $\sigma_g$ (deg/h) & $\tau_g$ (s) \\ \midrule
    Automotive & $0.3^2$ & 10 & 100 & $\left(5\times10^{-2}\right)^2$& 30 & 100 \\
    Industrial & $0.2^2$ & 3 & 100 & $\left(2\times10^{-2}\right)^2$&  10 & 100 \\
    High-Quality MEMS & $0.025^2$ & 0.04 & 100 & $\left(5\times10^{-3}\right)^2$&  5 & 100 \\
    Low-Quality Tactical & $0.025^2$ & 0.04 & 100 & $\left(2\times10^{-4}\right)^2$&  1 & 100 \\
    Tactical & $0.025^2$ & 0.04 & 100 & $\left(2\times10^{-4}\right)^2$&  0.1 & 100 \\
    Navigation & $0.01^2$ & 0.03 & 100 & $\left(3\times10^{-5}\right)^2$&  0.01 & 100 \\
    \bottomrule
  \end{tabular}
  \label{tab:typicalImuVals}  
\end{table}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:

% LocalWords:  Catan pmf Cov ZX diag kj datasheets milli cccccc randn ccc fY ci
% LocalWords:  cccc
