\chapter{Measurement and Dynamics Models for Estimation}
\label{cha:meas-dynam-models}

The estimator we will develop to infer the quadrotor's state from noisy sensor
measurements is a \emph{model-based estimator}, which means it is founded
on models of the sensor measurements and the vehicle dynamics.  The measurement
model relates the state of the system to the incoming measurements.  The
dynamics model describes how the state evolves in time.   

\section{State}
\label{sec:quadStateForEstimation}
The state of a dynamical system is commonly described by one or more elements
stacked in a vector.  Let us denote our state vector as $\vb{x}$.  We will adopt
a discrete-time state representation with a uniform measurement interval
$\Delta t$.  At time $t_k = k\Delta t$ the state of our quadrotor is
\begin{align}
  \label{eq:state15}
  \vb{x}(k) = \left[\ba{c} \vb{r}_{\rm I}(k) \\ \vb{v}_{\rm I}(k) \\ \vb{e}(k)
  \\ \vb{b}_a(k) \\ \vb{b}_g(k) \ea \right]
\end{align}
where each component is a $3 \times 1$ vector:
\[\ba{ll}
  \vb{r}_{\rm I}(k) & \mbox{position of quad CM expressed in I} \\
  \vb{v}_{\rm I}(k) & \mbox{time rate of change of $\vb{r}_{\rm I}$} \\
  \vb{e}(k) & \mbox{attitude error} \\
  \vb{b}_a(k) & \mbox{accelerometer bias} \\
  \vb{b}_g(k) & \mbox{gyro bias} \ea \] This 15-element state contains the
minimum number of elements necessary for position and orientation (pose)
estimation using an IMU and some position source (in our case, GNSS).  All
vector state components are as described in previous lectures except for
$\vb{e}$.  We could take $\vb{e}$ to be the vector of 3-1-2 Euler angles
$\phi, \theta$ and $\psi$ as we have in the past, but we would run into trouble
whenever $\phi \approx \pm \pi/2$ because, as you'll recall, for that value of
$\phi$ the 3-1-2 Euler representation becomes singular: there is no longer a
unique relationship between the Euler angles and the attitude matrix.

Another option would be to replace $\vb{e}$ with a quaternion $\vbb{q}$, but
the quaternion is not a minimal attitude representation (it has 4 elements
instead of 3), and the constraint it must obey ($\vbb{q}\T \vbb{q} = 1$) causes
trouble for state estimation.  (If you're interested in learning more about
this, see \cite{leffens1982kalman, crassidis2007survey}.)

We'd like to have a minimal 3-element representation, but we'd rather avoid the
singularity of a minimal representation.  We will adopt the approach described
in Section \ref{sec:kinem-equat-error}: we will treat $\vb{e}$ as a vector of
3-1-2 error Euler angles
$\vb{e} = \left[ \delta \phi, \delta \theta, \delta \psi\right]\T$ small enough
that $|\delta \phi| \ll \pi/2$, eliminating any risk of singularity.  Let
$R_{\rm BI}$ be the true I-to-B attitude matrix, and let $\bar{R}_{\rm BI}$ be
a model (e.g., an estimate) we have of $R_{\rm BI}$.  Our estimate may not
exactly equal $R_{\rm BI}$, but it will be close.  Then we can write
\begin{align}
  \label{eq:RBIeDef}
  R_{\rm BI}(\vb{e})= D(\vb{e}) \bar{R}_{\rm BI}
\end{align}
where $D(\vb{e})$ is the direction-cosine matrix associated with the
3-1-2 error Euler angles in $\vb{e}$.  In other words, $D(\vb{e})$ is
what you get when you plug $\vb{e}$ into the function {\tt euler2dcm}.

\section{Measurement Model}
The estimator's measurement model is closely related to the GNSS and camera
measurement models that were introduced previously.  The general form of the
measurement model is
\begin{align}
  \vb{z}(k) = \vb{h}[\vb{x}(k)] + \vb{w}(k)
  \label{eq:measModelNL}
\end{align}
where $\vb{z}(k) \in \mathbb{R}^{n_z}$ is a vector of measurements, with $n_z$
denoting the number of measurements;
$\vb{h} : \mathbb{R}^{n_x} \mapsto \mathbb{R}^{n_z}$ is a (nonlinear in our
case) vector-valued function of the state $\vb{x}(k)$; and
$\vb{w}(k) \in \mathbb{R}^{n_z}$ is the measurement noise, which is modeled as
zero mean, uncorrelated in time, having covariance matrix $Q(k)$, and
independent of the state $\vb{x}(k)$:
\begin{align}
  \E{\vb{w}(k)} &= \vb{0} \\
  \E{\vb{w}(k)\vb{w}\T(j)} &= R(k)\delta_{kj} \\
  \E{\vb{x}(k)\vb{w}\T(j)} &= \E{\vb{x}(k)}\E{\vb{w}\T(j)} = 0 ~\forall k,j
\end{align}

Suppose that at time $t_k$ we get $N_{{\rm f}k}$ camera measurements of 3D
environmental features projected onto the camera's 2D image plane.  Further
suppose that, due to a previous mapping campaign, we know the location of each
of these features in I.  Then for each of our camera measurements, we have a
pair of corresponding unit vectors, one in the camera (C) frame, and one in the
I frame.  With these pairs we can estimate the attitude of our quadrotor.

Let $\vb{X}_{i \rm I} \in \mathbb{R}^3$ be the location of the $i$th feature in
the I frame. In other words, $\vb{X}_{i \rm I}$ is the vector pointing from the
I frame origin to the $i$th feature, expressed in I.  The location of the C
frame origin (the camera center) in I is
\begin{align}
  \vb{r}_{c \rm I} = \vb{r}_{\rm I} + R_{\rm BI}\T \vb{r}_{oc \rm B}
\end{align}
where $ \vb{r}_{oc \rm B}$ is the location of the camera center in B. (Note that
$\vb{r}_c$ is identical to $\vb{t}$ from Section \ref{sec:from-i-frame}.) Then
the vector pointing from the camera center to the $i$th 3D feature, expressed in
the I frame, is
\begin{align}
  \vb{v}_{i \rm I} = \vb{X}_{i \rm I} - \vb{r}_{c \rm I}
\end{align}
Note that $\vb{v}_{i \rm I}$ depends on the state $\vb{x}$ because
$\vb{r}_{c \rm I}$ depends on $\vb{r}_{\rm I}$, which is the first vector
element of the state $\vb{x}$, and on $R_{\rm BI}$, which, according to
(\ref{eq:RBIeDef}), depends on the state's $\vb{e}$ vector.  Also note that
because our use of $\vb{v}_{i \rm I}$ will be solely for attitude estimation,
only its direction matters to us.  Accordingly, we normalize $\vb{v}_{i \rm I}$:
\[\vb{v}_{i \rm I}^u = \frac{\vb{v}_{i \rm I}}{\|\vb{v}_{i \rm I}\|} \]

A vector corresponding to $\vb{v}_{i \rm I}^u$ can be obtained in the C frame.
Let $\vb{x}_{i} \in \mathbb{R}^2$ be the true coordinates (in meters) of the
point feature at $\vb{X}_{i} $ as projected onto the camera's image plane, and
recall that $f$ is the focal length of the pinhole camera model, in meters.
Then
\begin{align}
  \vb{v}_{ci \rm C} = \left[\ba{c} \vb{x}_{i} \\ f \ea \right] \in \mathbb{R}^3
\end{align}
is the vector, expressed in C, that points from the camera center to the point
on the camera's image plane where $\vb{X}_{i \rm I}$ is projected.  Note that
the length of $ \vb{v}_{ci \rm C}$ doesn't tell us any new information beyond
what is available in $\vb{x}_{i}$, since $f$ is a known intrinsic
parameter of the camera.  Thus, we can normalize $\vb{v}_{ci \rm C}$ without
losing any information.  We will denote the normalized vector as  $\vb{v}_{i
  \rm C}^u$ because it refers to the same unit vector as  $\vb{v}_{i \rm I}^u$,
only expressed in C rather than I:   
\[\vb{v}_{i \rm C}^u = \frac{\vb{v}_{ci \rm C}}{\|\vb{v}_{ci \rm C}\|} \]


The above vector quantities are the \emph{true} vectors.  The corresponding
\emph{measured} vectors are identical except that they are denoted with a tilde
(e.g., $\vbt{r}_{p \rm I}$ is the measurement of $\vb{r}_{p \rm I}$) and are
modeled as equal to the true vector plus noise.  In the case of unit vectors,
the measurement vectors are also of unit length, which means that the noise is
assumed to lie only in the plane normal to the unit vector.  Stacking up all
$N_{{\rm f}k}$ camera measurements, and prepending these with the GNSS
measurement of the primary antenna $\vbt{r}_{p\rm I}$ and the normalized
primary-to-secondary constrained-baseline measurement $\vbt{r}_{b\rm I}^u$, our
measurement model becomes
\begin{align}
  \label{eq:measModelVec}
  \underbrace{\left[\def\arraystretch{1.4}
  \ba{c} \vbt{r}_{p\rm I} \\ \vbt{r}_{b\rm I}^u \\ \vbt{v}_{1 \rm C}^u \\
  \vdots \\ \vbt{v}_{N_{{\rm f}k} \rm C}^u \ea \right]}_{\vb{z}(k)} =
  \underbrace{\left[\def\arraystretch{1.4} \ba{c} \vb{r}_{\rm I} +
  R_{\rm BI}\T(\vb{e})\vb{r}_{a1 \rm B} \\
  R_{\rm BI}\T(\vb{e}) \vb{r}_{b \rm B}^u \\
  R_{\rm CB} R_{\rm BI}(\vb{e}) \vb{v}_{1 \rm I}^u \\
  \vdots \\
  R_{\rm CB} R_{\rm BI}(\vb{e}) \vb{v}_{N_{{\rm f}k} \rm I}^u \ea
  \right]}_{\vb{h}\left[\vb{x}(k) \right]} +
  \underbrace{\left[\def\arraystretch{1.4} \ba{c} \vb{w}_{p \rm I} \\ \vb{w}_{b \rm I} \\
  \vb{w}_{1 \rm C} \\ \vdots \\ \vb{w}_{N_{{\rm f}k} \rm C} \ea \right]}_{\vb{w}(k)}
\end{align}
In this model, $\vb{r}_{a1 \rm B}$ is the known location of the primary antenna
in B, $\vb{r}_{b \rm B}^u$ is the unit vector pointing from the primary to the
secondary antenna in B (the normalized ``baseline'' vector), $R_{\rm CB}$ is
the known B-to-C (body-to-camera) attitude matrix, and the various noise
vectors in $\vb{w}(k)$ model the noise for each of the measurements.

\section{Dynamics Model}
Our discrete-time nonlinear dynamics model has the form
\begin{align}
  \vb{x}(k+1) = \vb{f}\left[\vb{x}(k), \vb{u}(k), \vb{v}(k) \right]
  \label{eq:dynamModelNL}
\end{align}
where $k$ indexes time such that $t_k = k\Delta t$, and where the 
elements are defined as follows:
\[\ba{ll}
  \vb{f}& \mathbb{R}^{n_x} \times \mathbb{R}^{n_u} \times \mathbb{R}^{n_v}
  \mapsto \mathbb{R}^{n_x} ~~ \mbox{vector-valued dynamics function} \\
  \vb{x}& \mbox{$n_x\times 1$ state} \\
  \vb{u}& \mbox{$n_u\times 1$ known input} \\
  \vb{v}& \mbox{$n_v\times 1$ process noise} \ea \]

The state $\vb{x}$ is as described in Section \ref{sec:quadStateForEstimation}.
The known input $\vb{u}$ holds the IMU measurements,
\[\vb{u}(k) = \left[ \ba{c} \vbt{\omega}_{\rm B}(k) \\ \vbt{f}_{\rm B}(k) \ea
  \right] \]
and $\vb{v}$ holds the process noise elements,
\[\vb{v}(k) = \left[ \ba{c} \vb{v}_g(k) \\  \vb{v}_{g2}(k) \\  \vb{v}_a(k) \\  \vb{v}_{a2}(k) \ea
  \right] \] We model the process noise vector $\vb{v}(k)$ as zero mean,
uncorrelated in time, having covariance matrix $Q(k)$, independent of the
measurement noise $\vb{w}(k)$, and independent of the state $\vb{x}(k)$:
\begin{align}
  \E{\vb{v}(k)} &= \vb{0} \\
  \E{\vb{v}(k)\vb{v}\T(j)} &= Q(k)\delta_{kj} \\
  \E{\vb{v}(k)\vb{w}\T(j)} &= 0 ~ \forall k,j \\
  \E{\vb{x}(k)\vb{v}\T(j)} &= \E{\vb{x}(k)}\E{\vb{v}\T(j)} = 0 ~\forall k,j
\end{align}

Note that the process noise elements are all drawn from our IMU measurement
model.  This is because the architecture we have chosen for our estimator is a
so-called \emph{model replacement} architecture whereby we don't model the
quad's dynamics in the dynamics model $\vb{f}$ but instead we model the IMU's
dynamics and stochastic behavior.

Provided the interval $\Delta t$ is short enough (e.g., less than 10
milliseconds), the discrete-time function
$\vb{f}\left[\vb{x}(k), \vb{u}(k), \vb{v}(k) \right]$ can be implemented using
simple Euler-type integration.  We will assume that $\vb{u}(k)$ and $\vb{v}(k)$
remain constant over the interval from $t_k$ to $t_{k+1}$.  This so-called
zero-order-hold model for $\vb{u}(k)$ and $\vb{v}(k)$ is accurate for short
$\Delta t$.  Then $\vb{f}$ is modeled as
\begin{align}
  \underbrace{\left[\def\arraystretch{1.4}
  \ba{c} \vb{r}_{\rm I}(k+1) \\ \vb{v}_{\rm I}(k+1) \\ \vb{e}(k+1) \\
  \vb{b}_a(k+1) \\ \vb{b}_g(k+1) \ea \right]}_{\vb{x}(k+1)}
  =
  \underbrace{\left[\def\arraystretch{1.4} \ba{c} \vb{r}_{\rm I}(k) + \Delta t
   \vb{v}_{\rm I}(k) + \sfrac{1}{2}(\Delta t)^2 \vb{a}_{\rm I}(k) \\
   \vb{v}_{\rm I}(k) + \Delta t  \vb{a}_{\rm I}(k) \\
   \vb{e}(k) + \Delta t \vbd{e}(k) \\
   \alpha_a \vb{b}_a(k) + \vb{v}_{a2}(k) \\
   \alpha_g \vb{b}_g(k) + \vb{v}_{g2}(k) \ea \right]}_{\vb{f} \left[ \vb{x}(k),
   \vb{u}(k), \vb{v}(k)\right]} 
\end{align}
All of the quantities in this dynamics model are either known parameters or
elements of the state vector $\vb{x}$ or process noise vector $\vb{v}$, except
for $\vbd{e}(k)$ and $\vb{a}_{\rm I}$, which we obtain as described in the next
subsections.

We have called $\Delta t$ the measurement interval, as if all sensors produce
simultaneous measurements at this interval.  But in fact different sensors
provide measurements at different rates.  Typically, an IMU produces
measurements at the fastest rate (e.g., 100-200 Hz), whereas camera and GNSS
measurements come more slowly (e.g., 10-20 Hz).  The dynamics function
(\ref{eq:dynamModelNL}) should be propagated at the IMU measurement rate,
whereas the measurement model (\ref{eq:measModelNL}) applies at the camera and
GNSS measurement rate.  

\subsection{Obtaining $\vbd{e}(k)$}
If we know the angular rate $\vb{\omega}_{\rm B}(k)$ we can obtain $\vbd{e}(k)$
from (\ref{eq:errorEulerKinematics}).  But how can we obtain
$\vb{\omega}_{\rm B}(k)$?  From our angular rate sensor measurement
$\vbt{\omega}_{\rm B}(k)$, as follows:
\begin{align}
  \vb{\omega}_{\rm B}(k) = \vbt{\omega}_{\rm B}(k) - \vb{b}_g(k) - \vb{v}_g(k)
\end{align}
Note that the gyro bias $\vb{b}_g(k)$ is an element of the state, and the gyro
noise $\vb{v}_g(k)$ is an element of the process noise vector $\vb{v}(k)$.

\subsection{Obtaining $\vb{a}_{\rm I}(k)$}
The acceleration of the quad's CM relative to the I frame and expressed in the I
frame, written $\vb{a}_{\rm I}(k)$, can also be obtained from the IMU---not
directly, of course, since the IMU accelerometers only measure specific
force---but indirectly.  We simply solve for $\vb{a}_{\rm I}$ in the measurement
model (\ref{eq:accel_model_full}):
\begin{align}
  \vb{a}_{\rm I}(k) = R_{\rm BI}\T(\vb{e}(k)) \left[ \vbt{f}_{\rm B}(k) -
  \vb{b}_a(k) -\vb{v}_a(k) \right] - g\vb{e}_3 
\end{align}
Note that the accelerometer bias $\vb{b}_a(k)$ is an element of the state, and
the accelerometer noise $\vb{v}_a(k)$ is an element of the process noise vector
$\vb{v}(k)$.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:

% LocalWords:  euler dcm Wahba's
