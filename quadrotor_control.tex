\chapter{Quadrotor Control}
\label{cha:quadrotor-control}
\section{Controllability}
\qa{How many actuators does a quad have?}{4: each motor+propeller is an actuator.}
\qa{How many degrees of freedom does our quad model have?}{6: three
  translational DOFs and three rotational DOFs.}
\qa{What are the implications of having fewer actuators than DOFs?}{The quad is
  \emph{underactuated}: it has fewer actuators than degrees of freedom.  This
  implies that its attitude and position dynamics are \emph{coupled}:  We cannot
  ask our quad to fly an arbitrary trajectory $\vbs{r}_\text{I}(t)$ with an
  arbitrary attitude $\RBIs(t)$.}
Recall that the total torque acting on the quad about the CM expressed in the B
frame is
\[\vb{N}_{\rm B}= \sum^4_{i=1} \left(\vb{N}_{i\text{B}} + \vb{r}_{i\text{B}} \times \vb{F}_{i\text{B}} \right)  \]
And the total thrust acting on the CM  expressed in the B frame is
\[ \vb{F}_\text{B} = \sum_{i=1}^4 \vb{F}_{i\text{B}} \] For $i \in \{1,2,3,4\}$,
consider
\[\vb{r}_{i\text{B}} = \left[\ba{c} x_i \\ y_i \\ z_i \ea \right], \quad
  \vb{F}_{i\text{B}} = \left[\ba{c} 0 \\ 0 \\ F_i \ea \right] \] It is easy to
show that
\[ \vb{r}_{i\text{B}} \times \vb{F}_{i\text{B}} = \left[\ba{c} ~~y_i F_i \\ -x_i F_i  \\ 0
    \ea \right] \]
Also note that \[ \vb{N}_{i\text{B}} = \left[\ba{c} 0 \\ 0 \\ N_i \ea
  \right], \quad N_i = s_i k_T F_i  \]  Finally, define \[F \define \sum_{i=1}^4
  F_i \]

Putting all this together, we can write
\begin{equation}
  \label{eq:controlMatrixG}
  \left[\ba{c} F\\N_{\text{B}x} \\ N_{\text{B}y} \\ N_{\text{B}z} \ea \right] =
  \underbrace{\left[\ba{cccc} 1 & 1 & 1 & 1 \\ y_1 & y_2 & y_3 & y_4 \\ -x_1 &
      -x_2 & -x_3 & -x_4 \\ s_1 k_T & s_2 k_T & s_3 k_T & s_4 k_T \ea
    \right]}_{G} \left[\ba{c} F_1 \\ F_2 \\ F_2 \\ F_4 \ea \right]  
\end{equation}
Note that if the matrix $G$ is invertible, then we can find the thrust vector
$[F_1, F_2, F_3, F_4]\T$ to effect any desired total thrust and torque
$[F, \vb{N}_\text{B}\T]$. This is a useful amount of control!  But a quadrotor
lacks the actuation necessary to control the full thrust and torque vector
$\left[\vb{F}_\text{B}\T, \vb{N}_\text{B}\T\right]$.  \qa{Would $G$ be
  invertible for a tri-copter?}{No, it would not even be square.}  \qa{Could a
  hexacopter control the full thrust and torque vector
  $\left[\vb{F}_\text{B}\T, \vb{N}_\text{B}\T\right]$?}{Yes, but only if the
  propellers were arranged in such a way that the resulting $6\times6$ control
  matrix analogous to $G$ were invertible.}

\section{Overview of Feedback Control}
In control theory, we call the dynamical system we wish to control the
\emph{plant}.  This could be a chemical reaction, an oscillating circuit, or, as
in our case, a quadrotor.  Suppose we feed the plant $P$ with the output
$u(t)$ of a controller $C$, as in the diagram below.  We wish drive the
combined system with a reference input $r(t)$ that the plant's output $y(t)$
closely tracks.

The key feature of feedback control is that the output $y(t)$ is fed back and
subtracted from the reference input $r(t)$ to produce a control error signal
$e(t)$, which, in turn, drives the controller $C$.

\begin{figure}[h]
  \centering 
\begin{tikzpicture}[auto, node distance=2cm]
  \node [input, name=input] {};
  \node [sum, right of=input] (sum1) {};
  \node [block, right of=sum1,  node distance=2.5cm] (controller)
  {\begin{tabular}{c} Controller \\ $C$ \end{tabular}};
  \node [block, right of=controller, node distance=3.5cm] (plant)
  {\begin{tabular}{c} Plant \\ $P$ \end{tabular}};
  \node [output, right of=plant, node distance=2.5cm] (output) {};
  \draw [connector] (input) -- node{$r(t)$} (sum1);
  \draw [connector] (plant) -- node[name=y]{$y(t)$} (output);
  \draw [connector] (controller) -- node[name=u]{$u(t)$} (plant);
  \draw [connector] (sum1) -- node{$e(t)$} (controller);
  \node [block, below of=u, node distance=2cm] (conditioning)
  {\begin{tabular}{c} Feedback \\ conditioning \\ $H$ \end{tabular}};
  \draw [connector] (y) |- (conditioning);
  \draw [connector] (conditioning) -| node[pos=0.95] {$-$} (sum1);
\end{tikzpicture}
\end{figure}

\qa{What are the benefits of feedback control vs. feedforward control (in which
  the feedback line is omitted)?}{For feedforward control, one requires an
  exquisitely accurate model of $P$, including its initial conditions.  For
  feedback control, the model of $P$ is allowed to be only approximate.}
\qa{So is feedforward control useless?}{No.  The more accurately you model the
  plant, the further ahead you can look while formulating your controller's
  current actions.  A popular control technique called \emph{model predictive
    control} optimizes by looking ahead over a finite window in what amounts to
  \emph{hypothetical feedforward control}.  It chooses its control inputs
  depending on what yields the most desirable state at the end of the look-ahead
  window.}

For linear time-invariant (LTI) systems, we can recast the above system in the
Laplace domain:

\begin{figure}[h]
  \centering 
\begin{tikzpicture}[auto, node distance=2cm]
  \node [input, name=input] {};
  \node [sum, right of=input] (sum1) {};
  \node [block, right of=sum1,  node distance=3cm] (G) {$G(s) = C(s)P(s)$};
  \node [output, right of=G, node distance=3cm] (output) {};
  \draw [connector] (input) -- node{$R(S)$} (sum1);
  \draw [connector] (G) -- node[name=y]{$Y(s)$} (output);
  \draw [connector] (sum1) -- node{$E(s)$} (G);
  \node [block, below of=G, node distance=1.5cm] (H) {$H(s)$};
  \draw [connector] (y) |- (H);
  \draw [connector] (H) -| node[pos=0.95] {$-$} (sum1);
\end{tikzpicture}
\end{figure}

We call $G(s)H(s)$ the \emph{open-loop transfer function}.  The
\emph{closed-loop transfer function} $T(s) \define Y(s)/R(s)$ describes the full
input-output relationship of the closed-loop system.

\begin{figure}[h]
  \centering
\begin{tikzpicture}[auto, node distance=2cm]
  \node [input, name=input] (input) {};
  \node [block, right of=input] (tf) {$T(s)$};
  \node [output, right of=tf, node distance=2cm] (output) {};
  \draw [connector] (input) -- node {$R(s)$} (tf);
  \draw [connector] (tf) -- node {$Y(s)$}(output);
\end{tikzpicture}
\end{figure}

$T(s)$ may be derived as follows:
\begin{align*}
  E(s) &\define R(s) - H(s)Y(s) \\
  &=R(s) - H(s)G(s)E(s) \\
         &= \frac{R(s)}{1 + H(s)G(s)} \\
  Y(s) &= G(s) E(s) = \frac{G(s)R(s)}{1 + H(s)G(s)} \\
  T(s) &\define \frac{Y(s)}{R(s)}=\frac{G(s)}{1 + H(s)G(s)}
\end{align*}
A study of $T(s)$ reveals the performance and stability of the feedback system.

\section{Second-Order System Dynamics}
Consider a closed-loop transfer function with second-order dynamics:
\begin{equation}
  \label{eq:2ndOrderTransferFunction}
  T(s) = \frac{\omega_n^2}{s^2 + 2\zeta\omega_ns + \omega_n^2}  
\end{equation}
For reasons that will become clear later on, $\zeta$ is called the damping
ratio and $\omega_n$ the natural frequency.  Let us examine the system's
response to a unit step input, given by
\begin{equation*}
  r(t) = \begin{cases} 1 & t \geq 0 \\ 0  & t < 0 \end{cases} \qquad \qquad
\vcenter{\hbox{\begin{tikzpicture}[auto,node distance=2cm]
 \draw[->] (-2,0) -- (2,0) node[right]{$t$};
 \draw[->] (0,0) -- (0,1) node[above]{$r(t)$};
 \draw[thick] (-2,0) -- (0,0) node[below]{$0$} -- (0,0.5) node[left]{$1$} -- (2,0.5);
\end{tikzpicture}}}
\end{equation*}
In the Laplace domain, the unit step function is written $R(s) = 1/s$.
The response $Y(s)$ to $R(s) = 1/s$ is
\[Y(s) = R(s)T(s) = \frac{\omega_n^2}{s(s^2 + 2\zeta\omega_ns + \omega_n^2)}\]
Using the inverse Laplace transform, we can convert this to the time domain as
\begin{equation}
  \label{eq:timeDomainStepResponse2ndOrder}
  y(t) = 1 - \frac{1}{\beta}e^{-\zeta\omega_n t}\sin(\omega_n\beta t + \theta),
  \qquad  \ba{l} \beta = \sqrt{1 - \zeta^2} \\
  \theta = \arccos(\zeta)~\mbox{for}~ 0 < \zeta < 1 \ea
\end{equation}
Note that the phase $\theta$ is meaningless when $\beta$ is imaginary.

\qa{What is the frequency of oscillation of the step
  response?}{$\omega_n \beta$.  This explains why $\omega_n$ is called the
  natural frequency---it's the frequency at which the system would naturally
  oscillate in the absence of damping ($\zeta = 0,~ \beta = 1$).}

\qa{How long does it take for the oscillatory part attenuate to a factor of
  $1/e$ of its initial
  amplitude?}{$\displaystyle \tau = \frac{1}{\omega_n \zeta}$, which is called
  the time constant of the second-order system.  Thus, the larger the damping
  factor $\zeta$, the faster the system settles.}

As a function of normalized time $\omega_n t$, the step response for various
values of $\zeta$ is shown below.
\begin{minipage}{0.7\textwidth}
\includegraphics[width=\textwidth, trim={0 0 0 -10},
clip]{figs/secondOrderStepResponse.pdf}
\end{minipage}
\begin{minipage}{0.1\textwidth}
  \begin{tabular}{ll}
    $0 \leq \zeta < 1$& underdamped \\
    $\zeta = 1/\sqrt{2}$& ``Goldilocks'' damped \\
    $\zeta = 1$& critically damped \\
    $\zeta > 1$& overdamped  \\
                & \\
                & 
  \end{tabular}
  \vspace{1cm}
\end{minipage}

\subsection{Performance Indicators}
\label{sec:perf-indic}
\renewcommand{\arraystretch}{1.5}
\begin{tabular}{ll}
  Time constant:& $\displaystyle \tau = \frac{1}{\omega_n \zeta}$ \\
  Settling time:& $\displaystyle T_\text{s} = 4 \tau$ ~~ (settling to within
                  $2\%$ of steady-state value)\\
  Percent overshoot:& $\displaystyle P_\text{o} = 100 \cdot \exp(-\zeta \pi /\beta)$\\
  Rise time (100\%):& $\displaystyle T_\text{r} = \frac{\pi - \theta}{\omega_n - \beta}$
\end{tabular}
\renewcommand{\arraystretch}{1}

\section{Steady-state Tracking Error}
The final value theorem (FVT) says that for some signal $q(t)$ with Laplace transform
$Q(s)$, the value of $q(t)$ as $t \rightarrow \infty$ is given by
\[
\lim_{t\rightarrow\infty}q(t) = \lim_{s\rightarrow 0}sQ(s)
\]
The Laplace transform of the error signal $e(t) = r(t) - y(t)$ is
\[
E(s) = R(s) - Y(s) = R(s) - R(s)T(s)
\]
We can apply the FVT to obtain the so-called steady-state error:
\[
e_\text{ss} = \lim_{s\rightarrow 0}sE(s)
\]

We commonly test $e_\text{ss}$ with the following inputs:

\renewcommand{\arraystretch}{2.5}
\begin{tabular}{lll}
  Step: &  $\displaystyle R(s) = \frac{1}{s}$ & \begin{tikzpicture}[baseline=1mm]
  \draw[thick] (-1,0) -- (0,0) node{} -- (0,0.5) node{} -- (1,0.5);
 \end{tikzpicture} \\
  Ramp: &  $\displaystyle R(s) = \frac{1}{s^2}$ & \begin{tikzpicture}[baseline=1mm]
 \draw[thick] (-1,0) node{} -- (1,0.5) node{};
\end{tikzpicture} \\
  Quadratic:  & $\displaystyle R(s) = \frac{1}{s^3}$ & \begin{tikzpicture}[baseline=1mm]
\draw[thick] plot [smooth, tension=1] coordinates {(0,0) (1,0.2) (2,0.8)};
\end{tikzpicture}
\end{tabular}
\renewcommand{\arraystretch}{1}

\section{Proportional-Integral-Derivative Control}
About 97\% of controllers applied in industrial practice are PID controllers.

Consider a unity feedback control system with a PID controller $C$:
\begin{figure}[h]
  \centering 
\begin{tikzpicture}[auto, node distance=2cm]
  \node [input, name=input] {};
  \node [sum, right of=input] (sum1) {};
  \node [block, right of=sum1,  node distance=2.5cm] (controller){$C$};
  \node [block, right of=controller, node distance=3.5cm] (plant){$P$};
  \node [output, right of=plant, node distance=2.5cm] (output) {};
  \draw [connector] (input) -- node{$r(t)$} (sum1);
  \draw [connector] (plant) -- node[name=y]{$y(t)$} (output);
  \draw [connector] (controller) -- node[name=u]{$u(t)$} (plant);
  \draw [connector] (sum1) -- node{$e(t)$} (controller);
  \draw [connector] (y) |- (2,-1.5) -| node[pos=0.95] {$-$} (sum1);
\end{tikzpicture}
\end{figure}

The controller's output $u(t)$ is related to the error signal $e(t)$ by
\begin{align*}
u(t) &= \underbrace{ke(t)}_\text{P} + \underbrace{k_{i}\int_0^t e(\tau)
       d\tau}_\text{I} + \underbrace{k_{d} \dot{e}(t)}_\text{D}  \\
     & \\
     &= k \left[ e(t) + \frac{1}{T_{i}}\int_0^t e(\tau)d\tau + T_{d}\dot{e}(t) \right]  
\end{align*}

\qa{Why is PID control so effective?}{It offers a simple way to address 
  past, present, and future errors.}

\begin{figure}[h]
  \centering 
  \begin{tikzpicture}
    \node[anchor=south west,inner sep=0] (image) at (0,0)
    {\includegraphics[width=0.7\textwidth]{figs/pidTermsVisualized.pdf}};
    \begin{scope}[x={(image.south east)},y={(image.north west)}]
      \node at (.08,.85) {$e(t)$};
      \node at (.92,.23) {$t$};
      \node at (.50,.80) {past};
      \node at (.50,.87) {(I)};
      \node at (.635,.85) {present};
      \node at (.635,.92) {(P)};
      \node at (.76,.80) {future};
      \node at (.76,.87) {(D)};
      \node at (.64,.07) {$t'$};
      \node at (.70,.08) {$T_d$};
      \node at (.57,.50) {$e(t')$};
      \node at (.90,.57) {$e(t')+T_d\dot{e}(t')$};
    \end{scope}
  \end{tikzpicture}
\end{figure}

\begin{tabular}{lll}
  Proportional: & present error value & (prompt) \\
  Integral: & weighted accumulation of past error values & (retrospective) \\
  Derivative: & look ahead $T_{d}$ seconds, extrapolating via tangent & (anticipatory)
\end{tabular}

The Laplace-domain error signal in the unity-gain system above is
\begin{align*}
  E(s) &= R(s) - Y(s) = R(s) - C(s)P(s)E(s) \\
  &= \frac{R(s)}{1 + C(s) P(s)}
\end{align*}
Consider strictly proportional control, for which $C(s) = k$.  The steady-state error in
response to a step input $R(s) = 1/s$ is
\begin{align*}
  e_\text{ss} = \lim_{t \rightarrow \infty} e(t) &= \lim_{s \rightarrow 0} sE(s) \\
              &= \lim_{s \rightarrow 0} \frac{s\cdot \left(1/s\right)}{1 + kP(s)} \\
  &= \frac{1}{1 + kP(0)}
\end{align*}
Assuming a finite $P(0)$, this indicates there will remain a nonzero
$e_\text{ss}$ in response to the step input unless $k \rightarrow \infty$.

Suppose we add an integral component so that
\[ C(s) = k + \frac{k_{i}}{s} \]
Then the steady-state error becomes
\begin{align*}
  e_\text{ss} = \lim_{s\rightarrow 0} \frac{s\cdot \left(1/s\right)}{1 + \left(k
  + k_{i}/s\right) P(s)}
\end{align*}
which equals zero provided $P(0)$ is nonzero.  Thus, integral control ensures
zero steady-state error irrespective of $P(s)$, so long as $P(0) \neq 0$, which
is typical.

\qa{Proportional control in a PID is essential for basic function, and, as we
  have seen, integral control drives $e_\text{ss}$ to zero.  What role does
  derivative control play?}{The derivative term in the PID controller tends to
  improve stability by dampening oscillations.}

\subsection{Transfer Function for  Quad Control along a Single Dimension}
\label{sec:transf-funct-quad}
We control our quad's position $\vb{r}$ by applying a force $\vb{F}$, and its
attitude $\RBI$ by applying a torque $\vb{N}$.  The governing dynamical
equations are
\begin{align*}
  m\vbdd{r}_\text{I} &= \vb{F}_\text{I} \\
J\vbd{\omega}_\text{B} &= \vb{N}_\text{B} - [\wB \times] J \wB 
\end{align*}
These are coupled differential equations because the force $\vb{F}_\text{I}$ depends
on the orientation of the quad, which in turn depends on $\wB$.

Suppose three conditions hold regarding these governing equations.  First,
suppose the term $[\wB \times] J \wB$ can be measured accurately and cancelled
out by feedforward control.  Second, suppose the inertia matrix $J$ is diagonal,
i.e.,
\[
J = \begin{bmatrix} J_x & 0 & 0 \\ 0 & J_y & 0 \\ 0 & 0 & J_z \end{bmatrix}
\]
Third, suppose the quad's Euler angles $[\phi, \theta,\psi]\T$ are small enough
that the matrix $S(\phi, \theta, \psi)$ from (\ref{eq:eulerRates_312}) can be
approximated as $S(\phi, \theta, \psi) \approx I_{3\times 3}$, in which case
$\wB \approx [\dot{\phi}, \dot{\theta},\dot{\psi}]\T$.  Under
these three conditions, the quad's governing differential equations can be
decoupled along each degree of freedom and written as six 2nd-order ODEs:
\begin{align*}
  m \ddot{r}_{\text{I}x} &= F_{\text{I}x} \\
  m \ddot{r}_{\text{I}y} &= F_{\text{I}y} \\
  m \ddot{r}_{\text{I}z} &= F_{\text{I}z} \\
  J_x \dot{\omega}_{\text{B}x} &\approx   J_x \ddot{\phi} = N_{\text{B}x} \\
  J_y \dot{\omega}_{\text{B}y} &\approx   J_y \ddot{\theta} = N_{\text{B}y} \\
  J_z \dot{\omega}_{\text{B}z} &\approx   J_z \ddot{\psi} = N_{\text{B}z} 
\end{align*}
Each of these decoupled equations has the same form, which we represent
generically as
\[ \ddot{y}(t) = k_y u(t),  \qquad s^2 Y(s) = k_y U(s) \]
Defining our plant $P(s)$ as \[P(s) = \frac{Y(s)}{U(s)} = \frac{k_y}{s^2}\]
we recognize it as having double-integrator dynamics.  

Consider proportional and derivative (PD) control of this plant:
\begin{figure}[h]
  \centering 
\begin{tikzpicture}[auto, node distance=2cm]
  \node [input, name=input] {};
  \node [sum, right of=input, node distance=1.5cm] (sum1) {};
  \node [block, right of=sum1,  node distance=2.8cm] (controller){$C(s) = k +
    k_{d} s$};
  \node [block, right of=controller, node distance=3.8cm] (plant){$\displaystyle
    P(s) = \frac{k_y}{s^2}$};
  \node [output, right of=plant, node distance=2.5cm] (output) {};
  \draw [connector] (input) -- node{$R(s)$} (sum1);
  \draw [connector] (plant) -- node[name=y]{$Y(s)$} (output);
  \draw [connector] (controller) -- node[name=u]{$U(s)$} (plant);
  \draw [connector] (sum1) -- node{$E(s)$} (controller);
  \draw [connector] (y) |- (2,-1.5) -| node[pos=0.95] {$-$} (sum1);
\end{tikzpicture}
\end{figure}

The closed-loop transfer function $T(s)$ is
\begin{align*}
  T(s) &= \frac{C(s)P(s)}{1 + C(s)P(s)}   \\
       &= \frac{k_y(k + k_{d}s)}{s^2 + k_y(k + k_{d}s)}
\end{align*}
Note that $T(s)$ is a 2nd-order transfer function.  With appropriate
substitutions for $k, ~k_y$, and $k_{d}$, we can rewrite it in a form
similar to (\ref{eq:2ndOrderTransferFunction}):
\begin{equation}
  \label{eq:2ndOrderTransferFunctionQuadAxis}
  T(s) = \frac{\omega_n^2 + 2\zeta\omega_n s}{s^2 + 2\zeta\omega_ns + \omega_n^2}  
\end{equation}

\begin{framed}
  The upshot is that, by appropriate choice of $\omega_n$ and $\zeta$ (or $k$
  and $k_{d}$), we can tune the controller $C(s)$, trading off the performance
  characteristics listed in Section \ref{sec:perf-indic} to achieve closed-loop
  behavior close to our desired behavior.  Moreover, although we are only using
  PD control (no integral control), we can typically configure the controller
  such that the steady-state error $e_\text{ss}$ is negligibly small.
\end{framed}

\section{Quadrotor Trajectory and Attitude Control}
\label{sec:quadr-traj-attit}
Overall control of our quadrotor vehicle can be depicted as
\begin{figure}[h]
  \centering 
\begin{tikzpicture}[auto, node distance=2cm]
  \node [input, name=input] {};
  \node [block, right of=input,  node distance=3.5cm] (controller){Controller};
  \node [block, right of=controller, node distance=4.5cm] (quad){Quadrotor};
  \node [output, right of=quad, node distance=2.5cm] (output) {};
  \draw [connector] (input) -- node{Reference} (controller);
  \draw [connector] (quad) -- node[name=y]{State} (output);
  \draw [connector] (controller) -- node[name=u]{\begin{tabular}{c} Control \\ Inputs\end{tabular}} (quad);
  \draw [connector] (y) |- (4,-1.5) -| (controller);
\end{tikzpicture}
\end{figure}

\qa{How do we specify the reference trajectory and attitude?}{We might wish to
  specify the desired position and attitude time histories $\vbs{r}_\text{I}(t)$
  and $\RBIs(t)$.  But our underactuated system cannot control all 6
  independent elements of these quantities directly.  Therefore, instead of
  specifying $\RBIs(t)$, we will specify only the desired yaw angle
  $\psi^*(t)$, or, what amounts to the same thing, the desired direction of the
  body $\vb{x}$ axis in the I frame:
  \[\vbs{x}_\text{I}(t) = \RBIs(t) \vb{x}_\text{B}\]}

\qa{What will be our control inputs?}{$\vb{N}_\text{B}$ and $F$}

\qa{How should we specify the state?}{$S_{q} = \{\vb{r}_\text{I}, \vb{v}_\text{I},
  \RBI, \wB \}$}

We will adopt a cascaded controller paradigm in which the attitude controller is
downstream of the trajectory controller:

\begin{figure}[h]
  \centering 
\begin{tikzpicture}[auto, node distance=2cm]
  \node [input, name=rinput] {};
  \node [block, right of=rinput, node distance=2.5cm, minimum height=6em] (tc)
  {\begin{tabular}{c} Trajectory \\ Controller \end{tabular}};
  \node [block, right of=tc, node distance=7cm, minimum height=6em] (quad)
  {\begin{tabular}{c} Quad \\ Dynamics \end{tabular}};
  \node [output, right of=quad, node distance=5cm] (output) {};
  \draw [connector] (rinput) -- node{$\vbs{r}_\text{I}$} (tc);
  \draw [connector] (quad) -- node[name=state]
  {$S_{q} = \{\vb{r}_\text{I}, \vb{v}_\text{I},
    \RBI,\wB \}$} (output);
  \draw [connector] ([yshift=0.5cm] tc.east) -- node[name=F]{$F$}([yshift=0.5cm]
  quad.west);
  \node [block, below of=F, node distance=2cm, minimum height=7em] (ac)
  {\begin{tabular}{c} Attitude \\ Controller \end{tabular}};
  \coordinate (tcz) at ([yshift=-0.5cm] tc.east);
  \draw [connector] (tcz) -- node{$\vbs{z}_\text{I}$}
  ($(ac.west)!(tcz)!(ac.north west)$);
  \draw [connector] ($(ac.east)!(tcz)!(ac.north east)$) -- node{$\vbs{N}_\text{B}$}
  ($(quad.south west)!(tcz)!(quad.west)$);
  \draw [dotconnector] (state)  |- (4,-3) -| node[pos=0.75, name=P]{} (tc);
  \coordinate (xinput) at ([yshift=-1.5cm] rinput);
  \draw [connector] (xinput) --
  node[pos=0.12]{$\vbs{x}_\text{I}$}($(ac.south west)!(xinput)!(ac.west)$);
  \draw [dotconnector] (P) -- ($(ac.south west)!(P)!(ac.west)$);
\end{tikzpicture}
\end{figure}

\subsection{Trajectory Controller}
The trajectory controller takes in $\vbs{r}_\text{I}$ and outputs the desired
direction of thrust $\vbs{z}_\text{I} = R^*_\text{IB} \vb{e}_3$ and the thrust
magnitude $F$.  Let the position error be given by
\[\vb{e}_r = \vbs{r}_\text{I} - \vb{r}_\text{I} \]
A PD control law based on $\vb{e}_r$ takes the form
\begin{equation}
  \label{eq:pdTrajectoryControl}
  \vbs{F}_\text{I} = k_r \vb{e}_r + k_{d} \vbd{e}_r + m g \vb{e}_3 +
  m \vbdd{r}^*_\text{I}
\end{equation}
The coefficients $k_r$ and $ k_{d}$ are scalars: for trajectory control,
there is no need to set the proportional and derivative gains differently for
each axis.  The terms $m g \vb{e}_3$ and $m \vbdd{r}^*_\text{I}$ are feedforward
terms.  The first counteracts the constant downward gravitational force.  The
second is the instantaneous acceleration demanded by the desired input
trajectory.

We normalize $\vbs{F}_\text{I}$ to get the desired direction of $\vb{z}$ in the
I frame:
\[ \vbs{z}_\text{I} = \frac{\vbs{F}_\text{I}}{\|\vbs{F}_\text{I}\|} \] One might
assume that the commanded thrust $F$ should be given by
$F = \|\vbs{F}_\text{I}\|$.  But because $\vb{z}_\text{I}$, the true direction
of the body $\vb{z}$-axis, may not be aligned with the desired direction
$\vbs{z}_\text{I}$, we instead take $F$ as the projection of $\vbs{F}_\text{I}$
along $\vb{z}_\text{I}$:
\[F = \vbs{F}_\text{I} \cdot \vb{z}_\text{I} = \left(\vbs{F}_\text{I}\right)\T
  \RBI\T \vb{e}_3 \]

\subsection{Attitude Controller}
The attitude controller takes in $\vbs{x}_\text{I}$, $\vbs{z}_\text{I}$, and $S_q$ and outputs
the commanded torque $\vb{N}_\text{B}$.

From the inputs  $\vbs{x}_\text{I}$ and  $\vbs{z}_\text{I}$, we can assemble a full
$\RBIs$ matrix.

\qa{How many attitude DOFs are constrained by $\vbs{x}_\text{I}$ and  $\vbs{z}_\text{I}$?}{4,
  which is one more than needed for $\RBIs$. The procedure below throws away one DOF from $\vbs{x}_\text{I}$.}

\subsubsection{Procedure for obtaining $\RBIs$}
Recall that $\vb{x}_\text{B} = \vb{e}_1$, $\vb{y}_\text{B} = \vb{e}_2$, and
$\vb{z}_\text{B} = \vb{e}_3$.  Thus, since $\vb{x}_\text{I} = \RBI\T \vb{x}_\text{B}$, and likewise
for $\vb{y}$ and $\vb{z}$, it follows that
\[\RBI\T = \left[\vb{x}_\text{I}, \vb{y}_\text{I}, \vb{z}_\text{I} \right] \]
\begin{enumerate}
\item We want to preserve $\vbs{z}_\text{I}$ exactly, so we set
  $\left(\RBIs\right)\T = [\vb{a}, \vb{b}, \vbs{z}_\text{I}]$.
\item We take $\vb{b}$ to be a unit vector orthogonal to both $\vbs{z}_\text{I}$ and
  $\vbs{x}_\text{I}$:
\[ \vb{b} = \frac{\vbs{z}_\text{I} \times \vbs{x}_\text{I}}{\|\vbs{z}_\text{I} \times \vbs{x}_\text{I}\|} \]
\item We set $\vb{a}$ to complete the right-handed triad: $\vb{a} = \vb{b}
  \times \vbs{z}_\text{I}$.
\end{enumerate}

\qa{Why not simply set $\displaystyle \left(\RBIs\right)\T = \left[\vbs{x}_\text{I}, \frac{\vbs{z}_\text{I} \times
      \vbs{x}_\text{I}}{\|\vbs{z}_\text{I} \times \vbs{x}_\text{I}\|}, \vbs{z}_\text{I} \right]$?
}{Because $\vbs{x}_\text{I}$ and $\vbs{z}_\text{I}$ may not be orthogonal (they are
  \emph{desired}, not actual).} 

\subsubsection{Error signal from $\RBIs$}
We now have our desired attitude $\RBIs$.  And from $S_q$ we have our actual
attitude $\RBI$.

\qa{How can be get an error signal from  $\RBIs$ and $\RBI$ analagous to
  $\vb{e}_r = \vbs{r}_\text{I} - \vb{r}_\text{I}$?  Shall we set $R_E = \RBIs - \RBI$?}{Perish
the thought!  Attitude error in terms of direction-cosine matrices is
\emph{multiplicative}, not additive.}

We define the error rotation matrix as $R_E \define \RBIs \RBI\T$ so that
$\RBIs = R_E \RBI$. Thus, if $\RBI = \RBIs$, $R_E = I_{3\times3}$.  Let's expand
$R_E$ for a closer look:
\begin{equation*}
   R_E = \left[\ba{ccc}
    e_{11} & e_{12} & e_{13} \\
    e_{21} & e_{22} & e_{23} \\
    e_{31} & e_{32} & e_{33} \ea \right] = 
\underbrace{\left[\ba{c}
    \left(\vbs{x}_\text{I}\right)\T \\
    \left(\vbs{y}_\text{I}\right)\T \\
    \left(\vbs{z}_\text{I}\right)\T \ea \right]}_{\RBIs}
\underbrace{\left[\ba{ccc} \vb{x}_\text{I} & \vb{y}_\text{I} & \vb{z}_\text{I} \ea \right]}_{\RBI}
\end{equation*}

\begin{minipage}{0.3\linewidth}
\begin{tikzpicture}
  \coordinate (origin) at (0,0);
  \coordinate (x) at (2,0);
  \coordinate (y) at (0,2);
  \coordinate (xs) at (1.73,1);
  \coordinate (ys) at (-1,1.73);
  \coordinate (xsb2) at (0.87,0.5);
  \coordinate (ysb2) at (-0.5,0.87);
  \draw [connector] (origin) -- node[pos=1.15]{$\vb{x}_\text{I}$} (x);
  \draw [connector] (origin) -- node[pos=1.15]{$\vb{y}_\text{I}$} (y);
  \draw [connector,cyan] (origin) -- node[pos=1.15]{$\vbs{x}_\text{I}$} (xs);
  \draw [connector,cyan] (origin) -- node[pos=1.15]{$\vbs{y}_\text{I}$} (ys);

  \draw[-Stealth,thick,olive] (xsb2) arc
  [
  start angle=30,
  end angle=90,
  x radius=1,
  y radius =1
  ]
  node[pos=0.4, above]{$\alpha$};
\end{tikzpicture}
\end{minipage}
\begin{minipage}{0.7\linewidth}
  \begin{tabular}{lcl}
    $e_{12} = \left(\vbs{x}_\text{I}\right)\T \vb{y}_\text{I} = \cos(\alpha)$
    &  & torque about $\vb{z}_\text{I}$ to zero \\
  \end{tabular}
\end{minipage}

\begin{minipage}{0.3\linewidth}
\begin{tikzpicture}
  \coordinate (origin) at (0,0);
  \coordinate (x) at (2,0);
  \coordinate (y) at (0,2);
  \coordinate (xs) at (1.73,1);
  \coordinate (ys) at (-1,1.73);
  \coordinate (xsb2) at (0.87,0.5);
  \coordinate (ysb2) at (-0.5,0.87);
  \draw [connector, cyan] (origin) -- node[pos=1.15]{$\vbs{z}_\text{I}$} (x);
  \draw [connector, cyan] (origin) -- node[pos=1.15]{$\vbs{x}_\text{I}$} (y);
  \draw [connector] (origin) -- node[pos=1.15]{$\vb{z}_\text{I}$} (xs);
  \draw [connector] (origin) -- node[pos=1.15]{$\vb{x}_\text{I}$} (ys);

  \draw[-Stealth,thick,olive] (xsb2) arc
  [
  start angle=30,
  end angle=90,
  x radius=1,
  y radius =1
  ]
  node[pos=0.4, above]{$\beta$};
\end{tikzpicture}
\end{minipage}
\begin{minipage}{0.7\linewidth}
  \begin{tabular}{lcl}
    $e_{13} = \left(\vbs{x}_\text{I}\right)\T \vb{z}_\text{I} = \cos(\beta)$
    &  & torque about $-\vb{y}_\text{I}$ to zero
  \end{tabular}
\end{minipage}

\begin{minipage}{0.3\linewidth}
\begin{tikzpicture}
  \coordinate (origin) at (0,0);
  \coordinate (x) at (2,0);
  \coordinate (y) at (0,2);
  \coordinate (xs) at (1.73,1);
  \coordinate (ys) at (-1,1.73);
  \coordinate (xsb2) at (0.87,0.5);
  \coordinate (ysb2) at (-0.5,0.87);
  \draw [connector, cyan] (origin) -- node[pos=1.15]{$\vbs{z}_\text{I}$} (x);
  \draw [connector, cyan] (origin) -- node[pos=1.15]{$\vbs{y}_\text{I}$} (y);
  \draw [connector] (origin) -- node[pos=1.15]{$\vb{z}_\text{I}$} (xs);
  \draw [connector] (origin) -- node[pos=1.15]{$\vb{y}_\text{I}$} (ys);

  \draw[-Stealth,thick,olive] (xsb2) arc
  [
  start angle=30,
  end angle=90,
  x radius=1,
  y radius =1
  ]
  node[pos=0.4, above]{$\gamma$};
\end{tikzpicture}
\end{minipage}
\begin{minipage}{0.7\linewidth}
  \begin{tabular}{lcl}
    $e_{23} = \left(\vbs{y}_\text{I}\right)\T \vb{z}_\text{I} = \cos(\gamma)$
    &  & torque about $\vb{x}_\text{I}$ to zero
  \end{tabular}
\end{minipage}

\subsubsection{Attitude control law}
Let $\vb{e}_R \define \left[e_{23}, -e_{13}, e_{12} \right]\T$.  Then we can
implement a PD attitude control law as
\begin{equation}
  \label{eq:pdAttitude1}
  \vb{N}_\text{B} = K^\prime \vb{e}_R + K_d^\prime \vbd{e}_R + \cpe{\wB} J \wB
\end{equation}
where $K^\prime, K_d^\prime \in \mathbb{R}^{3\times 3}$ are the gain coefficient
matrices.  Note that $\vb{e}_R$ is based on dot products, which apply
equivalently in the I and B frames.  The term $\cpe{\wB} J \wB$ is a feedforward
term that cancels with an identical term in Euler's equation so that the
closed-loop PD transfer function for each attitude axis is nearly second-order,
as discussed in Section \ref{sec:transf-funct-quad}.

An even more efficient control law applies torque along the Euler axis of
rotation.  For this approach, we define
\begin{equation}
  \label{eq:eEdef}
  \vb{e}_E \define \left[\ba{c} e_{23} - e_{32} \\ e_{31} - e_{13} \\ e_{12} - e_{21}
    \ea \right]
\end{equation}
and then apply
\begin{equation}
  \label{eq:pdAttitude2}
  \vb{N}_\text{B} = K \vb{e}_E + K_d \vbd{e}_E + \cpe{\wB} J \wB
\end{equation}
where $K, K_d \in \mathbb{R}^{3\times 3}$ are the new gain coefficient
matrices for $\vb{e}_E$.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:


% LocalWords:  DOFs cccc LTI nd ns ILT undamped ss PID ke ODEs FVT sQ sE lll IB
