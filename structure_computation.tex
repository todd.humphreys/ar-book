\chapter{Structure Computation}
\label{chap:struct-comp}
In our state estimator, we used 3D feature points with known coordinates, and
their corresponding projections onto the camera image plane, to aid in
estimating our quad's state.

Now we wish to reverse the process: Given the pose of the quad corresponding to
each image in a set of images, we wish to determine the 3D coordinates of common
features in the images.  This is \emph{structure computation}. The book by
Hartley and Zisserman is an excellent reference on this topic
\cite{hartley2000multiple}.

\section{Review}
Recall from Section \ref{sec:camera-measurements} the relationship between a 3D
point in I and the coordinates of its projection on a camera's image plane, in
homogeneous notation:
\begin{align}
  \underbrace{\left[\ba{c} f X_c \\ f Y_c \\ Z_c \ea \right]}_{\vb{x}} =
  \left[ \ba{ccc|c} 
  f & 0 & 0  & 0 \\
  0 & f & 0  & 0 \\
  \undermat{K}{0 & 0 & 1}  & 0 \ea \right] \left[ \ba{cc}
  R_\text{CI} & \vb{t}_\text{C}\\
  0_{1 \times 3} & 1 \ea \right] \underbrace{\left[\ba{c} X \\ Y \\ Z \\ 1\ea \right]}_{\vb{X}_\text{I}}              
\end{align}
We can rewrite this as
\[ \vb{x} = K[R_\text{CI}, \vb{t}_\text{C}] \vb{X}_\text{I} =
  P\vb{X}_\text{I}\] where the projection matrix
$P = K [R_\text{CI}, \vb{t}_\text{C}]$ depends on both the \emph{intrinsic}
parameter $f$ (embedded in $K$), and \emph{extrinsic} parameters $R_\text{CI}$
and $\vb{t}_\text{C}$.  The true (noise-free) 2D projection of $\vb{X}_\text{I}$
onto the camera's image plane is
\begin{align}
     \label{eq:camera_meters}
\vb{x}_{c} = \left[\ba{c} x \\ y \ea \right] = \left[\ba{c} f X_c/Z_c \\ f Y_c/Z_c
    \ea \right] \quad \mathrm{(meters)}  
\end{align}
To model the \emph{measured} projection onto the image
plane, we scale by the reciprocal pixel size $p_s$ and assume additive noise:
\begin{align}
  \label{eq:camera_pixels}
  \vbt{x}_{c} = \left(\sfrac{1}{p_s}\right) \vb{x}_{c} + \vb{w}_{c} \quad
  \mathrm{(pixels)}
\end{align}
We assume that $\vb{w}_{c}$ has mean $\E{\vb{w}_{c}} = \vb{0}$ and covariance
matrix $R_{c} = \E{\vb{w}_{c} \vb{w}_{c}\T} \in \mathbb{R}^{2 \times 2}$.

\section{Problem Setup}
Suppose we have a single point $\vb{X}$ viewed from two cameras (which might be
the same camera at two different locations), as shown in the following graphic:

{

  \centering
  \includegraphics[width=0.6\textwidth]{figs/geom_labeled}

}

We can write $\vb{x}_1 = P_1 \vb{X}$ and $\vb{x}_2 = P_2 \vb{X}$, where $P_i$
is the projection matrix for the $i$th camera, which is centered at $O_i$.
Given $P_1$, $P_2$, and $\vb{x}_1$, we can determine $\vb{e}_1$ and
$\vb{e}_2$.  We can then constrain our search for $\vb{x}_2$ to the right
epipolar line.  This is a key insight from epipolar geometry that improves the
speed and correctness of \emph{feature matching}.

\qa{Suppose we know $P_1, P_2, \vb{x}_1$, and $\vb{x}_2$.  Can we determine
  $\vb{X}$?}{There are 3 unknowns in $\vb{X}$, and we have 4 constraints, 2
  from $\vb{x}_1$ and 2 from $\vb{x}_2$.  As a rule of thumb for any estimation
  problem, the number of unknowns $N_u$ is a lower bound for the number of
  constraints $N_c$ necessary to solve the problem. (You may need more
  constraints than $N_u$ if one or more constraints is ``flat'' near the
  solution.)  Thus, $N_u \leq N_c$ is a \emph{necessary condition}.  For the
  present problem $N_u \leq N_c$ is also a \emph{sufficient condition} so long
  as the geometry forms a triangle, as in the figure above.}


\section{Estimating $\vb{X}$ from measurements}
Suppose we have noisy measurements $\vbt{x}_{c1}$ and $\vbt{x}_{c2}$.
Converting these to meters and expressing in homogeneous coordinates, we
have
\[\vbt{x}_1 = \left[\ba{c} p_s \vbt{x}_{c1} \\ 1 \ea \right], \quad \vbt{x}_2 =
  \left[\ba{c} p_s \vbt{x}_{c2} \\ 1 \ea \right] \] How can we estimate $\vb{X}$
from $\vbt{x}_1$ and $\vbt{x}_2$?  Consider the following candidate approaches.

\begin{itemize}
\item[(A)] Back-project rays through $\vbt{x}_1$ and $\vbt{x}_2$.  Because of
  noise, these will intersect with probability 0.  But we could find the point
  of \emph{nearest intersection} by taking the midpoint of the common
  perpendicular. (Note that the existence of a common perpendicular is
  guaranteed: imagine constraining a line to be perpendicular to one
  back-projected ray, then sliding that line along the ray, and rotating it
  about the ray, until it can be made perpendicular to the other ray.)

{\bf Pro}: Intuitive; easy to calculate.

{\bf Con}: Less accurate than other approaches.  No easy way to get the solution's
error covariance matrix.
\item[(B)] Set up a least-squares problem of the form
  \[\left[\ba{c} \vbt{x}_1 \\ \vbt{x}_2 \ea \right] = \left[\ba{c} P_1 \\ P_2
      \ea \right] \left[
      \ba{c} X \\ Y \\ Z \\ 1 \ea \right] + \left[\ba{c} w_{11} \\ w_{12} \\
      w_{13} \\ w_{21} \\ w_{22} \\ w_{23} \ea \right] \] Note that our use of
  the homogeneous representation makes this a \emph{linear} measurement model.
  We can easily solve for
  \[ \vb{X}_r = \left[ \ba{c} X \\ Y \\ Z \ea \right] \] using standard linear
  least squares techniques.

  {\bf Pro}: Intuitive.  Leads to simple linear least squares, from which an
  error covariance matrix can be obtained.

  {\bf Con}: Over-counts the number of measurements.  We really only have 4
  measurements, but the measurement model claims 6 due to the homogeneous
  representation.  This will make the solution misleadingly overconfident unless
  the noise covariance matrix indicates infinite noise variance for the
  homogeneous elements in $\vbt{x}_1$ and $\vbt{x}_2$, which would make the
  covariance matrix ill-conditioned.

\item[(C)] Set up a \emph{nonlinear} least squares problem of the
  form
  \[\left[\ba{c} \vbt{x}_{c1} \\ \vbt{x}_{c2} \ea \right] = \vb{h}(\vb{X}_r) +
    \left[\ba{c} \vb{w}_{c1} \\ \vb{w}_{c2} \ea \right] \] and solve for
  $\vb{X}_r$ using nonlinear least squares techniques.

  {\bf Pro}: Gold standard accuracy, plus an error covariance matrix can be
  obtained.

  {\bf Con}: Requires an initial guess for $\vb{X}_r$ about which the
  measurement model can be linearized.  Requires taking the derivative of 
  $\vb{h}:\mathbb{R}^3 \mapsto \mathbb{R}^4$ with respect to $\vb{X}_r$.

\item[(D)] Set up a linear least squares problem based on the observation that
  for a noise-free measurement $\vb{x} = P\vb{X}$ the cross product of
  $\vb{x}$ with $P\vb{X}$ is zero:
  \begin{equation}
    \label{eq:cp_zero}
  \vb{x} \times P\vb{X} = \vb{0}    
  \end{equation}
  Suppose $P$ is written in terms of its rows as
  \[ P = \left[\ba{c} \vb{p}^{1\Tr} \\ \vb{p}^{2\Tr} \\ \vb{p}^{3\Tr} \ea \right] \]
  and suppose that for ease of notation we write $\vb{x} = [x,y,1]\T$.  Then
  (\ref{eq:cp_zero}) can be expressed as three separate equations:
  \begin{align*}
      x (\vb{p}^{3\Tr}\vb{X})& - (\vb{p}^{1\Tr} \vb{X}) = 0  \\
    y (\vb{p}^{3\Tr} \vb{X})& - (\vb{p}^{2\Tr} \vb{X}) = 0  \\
    x (\vb{p}^{2\Tr} \vb{X})& - y(\vb{p}^{1\Tr} \vb{X}) = 0     
  \end{align*}
  Factoring out $\vb{X}$, one can show that only two of these equations are
  linearly independent.  Choosing the top two, substituting the measurement
  $\vbt{x}_{i} = \left[\tilde{x}_i, \tilde{y}_i, 1\right]\T$ for $\vb{x}$, and
  stacking equations for two cameras ($i \in \{1,2\}$) yields a measurement
  model of the form
  \begin{align}
    \label{eq:lin_indep_sc}
  \underbrace{\left[ \ba{c}
      \tilde{x}_1 \vb{p}_1^{3\Tr} -\vb{p}_1^{1\Tr} \\
      \tilde{y}_1 \vb{p}_1^{3\Tr} -\vb{p}_1^{2\Tr} \\
      \tilde{x}_2 \vb{p}_2^{3\Tr} -\vb{p}_2^{1\Tr} \\
      \tilde{y}_2 \vb{p}_2^{3\Tr} -\vb{p}_2^{2\Tr} \ea \right]}_{H} \vb{X} +
    \underbrace{\left[\ba{c} w_{11} \\ w_{12} \\ w_{21} \\ w_{22} \ea \right]}_{\vb{w}} = \left[
  \ba{c} 0\\0\\0\\0 \ea \right]
\end{align}
where the vector $\vb{w}$ has been added to account for noise in the
measurements $\tilde{x}_i$ and $\tilde{y}_i$, $i = 1, 2$.  The above model is
for only two cameras (images).  In general, for $N$ cameras, we have
$i = 1, 2, ..., N$, and $H$ has $2N$ rows.

We can rearrange (\ref{eq:lin_indep_sc}) into the form of a standard linear
measurement model.  Let $H_r$ be the first three columns of $H$, and $-\vb{z}$
be the fourth column.  Then we can write (\ref{eq:lin_indep_sc}) as
\[ \underbrace{\left[\ba{cc} H_r & -\vb{z} \ea \right]}_{H}
  \underbrace{\left[\ba{c} \vb{X}_r \\ 1 \ea \right]}_{\vb{X}} + \vb{w} = \vb{0} \] which
we can rearrange as
\begin{align}
  \label{eq:standard_measurement_model_sc}
  \vb{z} = H_r \vb{X}_r + \vb{w}
\end{align}
It is straightforward to solve this linear measurement equation for the optimal
$\vb{X}_r$ in a least-squares sense.  Assume that $\vb{w}$ has mean
$\E{\vb{w}} = \vb{0}$ and covariance matrix
$R = \E{\vb{w}\vb{w}\T} \in \mathbb{R}^{2N \times 2N}$.  Then the
least-squares-optimal solution for $\vb{X}_r$ is
\[ \vbh{X}_r = \left(H_r\T R^{-1} H_r \right)^{-1} H_r\T R^{-1} \vb{z} \]
Moreover, the solution's error covariance matrix is readily calculable:
\[P_x = \E{\left(\vbh{X}_r - \vb{X}_r \right)\left(\vbh{X}_r - \vb{X}_r
    \right)\T} = \left(H_r\T R^{-1} H_r\right)^{-1} \] $P_x$ tells us how
precise we can expect our estimate $\vbh{X}_r$ to be.

It's somewhat complicated to express $R$ exactly in terms of the covariance
$R_{c}$ of the pixel-level measurement (\ref{eq:camera_pixels}).  But $R$ can be
approximated as a block-diagonal matrix with repeated copies of $R_c$ lying
along the diagonal and all the other entries of the matrix equal 0, scaled by
$p_s^2$:
\[ R = p_s^2 \left[\ba{ccc} R_{c} &  &  0 \\
     & \ddots &  \\
     0 &  & R_{c} \ea \right] \]

Compared to the other methods, (D) has much to recommend it:  

{\bf Pro}: Leads to simple linear least squares, from which an accurate
estimate of $\vb{X}_r$ and an error covariance matrix $P_x$ can be obtained.

{\bf Con}: Slightly less accurate than method (C).  

\end{itemize}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End:
